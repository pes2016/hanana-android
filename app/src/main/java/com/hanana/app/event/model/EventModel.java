package com.hanana.app.event.model;

import android.util.Log;

import com.hanana.app.DataModels.ComplaintObject;
import com.hanana.app.DataModels.Event;
import com.hanana.app.DataModels.RateObject;
import com.hanana.app.RESTInterface.ComplainsAPI;
import com.hanana.app.RESTInterface.EventsAPI;
import com.hanana.app.Utils.ActiveUser;
import com.hanana.app.Utils.Constants;
import com.hanana.app.createEvent.presenter.ICreateEventPresenter;
import com.hanana.app.event.presenter.IEventPresenter;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.hanana.app.Utils.Constants.EVENT;

public class EventModel implements IEventModel{
    private IEventPresenter mPresenter;
    private final static String TAG = "EventModel";
    private EventsAPI eventsAPIService;
    private ComplainsAPI complainsAPIService;


    public EventModel(IEventPresenter eventPresenter) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.mPresenter = eventPresenter;
        this.eventsAPIService = retrofit.create(EventsAPI.class);
        this.complainsAPIService = retrofit.create(ComplainsAPI.class);
    }


    @Override
    public void getEvent(String eventId) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<Event> call = eventsAPIService.getEvent(eventId, accessToken);
        call.enqueue(new Callback<Event>() {
            @Override
            public void onResponse(Call<Event> call, Response<Event> response) {
                Event event = response.body();
                Log.d(TAG, "onResponse: " + response.code());
                mPresenter.displayEvent(event);
            }

            @Override
            public void onFailure(Call<Event> call, Throwable t) {
                Log.v(TAG,"Failure");
            }

        });
    }

    @Override
    public void toggleAssist(String eventId) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<Boolean> call = eventsAPIService.toggleAssist(eventId, accessToken);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                Boolean assist = response.body();
                if (assist) {
                    mPresenter.setAssistButtonPressed();
                } else {
                    mPresenter.setAssistButtonUnpressed();
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.v(TAG,"Failure");
            }

        });
    }

    @Override
    public void createEvent(final Event event, final File file) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<Event> call = eventsAPIService.createEvent(event, accessToken);
        call.enqueue(new Callback<Event>() {
            @Override
            public void onResponse(Call<Event> call, Response<Event> response) {
                Log.d(TAG, "onResponse: response code " + response.code());
                Event event = response.body();
                if(file != null) {
                    updateImage(event.get_Id(), file);
                }
                else{
                    Log.d(TAG, "onResponse: file is null");
                    ((ICreateEventPresenter)mPresenter).onEventCreated(event.get_Id());
                }
            }

            @Override
            public void onFailure(Call<Event> call, Throwable t) {
                Log.v(TAG,"Failure");
            }

        });
    }

    private void updateImage(final String eventId, File file) {
        Log.d(TAG, "updateImage: 'GONNA UPDATE THE IMAGE");
        String accessToken = ActiveUser.getInstance().getAccess_token();

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);


        Call<Event> call = eventsAPIService.updateAvatar(eventId, body ,accessToken);
        call.enqueue(new Callback<Event>() {
            @Override
            public void onResponse(Call<Event> call, Response<Event> response) {
                Log.d(TAG, "onResponse: response code image " + response.code());
                Log.d(TAG, "onResponse: response body image " + response.body());
                ((ICreateEventPresenter)mPresenter).onEventCreated(eventId);
            }

            @Override
            public void onFailure(Call<Event> call, Throwable t) {
                Log.v(TAG,"Failure" + t.getMessage());
            }
        });
    }

    @Override
    public void rateEvent(String eventId, int value, String comment) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<Void> call = eventsAPIService.rateEvent(eventId, new RateObject(value, comment), accessToken);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                int statusCode = response.code();
                Log.v(TAG,"rateEvent: Status code " + statusCode);
                if (statusCode < 300) {
                    Log.d(TAG, "onResponse: ok");
                }
                else {
                    Log.d(TAG, "onResponse: not ok");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e("rateEvent error:", t.getMessage());
            }
        });
    }

    @Override
    public void complainEvent(String eventId, String comment) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<Void> call = complainsAPIService.complain(eventId, EVENT, accessToken, new ComplaintObject(comment));
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                int statusCode = response.code();
                if (statusCode < 300) {
                    Log.d(TAG, "onResponse: ok");
                }
                Log.v(TAG,"complainEvent: Status code " + statusCode + " body " + response.raw());
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.v(TAG,"Failure");
            }
        });
    }

    @Override
    public void hasShared(String eventId) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<Void> call = eventsAPIService.share(eventId, accessToken);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                int statusCode = response.code();
                Log.v(TAG,"hasShared: Status code " + statusCode);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.v(TAG,"Failure");
            }
        });
    }
}

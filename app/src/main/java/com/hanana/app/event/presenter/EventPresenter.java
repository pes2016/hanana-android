package com.hanana.app.event.presenter;

import com.hanana.app.DataModels.Event;
import com.hanana.app.event.model.EventModel;
import com.hanana.app.event.model.IEventModel;
import com.hanana.app.event.view.IEventView;

public class EventPresenter implements IEventPresenter {
    private IEventView mView;
    private IEventModel mModel;
    private Boolean canModifiy;

    public EventPresenter(IEventView view) {
        super();
        mView = view;
        mModel = new EventModel(this);
        canModifiy = true;
    }

    @Override
    public void getEvent(String eventId) {
        mModel.getEvent(eventId);
    }

    @Override
    public void displayEvent(Event event) {
        mView.displayEvent(event);
    }

    @Override
    public void toggleAssist(String eventId) {
        if(canModifiy) {
            canModifiy = false;
            mModel.toggleAssist(eventId);
        }
    }

    @Override
    public void setAssistButtonPressed() {
        mView.setAssistButtonPressed();
        canModifiy = true;
    }

    @Override
    public void setAssistButtonUnpressed() {
        mView.setAssistButtonUnpressed();
        canModifiy = true;
    }

    @Override
    public void rateEvent(String eventId, int value, String comment) {
        mModel.rateEvent(eventId, value, comment);
    }

    @Override
    public void complainEvent(String eventId, String comment) {
        mModel.complainEvent(eventId, comment);
    }

    @Override
    public void hasShared(String eventId) {
        mModel.hasShared(eventId);
    }
}

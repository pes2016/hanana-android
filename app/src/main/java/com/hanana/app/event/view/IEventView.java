package com.hanana.app.event.view;

import com.hanana.app.Adapters.OnFragmentRequestDataListener;
import com.hanana.app.DataModels.Event;
import com.hanana.app.DataModels.RatesList;

public interface IEventView {
    void displayEvent(Event event);
    void setAssistButtonPressed();
    void setAssistButtonUnpressed();
}

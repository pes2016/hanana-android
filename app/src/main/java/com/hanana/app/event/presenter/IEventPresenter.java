package com.hanana.app.event.presenter;

import com.hanana.app.DataModels.Event;

public interface IEventPresenter {
    void getEvent(String eventId);
    void displayEvent(Event event);
    void toggleAssist(String eventId);
    void setAssistButtonPressed();
    void setAssistButtonUnpressed();
    void rateEvent(String eventId, int value, String comment);
    void complainEvent(String eventId, String comment);
    void hasShared(String eventId);
}

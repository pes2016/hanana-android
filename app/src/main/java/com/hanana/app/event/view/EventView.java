package com.hanana.app.event.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hanana.app.DataModels.Event;
import com.hanana.app.R;
import com.hanana.app.Utils.DateFormatter;
import com.hanana.app.Utils.ImageLoader;
import com.hanana.app.Utils.StringFormatter;
import com.hanana.app.Views.WebViewActivity;
import com.hanana.app.event.presenter.EventPresenter;
import com.hanana.app.event.presenter.IEventPresenter;
import com.hanana.app.follows.view.FollowsView;
import com.hanana.app.rates.view.RatesView;

import java.text.ParseException;

import eu.fiskur.chipcloud.ChipCloud;
import eu.fiskur.chipcloud.ChipListener;

import static com.hanana.app.Utils.Constants.EVENTS;
import static com.hanana.app.Utils.Constants.ID;
import static com.hanana.app.Utils.Constants.LIST_TYPE;

//TODO do not show map && tags if private
public class EventView extends AppCompatActivity implements IEventView, OnMapReadyCallback, View.OnClickListener {
    private final static String TAG = "EventView";
    private IEventPresenter mPresenter;
    private Event event;

    private Intent shareIntent;
    private Context context;

    private MapView mMapView;
    private GoogleMap mGoogleMap;
    private ScrollView mainScrollView;

    private TextView eventName, eventAssistants, eventFriends, eventDescription, eventAddress;
    private TextView eventDate, eventAge, eventPrice, eventHours, ratingComment, numRates, finalizado;
    private ImageView eventImage, transparentImageView;
    private Button eventMoreInfoButton, eventAssistButton;
    private RatingBar ratingBar;
    private ChipCloud chipCloud;
    private ShareActionProvider mShareActionProvider;
    private String eventId;
    private Boolean isPrivate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;

        setContentView(R.layout.activity_event_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initialize(this);
        initializeUI();

        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();
        try {
            MapsInitializer.initialize(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMapView.getMapAsync(this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        transparentImageView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        mainScrollView.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        double latitude, longitude;
        if (event == null || event.getLat() == null || event.getLon() == null) {
            latitude = 41.38879;
            longitude = 2.15899;
        }
        else {
            latitude = Double.parseDouble(event.getLat());
            longitude = Double.parseDouble(event.getLon());

            MarkerOptions marker = new MarkerOptions().position(
                    new LatLng(latitude, longitude));
            marker.icon(BitmapDescriptorFactory
                    .defaultMarker(BitmapDescriptorFactory.HUE_RED));
            mGoogleMap.addMarker(marker);
        }
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,longitude), 15));
    }

    private void initialize(IEventView view) {
        mPresenter = new EventPresenter(view);

        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        String eventId = "";

        if (Intent.ACTION_SEND.equals(action) && type != null && "text/plain".equals(type)) {
                eventId = intent.getStringExtra(Intent.EXTRA_TEXT);
        }else if (intent.getExtras() != null) {
            eventId = intent.getStringExtra(ID);
        } else {
            Log.d(TAG, "initialize: extras are null");
        }

        mPresenter.getEvent(eventId);
    }

    private void initializeUI() {
        eventName = (TextView) findViewById(R.id.event_single_name);
        eventAssistants = (TextView) findViewById(R.id.event_single_assistants);
        eventFriends = (TextView) findViewById(R.id.event_single_friends);
        eventDescription = (TextView) findViewById(R.id.event_single_description);
        eventAddress = (TextView) findViewById(R.id.event_single_address);
        eventDate = (TextView) findViewById(R.id.event_single_date);
        eventAge = (TextView) findViewById(R.id.event_single_age);
        eventPrice = (TextView) findViewById(R.id.event_single_price);
        eventHours = (TextView) findViewById(R.id.event_single_hours);
        eventImage = (ImageView) findViewById(R.id.event_single_image);
        chipCloud = (ChipCloud) findViewById(R.id.chip_cloud);
        eventMoreInfoButton = (Button) findViewById(R.id.event_single_button_info);
        eventAssistButton = (Button) findViewById(R.id.event_single_button_assist);
        mainScrollView = (ScrollView) findViewById(R.id.event_scrollView);
        mMapView = (MapView) findViewById(R.id.event_mapView);
        transparentImageView = (ImageView) findViewById(R.id.transparent_image);
        ratingBar = (RatingBar) findViewById(R.id.event_single_rating);
        ratingComment = (TextView) findViewById(R.id.event_rate_desc);
        numRates = (TextView) findViewById(R.id.event_num_rates);
        finalizado = (TextView) findViewById(R.id.finalizado);
        eventAssistButton.setOnClickListener(this);
        eventAssistants.setOnClickListener(this);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void displayEvent(Event modelEvent) {
        if (modelEvent == null) {
            Log.v(TAG, "EVENT IS NULL");
            return;
        }
        event = modelEvent;
        eventId = event.get_Id();

        setShareIntent();

        String age = event.getAge();
        if (age == null) {
            age = "+18";
        }
        eventAge.setText(age);


        String date = event.getDate();
        String hour = null;
        if (date == null) {
            date = "No date";
        }
        else {
            try {
                hour = DateFormatter.getHoursString(date);
                date = DateFormatter.getDDMMYYYY(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        eventDate.setText(date);

        eventHours.setText(hour);


        String price = event.getPrice();
        if (price == null) {
            price = "0";
        }
        eventPrice.setText(price + " €");


        String title = event.getTitle();
        eventName.setText(title);

        if (event.getNumberOfAssistants() == 0) {
            eventAssistants.setOnClickListener(null);
        }
        eventAssistants.setText(StringFormatter.getAssistantsWordFormatted(this,event));

        eventFriends.setText(StringFormatter.getFriendsWordFormatted(this,event));

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            eventDescription.setText(Html.fromHtml(event.getDescription(),Html.FROM_HTML_MODE_LEGACY));
        }
        else {
            //noinspection deprecation
            eventDescription.setText(Html.fromHtml(event.getDescription()));
        }

        String address = event.getAddress();
        eventAddress.setText(address);

        if (mGoogleMap != null && event.getLat() != null && event.getLon() != null) {
            double latitude = Double.parseDouble(event.getLat());
            double longitude = Double.parseDouble(event.getLon());

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(latitude, longitude)).zoom(15).build();
            mGoogleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));

            MarkerOptions marker = new MarkerOptions().position(
                    new LatLng(latitude, longitude));
            marker.icon(BitmapDescriptorFactory
                    .defaultMarker(BitmapDescriptorFactory.HUE_RED));
            mGoogleMap.addMarker(marker);
        }
        if (event.getLat() == null && event.getLon() == null) {
            isPrivate = true;
            findViewById(R.id.event_mapLayout).setVisibility(View.GONE);
            findViewById(R.id.tags_view).setVisibility(View.GONE);
        }
        if (event.getImage() != null && !event.getImage().isEmpty()) {
            ImageLoader.loadImage(this, event.getImage(), eventImage);
        }

        if(event.getUrl() != null && event.getUrl().length() > 0){
            eventMoreInfoButton.setOnClickListener(this);
        } else {
            eventMoreInfoButton.setEnabled(false);
        }

        boolean past, inProgres;
        past = inProgres = false;
        try {
            past = DateFormatter.checkIfPast(event.getDate());
            if (past) {
                inProgres = DateFormatter.checkIfInProgress(event.getDate());
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (past) {
            if (! inProgres) {
                finalizado.setVisibility(View.VISIBLE);
            }
            eventAssistButton.setOnClickListener(null);

            numRates.setVisibility(View.VISIBLE);
            if (event.getNumberOfRates() > 0) {
                numRates.setOnClickListener(this);
            }
            numRates.setText(StringFormatter.getNumberOfRatesWordFormatted(this,event));

            if (isPrivate && event.isWillAssist()) {
                setAttended();
            }
            else if(event.isHaveAttended()){
                ratingBar.setVisibility(View.VISIBLE);
                if (event.getMyRate() != null) {
                    ratingBar.setRating(event.getMyRate().getValue());
                    ratingComment.setText(event.getMyRate().getComment());
                    ratingBar.setIsIndicator(true);
                }
                else {
                    ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                        @Override
                        public void onRatingChanged(final RatingBar ratingBar, final float rating, boolean fromUser) {

                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.rating_dialog, null);

                            AlertDialog.Builder ratingDialog = new AlertDialog.Builder(context);

                            final RatingBar ratingBarDialog = (RatingBar) dialogView.findViewById(R.id.dialog_ratingbar);
                            ratingBarDialog.setRating(rating);
                            ratingBarDialog.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                                @Override
                                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                                    if (rating < 1) {
                                        ratingBar.setRating(1.0f);
                                    }
                                }
                            });

                            final RatingBar.OnRatingBarChangeListener thisContext = this;

                            ratingDialog.setTitle(R.string.rate_event)
                                    .setView(dialogView)
                                    .setPositiveButton(getString(R.string.send), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int id) {
                                            Log.d(TAG, "onAccept: ");
                                            ratingBar.setOnRatingBarChangeListener(null);
                                            ratingBar.setRating(ratingBarDialog.getRating());
                                            ratingBar.setOnRatingBarChangeListener(thisContext);

                                            int rating = (int) ratingBar.getRating();
                                            String comment = ((EditText) dialogView.findViewById(R.id.dialog_rate_desc)).getText().toString();
                                            ratingComment.setText(comment);

                                            mPresenter.rateEvent(eventId, rating, comment);

                                        }
                                    })
                                    .setOnCancelListener(new DialogInterface.OnCancelListener() {
                                        @Override
                                        public void onCancel(DialogInterface dialog) {
                                            Log.d(TAG, "onCancel: ");
                                            ratingBar.setOnRatingBarChangeListener(null);
                                            ratingBar.setRating(0);
                                            ratingBar.setOnRatingBarChangeListener(thisContext);
                                        }
                                    });

                            ratingDialog.create();
                            ratingDialog.show();
                        }
                    });
                }
                setAttended();
            } else {
                setNotAttended();
            }
        }
        else {
            if (event.isWillAssist()) {
                setAssistButtonPressed();
            } else {
                setAssistButtonUnpressed();
            }
        }

        String[] tagsArray = event.getTags().toArray(new String[0]);

        new ChipCloud.Configure()
                .chipCloud(chipCloud)
                .selectTransitionMS(500)
                .deselectTransitionMS(250)
                .labels(tagsArray)
                .mode(ChipCloud.Mode.NONE)
                .chipListener(new ChipListener() {
                    @Override
                    public void chipSelected(int index) {
                        Log.d(TAG, "chipSelected: " +index);
                    }
                    @Override
                    public void chipDeselected(int index) {
                        Log.d(TAG, "chipDeselected: " + index);
                    }
                })
                .build();

    }

    private void setAttended() {
        eventAssistButton.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimaryLight));
        eventAssistButton.setText(getResources().getString(R.string.attended));
    }

    private void setNotAttended() {
        eventAssistButton.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimaryLight));
        eventAssistButton.setText(getResources().getString(R.string.not_attended));
    }

    @Override
    public void setAssistButtonPressed() {
        eventAssistButton.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimaryLight));
        eventAssistButton.setText(getResources().getString(R.string.will_assist));
    }

    @Override
    public void setAssistButtonUnpressed() {
        eventAssistButton.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimaryDark));
        eventAssistButton.setText(getResources().getString(R.string.want_assist));
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.event_single_button_info:
                Intent i = new Intent(context, WebViewActivity.class);
                i.putExtra("url",event.getUrl());
                startActivity(i);
                break;

            case R.id.event_single_button_assist:
                mPresenter.toggleAssist(eventId);
                break;

            case R.id.event_single_assistants:
                Intent i2 = new Intent(context, FollowsView.class);
                i2.putExtra(ID,event.get_Id());
                i2.putExtra(LIST_TYPE, EVENTS);
                startActivity(i2);
                break;

            case R.id.event_num_rates:
                Intent i3 = new Intent(context, RatesView.class);
                i3.putExtra(ID, event.get_Id());
                startActivity(i3);
                break;
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate menu resource file.
        getMenuInflater().inflate(R.menu.event_single_menu, menu);
        // Locate MenuItem with ShareActionProvider
        MenuItem item = menu.findItem(R.id.menu_item_share);
        // Fetch and store ShareActionProvider
        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);

        if (shareIntent != null) {
            Log.d(TAG, "onCreateOptionsMenu: setShareIntent before menu");
            mShareActionProvider.setShareIntent(shareIntent);
        }

        mShareActionProvider.setOnShareTargetSelectedListener(new ShareActionProvider.OnShareTargetSelectedListener() {
            @Override
            public boolean onShareTargetSelected(ShareActionProvider source, Intent intent) {
                Log.d(TAG, "onShareTargetSelected: hasShared");
                mPresenter.hasShared(eventId);
                return false;
            }
        });

        // Return true to display menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_report) {
            LayoutInflater inflater = getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.report_dialog, null);

            AlertDialog.Builder reportDialog = new AlertDialog.Builder(context);

            reportDialog.setTitle(R.string.report_dialog)
                    .setView(dialogView)
                    .setPositiveButton(getString(R.string.send), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            Log.d(TAG, "onAccept: ");
                            String comment = ((EditText)dialogView.findViewById(R.id.dialog_report_desc)).getText().toString();
                            mPresenter.complainEvent(eventId, comment);
                        }
                    });
            reportDialog.create();
            reportDialog.show();
        }
        return true;
    }

    // Call to update the share intent
    private void setShareIntent() {
        Log.d(TAG, "setShareIntent: start set share intent");
        shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, StringFormatter.getEventShareString(this,event));

        if (mShareActionProvider != null) {
            Log.d(TAG, "setShareIntent: share provider ok");
            mShareActionProvider.setShareIntent(shareIntent);
        } else {
            Log.d(TAG, "setShareIntent: share provider is null");
        }
    }
}

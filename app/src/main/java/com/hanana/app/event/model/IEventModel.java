package com.hanana.app.event.model;

import com.hanana.app.DataModels.Event;

import java.io.File;

public interface IEventModel {
    void getEvent(String eventId);
    void toggleAssist(String eventId);
    void createEvent(Event event, File file);
    void rateEvent(String eventId, int value, String comment);
    void complainEvent(String eventId, String comment);
    void hasShared(String eventId);
}

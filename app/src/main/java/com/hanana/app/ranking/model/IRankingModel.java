package com.hanana.app.ranking.model;

public interface IRankingModel {
    void getRanking(int page);
}

package com.hanana.app.ranking.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.hanana.app.Adapters.Behaviors.IStartBehavior;
import com.hanana.app.Adapters.Behaviors.OpenUserBehavior;
import com.hanana.app.Adapters.Behaviors.RankingListStartBehavior;
import com.hanana.app.Adapters.GenericAdapter;
import com.hanana.app.Adapters.GenericFragment;
import com.hanana.app.Adapters.IGenericAdapter;
import com.hanana.app.Adapters.IGenericFragment;
import com.hanana.app.Adapters.IMapFragment;
import com.hanana.app.DataModels.User;
import com.hanana.app.R;
import com.hanana.app.Views.BaseActivity;
import com.hanana.app.ranking.presenter.IRankingPresenter;
import com.hanana.app.ranking.presenter.RankingPresenter;

import java.util.List;

import static com.hanana.app.Utils.Constants.RANKING_ACTIVITY_NBR;
import static com.hanana.app.Utils.Constants.RANKING_HOLDER_TYPE;

public class RankingView extends BaseActivity implements IRankingView {
    private IGenericFragment rankingFragment;
    private IRankingPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_single_fragment);
        super.setNavigationItemClicked(whoIAm());

        mPresenter = new RankingPresenter(this);

        IGenericAdapter adapter = new GenericAdapter(
            this,
            R.layout.ranking_item,
            RANKING_HOLDER_TYPE,
            new OpenUserBehavior()
        );

        IStartBehavior rankingListStartBehavior = new RankingListStartBehavior();

        rankingFragment = new GenericFragment();
        rankingFragment.initialize(adapter);
        rankingFragment.setStartBehavior(rankingListStartBehavior);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame, (Fragment) rankingFragment, "followsView")
                .commit();
    }


    @Override
    protected int whoIAm() {
        return RANKING_ACTIVITY_NBR;
    }


    @Override
    public void getData(int page) {
        mPresenter.getData(page);
    }

    @Override
    public void displayUsers(List<User> users, int page) {
        if(page != 0){
            rankingFragment.appendData(users);
        }
        else{
            rankingFragment.setData(users);
        }




    }

    @Override
    public void getEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getSuggestedEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getPopularEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getFriendsEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getGroups(int page, IGenericFragment fragment) {

    }

    @Override
    public void getFutureEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getPastEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getUsers(int page, IGenericFragment fragment) {

    }

    @Override
    public void getMapEvents(double lat, double lon, double distance, IMapFragment fragment) {

    }
}

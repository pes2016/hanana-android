package com.hanana.app.ranking.presenter;

import com.hanana.app.DataModels.User;

import java.util.List;

public interface IRankingPresenter {
    void displayUsers(List<User> users, int page);
    void getData(int page);
}

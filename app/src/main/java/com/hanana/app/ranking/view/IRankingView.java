package com.hanana.app.ranking.view;

import com.hanana.app.Adapters.OnFragmentRequestDataListener;
import com.hanana.app.DataModels.User;

import java.util.List;


public interface IRankingView extends OnFragmentRequestDataListener {
    void displayUsers(List<User> users, int page);
}

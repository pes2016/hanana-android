package com.hanana.app.ranking.presenter;

import com.hanana.app.DataModels.User;
import com.hanana.app.ranking.model.IRankingModel;
import com.hanana.app.ranking.model.RankingModel;
import com.hanana.app.ranking.view.IRankingView;

import java.util.List;


public class RankingPresenter implements IRankingPresenter{
    public final static String TAG = "RankingPresenter";
    private IRankingModel mModel;
    private IRankingView mView;

    public RankingPresenter(IRankingView view) {
        this.mModel = new RankingModel(this);
        mView = view;
    }

    @Override
    public void getData(int page) {
        mModel.getRanking(page);
    }

    @Override
    public void displayUsers(List<User> users, int page) {
        mView.displayUsers(users, page);
    }

}

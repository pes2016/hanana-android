package com.hanana.app.ranking.model;

import android.util.Log;

import com.hanana.app.DataModels.SearchObject;
import com.hanana.app.DataModels.UsersList;
import com.hanana.app.RESTInterface.UsersAPI;
import com.hanana.app.Utils.ActiveUser;
import com.hanana.app.Utils.Constants;
import com.hanana.app.blocked.presenter.IBlockedPresenter;
import com.hanana.app.ranking.presenter.IRankingPresenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RankingModel implements IRankingModel{
    private static final String TAG = "RankingModel";

    private IRankingPresenter mPresenter;
    private UsersAPI apiService;

    public RankingModel(IRankingPresenter presenter) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mPresenter = presenter;
        apiService = retrofit.create(UsersAPI.class);
    }

    @Override
    public void getRanking(final int page) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<UsersList> call = apiService.getRanking(accessToken, new SearchObject(page));
        call.enqueue(new Callback<UsersList>() {
            @Override
            public void onResponse(Call<UsersList> call, Response<UsersList> response) {
                UsersList users = response.body();
                mPresenter.displayUsers(users.getUsers(), page);
            }

            @Override
            public void onFailure(Call<UsersList> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }
}

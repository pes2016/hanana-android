package com.hanana.app.DataModels;

public class MapSearchObject extends SearchObject {
    private double lat, lon, distance;

    public MapSearchObject(String search, double lat, double lon, double distance) {
        super(search);
        this.lat = lat;
        this.lon = lon;
        this.distance = distance;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}

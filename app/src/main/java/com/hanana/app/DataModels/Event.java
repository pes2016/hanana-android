package com.hanana.app.DataModels;

import java.util.ArrayList;
import java.util.List;

public class Event implements IEvent {
    private String title, description, date, lat, lon, image, url, age, _id;
    private boolean willAssist, haveAttended;
    private List<String> friends, assistants, tags;
    private String price, address;
    private int numberOfRates;
    private RateObject myRate;

    public Event() {
        super();
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String get_Id() {
        return _id;
    }

    public boolean isHaveAttended() {
        return haveAttended;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public int getNumberOfFriends() {
        if (friends == null) {
            friends = new ArrayList<>();
        }
        return friends.size();
    }

    public int getNumberOfAssistants() {
        if (assistants == null) {
            assistants = new ArrayList<>();
        }
        return assistants.size();
    }

    public int getNumberOfRates() {
        return numberOfRates;
    }

    public String getPrice() {
        return price;
    }

    public String getAge() {
        return age;
    }

    public String getAddress() {
        return address;
    }

    public List<String> getTags() {
        return tags;
    }

    public boolean isWillAssist() {
        return willAssist;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public RateObject getMyRate() {
        return myRate;
    }
}

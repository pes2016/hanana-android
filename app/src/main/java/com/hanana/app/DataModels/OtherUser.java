package com.hanana.app.DataModels;

import java.util.ArrayList;
import java.util.List;

public class OtherUser implements INumberOfFollowers, INumberOfBadges {
    private Boolean isPrivate, isActive, isFollowing, isPending, isFollowingBack, isDiscarded, canChat;
    private String _id, avatar, about, name;
    private int numberOfFollowers, numberOfFollowings;
    private List<String> following, followers, pending;
    private List<Group> followingGroups;
    private List<EventListItem> events;
    private List<Badge> badgeValues;

    public String get_Id() {
        return _id;
    }

    public void set_Id(String _id) {
        this._id = _id;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Boolean getCanChat() {
        return canChat;
    }

    public void setCanChat(Boolean canChat) {
        this.canChat = canChat;
    }

    public Boolean getIsDiscarded() {
        return isDiscarded;
    }

    public void setIsDiscarded(Boolean discarded) {
        isDiscarded = discarded;
    }

    public List<EventListItem> getEvents() {
        return events;
    }

    public void setEvents(List<EventListItem> events) {
        this.events = events;
    }

    public List<String> getFollowers() {
        return followers;
    }

    public void setFollowers(List<String> followers) {
        this.followers = followers;
    }

    public List<String> getFollowing() {
        return following;
    }

    public void setFollowing(List<String> following) {
        this.following = following;
    }

    public Boolean getFollowingBack() {
        return isFollowingBack;
    }

    public void setFollowingBack(Boolean followingBack) {
        isFollowingBack = followingBack;
    }

    public List<String> getPending() {
        return pending;
    }

    public void setPending(List<String> pending) {
        this.pending = pending;
    }

    public Boolean getIsPending() {
        return isPending;
    }

    public void setIsPending(Boolean pending) {
        isPending = pending;
    }

    public Boolean getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(Boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfFollowers() {
        return numberOfFollowers;
    }

    public void setNumberOfFollowers(int numberOfFollowers) {
        this.numberOfFollowers = numberOfFollowers;
    }

    public int getNumberOfFollowings() {
        return numberOfFollowings;
    }

    public void setNumberOfFollowings(int numberOfFollowings) {
        this.numberOfFollowings = numberOfFollowings;
    }

    public List<Group> getFollowingGroups() {
        return followingGroups;
    }

    public void setFollowingGroups(List<Group> followingGroups) {
        this.followingGroups = followingGroups;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Boolean getIsFollowing() {
        return isFollowing;
    }

    public void setIsFollowing(Boolean following) {
        isFollowing = following;
    }

    public int getNumberOfBadges() {
        if (badgeValues == null) {
            badgeValues = new ArrayList<>();
        }
        return badgeValues.size();
    }

    public void setBadgeValues(List<Badge> badgeValues) {
        this.badgeValues = badgeValues;
    }
}
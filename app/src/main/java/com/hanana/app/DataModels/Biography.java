package com.hanana.app.DataModels;

public class Biography {
    private String about;

    public Biography(String text) {
        this.about = text;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }
}

package com.hanana.app.DataModels;

import java.util.List;

public class Group implements INumberOfFollowers {
    private String _id, name, description, image;
    private boolean isActive, isFollowing;
    private int numberOfEvents, numberOfFollowers;
    private List<EventListItem> futureEvents, pastEvents;

    public String get_Id() {
        return _id;
    }

    public void set_Id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isFollowing() {
        return isFollowing;
    }

    public void setFollowing(boolean following) {
        isFollowing = following;
    }

    public int getNumberOfEvents() {
        return numberOfEvents;
    }

    public void setNumberOfEvents(int numberOfEvents) {
        this.numberOfEvents = numberOfEvents;
    }

    public int getNumberOfFollowers() {
        return numberOfFollowers;
    }

    public void setNumberOfFollowers(int numberOfFollowers) {
        this.numberOfFollowers = numberOfFollowers;
    }

    public List<EventListItem> getFutureEvents() {
        return futureEvents;
    }

    public void setFutureEvents(List<EventListItem> futureEvents) {
        this.futureEvents = futureEvents;
    }

    public List<EventListItem> getPastEvents() {
        return pastEvents;
    }

    public void setPastEvents(List<EventListItem> pastEvents) {
        this.pastEvents = pastEvents;
    }
}

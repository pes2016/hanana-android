package com.hanana.app.DataModels;

import android.os.Bundle;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by marcos on 12/01/2017.
 */

public class NotificationDAO {
    private String title, message;
    private boolean isChat;
    private Map<String,String> extras;

    public NotificationDAO() {
        extras = new HashMap<>();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isChat() {
        return isChat;
    }

    public void setChat(boolean chat) {
        isChat = chat;
    }

    public void putExtra(String key, String value){
        extras.put(key,value);
    }

    public String getExtra(String key){
        return extras.get(key);
    }

}

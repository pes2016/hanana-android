package com.hanana.app.DataModels;

import java.util.ArrayList;
import java.util.List;

public class EventListItem implements IEvent {
    private String title, description, date, lat, lon, image, _id;
    private List<String> friends, assistants;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String get_Id() {
        return _id;
    }

    public void set_Id(String _id) {
        this._id = _id;
    }

    public List<String> getFriends() {
        return friends;
    }

    public void setFriends(List<String> friends) {
        this.friends = friends;
    }

    public int getNumberOfFriends() {
        if (friends == null) {
            friends = new ArrayList<>();
        }
        return friends.size();
    }

    public int getNumberOfAssistants() {
        if (assistants == null) {
            assistants = new ArrayList<>();
        }
        return assistants.size();
    }

    public List<String> getAssistants() {
        return assistants;
    }

    public void setAssistants(List<String> assistants) {
        this.assistants = assistants;
    }
}

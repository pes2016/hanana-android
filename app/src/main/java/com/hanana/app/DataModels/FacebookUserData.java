package com.hanana.app.DataModels;

public class FacebookUserData {
    private String access_token;
    private String id;

    public FacebookUserData() {
        super();
    }

    public FacebookUserData(String token, String userId) {
        super();
        this.access_token = token;
        this.id = userId;
    }


    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

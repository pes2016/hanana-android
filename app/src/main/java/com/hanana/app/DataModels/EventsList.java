package com.hanana.app.DataModels;

import java.util.List;

public class EventsList {

    private List<EventListItem> events;

    public List<EventListItem> getEvents() {
        return events;
    }

    public void setEvents(List<EventListItem> events) {
        this.events = events;
    }
}

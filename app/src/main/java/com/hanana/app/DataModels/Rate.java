package com.hanana.app.DataModels;

import com.hanana.app.Utils.ActiveUser;

public class Rate {
    private Boolean justVoted, hidden, mine;
    private String _id, comment;
    private int value, voted, numberOfUpvotes, numberOfDownvotes;
    private OtherUser user;

    public String get_Id() {
        return _id;
    }

    public String getUserId() {
        return user.get_Id();
    }

    public String getAvatar() {
        return user.getAvatar();
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public String getName() {
        return user.getName();
    }

    public int getNumberOfDownvotes() {
        return numberOfDownvotes;
    }

    public int getNumberOfUpvotes() {
        return numberOfUpvotes;
    }

    public String getComment() {
        return comment;
    }

    public Boolean isJustVoted() {
        if (justVoted == null) {
            justVoted = false;
        }
        return justVoted;
    }

    public Boolean isMine() {
        if (mine == null) {
            mine = user.get_Id().equals(ActiveUser.getInstance().get_Id());
        }
        return mine;
    }

    public void setJustVoted(Boolean voted) {
        justVoted = voted;
    }

    public int getValue() {
        return value;
    }

    public boolean getIsUpvoted() {
        return voted == 1;
    }

    public boolean getIsDownvoted() {
        return voted == -1;
    }
}
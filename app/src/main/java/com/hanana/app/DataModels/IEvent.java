package com.hanana.app.DataModels;

public interface IEvent {
    String getTitle();
    int getNumberOfFriends();
    int getNumberOfAssistants();
    String getDate();
}

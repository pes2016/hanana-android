package com.hanana.app.DataModels;

import java.util.ArrayList;
import java.util.List;

public class User implements INumberOfFollowers, INumberOfBadges, IPoints{
    private Boolean isPrivate, isActive, isFirstLogin, isFollowing, pushNotifications;
    private String _id, access_token, refresh_token, avatar, about, name;
    private int numberOfFollowers, numberOfFollowersInCommon, points;
    private List<Badge> badgeValues;

    public User(String _id) {
        this._id = _id;
    }

    public String get_Id() {
        return _id;
    }

    public void set_Id(String _id) {
        this._id = _id;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Boolean getPrivate() {
        return isPrivate;
    }

    public void setPrivate(Boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public Boolean isFirstLogin() {
        return isFirstLogin;
    }

    public void setFirstLogin(Boolean firstLogin) {
        isFirstLogin = firstLogin;
    }

    public int getNumberOfFollowers() {
        return numberOfFollowers;
    }

    public void setNumberOfFollowers(int numberOfFollowers) {
        this.numberOfFollowers = numberOfFollowers;
    }

    public int getNumberOfFollowersInCommon() {
        return numberOfFollowersInCommon;
    }

    public void setNumberOfFollowersInCommon(int numberOfFollowersInCommon) {
        this.numberOfFollowersInCommon = numberOfFollowersInCommon;
    }

    public Boolean getIsFollowing() {
        return isFollowing;
    }

    public void setIsFollowing(Boolean following) {
        isFollowing = following;
    }

    public int getNumberOfBadges() {
        if (badgeValues == null) {
            badgeValues = new ArrayList<>();
        }
        return badgeValues.size();
    }

    public void setBadgeValues(List<Badge> badgeValues) {
        this.badgeValues = badgeValues;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public Boolean getPushNotifications() {
        return pushNotifications;
    }

    public void setPushNotifications(Boolean pushNotifications) {
        this.pushNotifications = pushNotifications;
    }
}
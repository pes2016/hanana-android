package com.hanana.app.DataModels;

import java.util.List;

public class RatesList {
    private List<Rate> rates;

    public List<Rate> getRates() {
        return rates;
    }

    public void setRates(List<Rate> users) {
        this.rates = rates;
    }
}

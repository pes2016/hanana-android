package com.hanana.app.DataModels;

public class Badge implements IPoints{
    private String _id, name, description, image;
    private int points, currentValue, triggerValue;

    public String get_Id() {
        return _id;
    }

    public void set_Id(String _id) {
        this._id = _id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(int currentValue) {
        this.currentValue = currentValue;
    }

    public int getTriggerValue() {
        return triggerValue;
    }

    public void setTriggerValue(int triggerValue) {
        this.triggerValue = triggerValue;
    }
}

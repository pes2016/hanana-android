package com.hanana.app.DataModels;

public interface INumberOfBadges {
    int getNumberOfBadges();
}

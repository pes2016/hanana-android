package com.hanana.app.DataModels;

public interface INumberOfFollowers {
    int getNumberOfFollowers();
}

package com.hanana.app.DataModels;

public class SearchObject {
    private String searchData;
    private int page;

    public SearchObject(String search) {
        this.searchData = search;
        this.page = 0;
    }

    public SearchObject(int page) {
        this.page = page;
    }

    public SearchObject(String search, int page) {
        this.searchData = search;
        this.page = page;
    }
}

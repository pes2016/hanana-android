package com.hanana.app.DataModels;

import java.util.List;

public class BadgesList {
    private List<Badge> badges;

    public List<Badge> getBadges() {
        return badges;
    }

    public void setBadges(List<Badge> badges) {
        this.badges = badges;
    }
}

package com.hanana.app.DataModels;

import java.util.List;

public class GroupList {
    private List<Group> groups;

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }
}

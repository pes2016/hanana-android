package com.hanana.app.profile.presenter;

import com.hanana.app.DataModels.OtherUser;
import com.hanana.app.profile.model.IProfileModel;
import com.hanana.app.profile.model.ProfileModel;
import com.hanana.app.profile.view.IProfileView;

import java.io.File;

public class ProfilePresenter implements IProfilePresenter {
    final static String TAG = "ProfilePresenter";
    private IProfileView mView;
    private IProfileModel mModel;


    public ProfilePresenter(IProfileView mView) {
        this.mView = mView;
        this.mModel = new ProfileModel(this);
    }

    @Override
    public void uploadAvatar(File imageFile) {
        mModel.uploadAvatar(imageFile);
    }

    @Override
    public void updateBio(String bio) {
        mModel.updateBio(bio);
    }

    @Override
    public void refreshAvatar() {
        mView.refreshAvatar();
    }

    @Override
    public void getMe() {
        mModel.getMe();
    }

    @Override
    public void displayMe(OtherUser user) {
        mView.displayMe(user);
    }

    @Override
    public void activatePrivateButton() {
        mView.activatePrivateButton();
    }

    @Override
    public void togglePrivacity() {
        mModel.togglePrivacity();
    }

    @Override
    public void togglePrivateButton() {
        mView.togglePrivateButton();
    }

    @Override
    public void toggleFollowing(String userId) {
        mModel.toggleFollowing(userId);
    }

    @Override
    public void activateFollowingButton() {
        mView.activateFollowingButton();
    }

    @Override
    public void toggleFollowingButton() {
        mView.toggleFollowingButton();
    }

    @Override
    public void getUser(String userId) {
        mModel.getUser(userId);
    }

    @Override
    public void displayUser(OtherUser user) {
        mView.displayUser(user);
    }

    @Override
    public void blockUser(String userId) {
        mModel.blockUser(userId);
    }

    @Override
    public void sendToHome() {
        mView.sentToHome();
    }

    @Override
    public void complainUser(String userId, String comment) {
        mModel.complainUser(userId, comment);
    }
}

package com.hanana.app.profile.view;

import com.hanana.app.Adapters.OnFragmentRequestDataListener;
import com.hanana.app.DataModels.OtherUser;

public interface IProfileView extends OnFragmentRequestDataListener{
    void displayMe(OtherUser user);
    void displayUser(OtherUser user);
    void togglePrivateButton();
    void activatePrivateButton();
    void activateFollowingButton();
    void toggleFollowingButton();
    void sentToHome();
    void refreshAvatar();
}

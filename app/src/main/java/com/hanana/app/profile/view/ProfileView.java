package com.hanana.app.profile.view;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hanana.app.Adapters.IGenericFragment;
import com.hanana.app.Adapters.IMapFragment;
import com.hanana.app.Adapters.TabControllers.ProfileFragmentAdapter;
import com.hanana.app.DataModels.OtherUser;
import com.hanana.app.R;
import com.hanana.app.Utils.ActiveUser;
import com.hanana.app.Utils.ImageLoader;
import com.hanana.app.Utils.StringFormatter;
import com.hanana.app.Views.BaseActivity;
import com.hanana.app.Views.ChatView;
import com.hanana.app.badges.view.BadgesView;
import com.hanana.app.follows.view.FollowsView;
import com.hanana.app.fullDiscover.view.FullDiscoverView;
import com.hanana.app.pendingRequests.view.PendingRequestsView;
import com.hanana.app.profile.presenter.IProfilePresenter;
import com.hanana.app.profile.presenter.ProfilePresenter;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.SendBirdException;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static com.hanana.app.Utils.Constants.FOLLOWERS;
import static com.hanana.app.Utils.Constants.FOLLOWINGS;
import static com.hanana.app.Utils.Constants.ID;
import static com.hanana.app.Utils.Constants.LIST_TYPE;
import static com.hanana.app.Utils.Constants.PROFILE_ACTIVITY_NBR;
import static com.hanana.app.Utils.Constants.SELECT_PHOTO;

public class ProfileView extends BaseActivity implements IProfileView, View.OnClickListener {
    private final static String TAG = "ProfileView";
    private IProfilePresenter mPresenter;
    private IGenericFragment groupsFragment, eventsFragment;
    private Context context;
    private OtherUser user;
    private ImageView pictureView, lockView, chatView, followView;
    private TextView nameTextView, biographyTextView, followingTextView, badgesTextView,followersTextView, inboxTextView;
    private EditText biographyEditText;
    private RelativeLayout inboxContainer;
    private boolean isPrivate, isEditing, isFollowing, itsMe;
    private String bioAux, userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_profile_view);
        context = this;
        initialize(this);
        initializeUI();

        if (itsMe) {
            super.setNavigationItemClicked(whoIAm());
        }
    }

    private void initialize(IProfileView view) {
        mPresenter = new ProfilePresenter(view);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            userId = extras.getString(ID, "");
            Log.d(TAG, "userId:" + userId);
            if (userId.equals(ActiveUser.getInstance().get_Id())) {
                itsMe = true;
            }
            else {
                itsMe = false;
            }
        } else {
            userId = ActiveUser.getInstance().get_Id();
            itsMe = true;
            isEditing = false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //TODO hacer la misma petición ??
        if (itsMe) {
            mPresenter.getMe();
        }
        else {
            mPresenter.getUser(userId);
        }
    }

    private void initializeUI() {
        pictureView = (ImageView) findViewById(R.id.imageView_profile);
        pictureView.setOnClickListener(this);
        lockView = (ImageView) findViewById(R.id.imageView_lock);

        if (itsMe) {
            inboxContainer = (RelativeLayout) findViewById(R.id.inbox_container);
            inboxTextView = (TextView) findViewById(R.id.textView_inbox);
            lockView.setOnClickListener(this);
            lockView.setVisibility(View.VISIBLE);
            inboxContainer.setOnClickListener(this);
        }
        else {
            chatView = (ImageView) findViewById(R.id.imageView_chat);
            followView = (ImageView) findViewById(R.id.imageView_follow);
            followView.setVisibility(View.VISIBLE);
            followView.setOnClickListener(this);
        }
        nameTextView = (TextView) findViewById(R.id.textView_name);
        biographyTextView = (TextView) findViewById(R.id.textView_biography);
        biographyEditText = (EditText) findViewById(R.id.editText_biography);
        followingTextView = (TextView) findViewById(R.id.textView_following);
        badgesTextView = (TextView) findViewById(R.id.textView_badges);
        followersTextView = (TextView) findViewById(R.id.textView_followers);
        followingTextView.setOnClickListener(this);
        badgesTextView.setOnClickListener(this);
        followersTextView.setOnClickListener(this);



        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout_profile);
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.groups)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.events)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.interests)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final ProfileFragmentAdapter adapter = new ProfileFragmentAdapter
                (context, tabLayout.getTabCount(), itsMe, userId);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(2);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (itsMe) {
            getMenuInflater().inflate(R.menu.activity_profile, menu);
        }
        else {
            getMenuInflater().inflate(R.menu.activity_profile_other, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                if (!isEditing) {
                    isEditing = true;
                    bioAux = biographyEditText.getText().toString();
                    biographyTextView.setVisibility(View.GONE);
                    biographyEditText.setVisibility(View.VISIBLE);
                    if(bioAux.equals(getString(R.string.about_you_placeholder))) {
                        biographyEditText.setText("");
                    }
                    biographyEditText.setSelection(biographyEditText.getText().length());
                    item.setIcon(R.drawable.ic_save);

                    biographyEditText.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(biographyEditText, InputMethodManager.SHOW_IMPLICIT);
                }
                else {
                    isEditing = false;
                    biographyTextView.setVisibility(View.VISIBLE);
                    biographyEditText.setVisibility(View.GONE);
                    String bio = biographyEditText.getText().toString();
                    if (!bioAux.equals(bio)) {
                        mPresenter.updateBio(bio);
                    }
                    if(bio.length() == 0) {
                        bio = getString(R.string.about_you_placeholder);
                    }
                    biographyTextView.setText(bio);
                    item.setIcon(R.drawable.ic_edit);
                    View view = this.getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                }
                break;

            case R.id.action_block:
                mPresenter.blockUser(userId);
                break;

            case R.id.action_report:
                LayoutInflater inflater = getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.report_dialog, null);

                AlertDialog.Builder reportDialog = new AlertDialog.Builder(context);

                reportDialog.setTitle(R.string.report_dialog)
                        .setView(dialogView)
                        .setPositiveButton(getString(R.string.send), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                Log.d(TAG, "onAccept: ");
                                String comment = ((EditText)dialogView.findViewById(R.id.dialog_report_desc)).getText().toString();
                                mPresenter.complainUser(userId, comment);
                            }
                        });
                reportDialog.create();
                reportDialog.show();
                break;
        }
        return true;
    }

    @Override
    protected int whoIAm() {
        return PROFILE_ACTIVITY_NBR;
    }

    @Override
    public void refreshAvatar() {
        ImageLoader.loadImageCircle(this, ActiveUser.getInstance().getAvatar(), pictureView);
    }


    @Override
    public void displayMe(OtherUser userModel) {
        user = userModel;
        ImageLoader.loadImageCircle(this, user.getAvatar(), pictureView);
        if (groupsFragment != null) groupsFragment.setData(user.getFollowingGroups());
        if (eventsFragment != null) eventsFragment.setData(user.getEvents());

        nameTextView.setText(user.getName());
        if (user.getIsPrivate()) {
            lockView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_lock_closed, null));
            isPrivate = true;
        }
        else {
            lockView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_lock_open, null));
            isPrivate = false;
        }
        String about = user.getAbout();
        if(about == null || about.length() == 0){
            about = getString(R.string.about_you_placeholder);
            biographyEditText.setText("");
        }
        else {
            biographyEditText.setText(about);
        }
        biographyTextView.setText(about);
        if (user.getIsPrivate()){
            String pending;
            if (user.getPending() == null) {
                pending = "0";
            } else {
                pending = String.valueOf(user.getPending().size());
            }
            inboxTextView.setText(pending);
            inboxContainer.setVisibility(View.VISIBLE);
        }
        if (user.getNumberOfFollowings() == 0) {
            followingTextView.setOnClickListener(null);
        }
        followingTextView.setText(user.getNumberOfFollowings() + " " + getString(R.string.following));
        if (user.getNumberOfFollowers() == 0) {
            followersTextView.setOnClickListener(null);
        }
        followersTextView.setText(StringFormatter.getNumberOfFollowersWordFormatted(this, user));
        if (user.getNumberOfBadges() == 0) {
            badgesTextView.setOnClickListener(null);
        }
        badgesTextView.setText(StringFormatter.getNumberOfBadgesWordFormatted(this, user));

    }

    @Override
    public void displayUser(OtherUser userModel) {
        user = userModel;
        ImageLoader.loadImageCircle(this, user.getAvatar(), pictureView);
        nameTextView.setText(user.getName());
        if (user.getIsPrivate() && !user.getIsFollowing()) {
            lockView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_lock_others, null));
            lockView.setVisibility(View.VISIBLE);
            findViewById(R.id.tab_layout_profile).setVisibility(View.GONE);
            findViewById(R.id.pager).setVisibility(View.GONE);
            followersTextView.setOnClickListener(null);
            badgesTextView.setOnClickListener(null);
            followingTextView.setOnClickListener(null);
            if (user.getIsDiscarded()) {
                followView.setVisibility(View.GONE);
                followView.setOnClickListener(null);
                isFollowing = false;
            }
            else if (user.getIsPending()) {
                followView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_hourglass_empty, null));
                isFollowing = false;
            }
            else {
                followView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_follow, null));
                isFollowing = false;
            }
        }
        else {
            if (user.getIsFollowing()) {
                isFollowing = true;
                followView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_cancel, null));
            }
            else {
                isFollowing = false;
                followView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_follow, null));
            }
            //TODO presenter + model
            if (groupsFragment != null) groupsFragment.setData(user.getFollowingGroups());
            if (eventsFragment != null) eventsFragment.setData(user.getEvents());
            if (user.getCanChat()) {
                chatView.setVisibility(View.VISIBLE);
                chatView.setOnClickListener(this);
            }
        }
        biographyTextView.setText(user.getAbout());

        if (user.getNumberOfFollowings() == 0) {
            followingTextView.setOnClickListener(null);
        }
        followingTextView.setText(user.getNumberOfFollowings() + " " + getString(R.string.following));
        if (user.getNumberOfFollowers() == 0) {
            followersTextView.setOnClickListener(null);
        }
        followersTextView.setText(StringFormatter.getNumberOfFollowersWordFormatted(this, user));
        if (user.getNumberOfBadges() == 0) {
            badgesTextView.setOnClickListener(null);
        }
        badgesTextView.setText(StringFormatter.getNumberOfBadgesWordFormatted(this, user));
    }

    @Override
    public void activatePrivateButton() {
        lockView.setClickable(true);
    }

    @Override
    public void togglePrivateButton() {
        lockView.setClickable(true);
        if (isPrivate) {
            lockView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_lock_open, null));
            isPrivate = false;
        }
        else {
            lockView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_lock_closed, null));
            isPrivate = true;
        }
    }

    @Override
    public void activateFollowingButton() {
        followView.setClickable(true);
    }

    @Override
    public void toggleFollowingButton() {
        followView.setClickable(true);
        if (user.getIsPrivate()) {
            if (user.getIsPending()) {
                followView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_follow, null));
                user.setIsPending(false);
            }
            else if (!user.getIsFollowing()) {
                followView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_hourglass_empty, null));
                user.setIsPending(true);
            }
            else {
                followView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_follow, null));
                chatView.setVisibility(View.GONE);
                user.setIsFollowing(false);
                user.setCanChat(false); //TODO ??
                isFollowing = false;
            }

        }
        else {
            if (isFollowing) {
                followView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_follow, null));
                chatView.setVisibility(View.GONE);
                user.setIsFollowing(false);
                user.setCanChat(false); //TODO ??
                isFollowing = false;
            } else {
                followView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_cancel, null));
                if (user.getFollowingBack()) {
                    chatView.setVisibility(View.VISIBLE);
                }
                user.setIsFollowing(true);
                isFollowing = true;
            }
        }
    }

    @Override
    public void sentToHome() {
        //TODO showToast with info?
        Intent intent = new Intent(context, FullDiscoverView.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.imageView_profile:
                if (itsMe) {
                    askForPermission(Manifest.permission.READ_EXTERNAL_STORAGE,SELECT_PHOTO);
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(photoPickerIntent, SELECT_PHOTO);
                }
                else {
                    //TODO mostrar la foto en un visor?
                    Log.d(TAG, "onClick: show profile picture viewer");
                }
                break;

            case R.id.imageView_lock:
                lockView.setClickable(false);
                mPresenter.togglePrivacity();
                break;

            case R.id.imageView_chat:
                //TODO Comprobar en server si pueden hablar? (opt)

                Log.d(TAG, "onClick: chat");
                List<String> userIds = Arrays.asList(ActiveUser.getInstance().get_Id(), user.get_Id());
                GroupChannel.createChannelWithUserIds(userIds, true, new GroupChannel.GroupChannelCreateHandler() {
                    @Override
                    public void onResult(GroupChannel groupChannel, SendBirdException e) {
                        Log.d(TAG, "channel created");
                        if (e != null) {
                            Log.v(TAG, "Error creating channel : " + e.getMessage() );
                        }else{
                            Log.d(TAG, "onResult: Go chat");
                            Intent intent = new Intent(getApplicationContext(), ChatView.class);
                            intent.putExtra("channel_url", groupChannel.getUrl());
                            startActivity(intent);
                            Log.d(TAG, "onResult: Activity started");
                        }
                    }
                });



                break;

            case R.id.imageView_follow:
                followView.setClickable(false);
                mPresenter.toggleFollowing(userId);
                break;

            case R.id.inbox_container:
                startActivity(new Intent(context, PendingRequestsView.class));
                break;

            case R.id.textView_followers:
                Intent followersIntent = new Intent(context, FollowsView.class);
                followersIntent.putExtra(ID, user.get_Id());
                followersIntent.putExtra(LIST_TYPE, FOLLOWERS);
                startActivity(followersIntent);
                break;

            case R.id.textView_following:
                Intent followingIntent = new Intent(context, FollowsView.class);
                followingIntent.putExtra(ID, user.get_Id());
                followingIntent.putExtra(LIST_TYPE, FOLLOWINGS);
                startActivity(followingIntent);
                break;

            case R.id.textView_badges:
                Intent badgesIntent = new Intent(context, BadgesView.class);
                badgesIntent.putExtra(ID, user.get_Id());
                startActivity(badgesIntent);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_PHOTO && resultCode == RESULT_OK) {
            final Uri imageUri = data.getData();
            String realPath = getRealPathFromURI(imageUri);
            File imageFile = new File(realPath);
            ImageLoader.loadImageCircle(this, imageUri, pictureView);
            mPresenter.uploadAvatar(imageFile);
        }
    }

    @Override
    public void getFriendsEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getSuggestedEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getPopularEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getGroups(int page, IGenericFragment fragment) {
        if (groupsFragment == null) {
            groupsFragment = fragment;
        }
        if (user != null) groupsFragment.setData(user.getFollowingGroups());
        //TODO presenter + model
        //TODO allow paging requests
    }

    @Override
    public void getEvents(int page, IGenericFragment fragment) {
        if (eventsFragment == null) {
            eventsFragment = fragment;
        }
        if (user != null)  eventsFragment.setData(user.getEvents());
        //TODO presenter + model
        //TODO allow paging requests
    }

    @Override
    public void getFutureEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getPastEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getUsers(int page, IGenericFragment fragment) {

    }

    @Override
    public void getMapEvents(double lat, double lon, double distance, IMapFragment fragment) {

    }

    @Override
    public void getData(int page) {

    }
}

package com.hanana.app.profile.model;

import android.util.Log;

import com.hanana.app.DataModels.ComplaintObject;
import com.hanana.app.RESTInterface.ComplainsAPI;
import com.hanana.app.Utils.ActiveUser;
import com.hanana.app.DataModels.Biography;
import com.hanana.app.DataModels.OtherUser;
import com.hanana.app.RESTInterface.UsersAPI;
import com.hanana.app.Utils.Constants;
import com.hanana.app.profile.presenter.IProfilePresenter;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.hanana.app.Utils.Constants.USER;

public class ProfileModel implements IProfileModel {
    private static final String TAG ="ProfileModel";
    private IProfilePresenter mPresenter;
    private UsersAPI usersAPIService;
    private ComplainsAPI complainsAPIService;

    public ProfileModel(IProfilePresenter profilePresenter) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.mPresenter = profilePresenter;
        usersAPIService = retrofit.create(UsersAPI.class);
        complainsAPIService = retrofit.create(ComplainsAPI.class);
    }

    @Override
    public void uploadAvatar(File file) {
        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("avatar", file.getName(), requestFile);


        Call<ResponseBody> call = usersAPIService.uploadAvatar(body, ActiveUser.getInstance().getAccess_token());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int statusCode = response.code();
                if (statusCode < 300) {
                    ActiveUser current = ActiveUser.getInstance();
                    try {
                        current.setAvatar(response.body().string());
                        mPresenter.refreshAvatar();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                Log.v(TAG,"Upload: Status code " + statusCode + " body " + response.raw());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }

    @Override
    public void updateBio(final String bio) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<Void> call = usersAPIService.updateBio(accessToken, new Biography(bio));
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                int statusCode = response.code();
                if (statusCode < 300) {
                    Log.d(TAG, "onResponse: ok");
                }
                Log.v(TAG,"UpdateBio: Status code " + statusCode + " body " + response.raw());
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e("UpdateBio error:", t.getMessage());
            }
        });
    }

    @Override
    public void togglePrivacity() {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<Void> call = usersAPIService.togglePrivacity(accessToken);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                int statusCode = response.code();
                Log.v(TAG,"togglePrivacity: Status code " + statusCode + " body " + response.raw());
                if (statusCode < 300) {
                    mPresenter.togglePrivateButton();
                }
                else {
                    mPresenter.activatePrivateButton();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e("togglePrivacity error:", t.getMessage());
                mPresenter.activatePrivateButton();
            }
        });
    }

    @Override
    public void toggleFollowing(String userId) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<Void> call = usersAPIService.toggleFollowing(userId, accessToken);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                int statusCode = response.code();
                Log.v(TAG,"toggleFollowing: Status code " + statusCode + " body " + response.raw());
                if (statusCode < 300) {
                    mPresenter.toggleFollowingButton();
                }
                else {
                    mPresenter.activateFollowingButton();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e("toggleFollowing error:", t.getMessage());
                mPresenter.activateFollowingButton();
            }
        });
    }

    @Override
    public void getMe() {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        String userId = ActiveUser.getInstance().get_Id();
        Call<OtherUser> call = usersAPIService.getUser(userId, accessToken);
        call.enqueue(new Callback<OtherUser>() {
            @Override
            public void onResponse(Call<OtherUser> call, Response<OtherUser> response) {
                int statusCode = response.code();
                Log.v(TAG,"getMe: Status code " + statusCode + " body " + response.raw());
                if (statusCode < 300) {
                    OtherUser user = response.body();
                    Log.v(TAG, response.raw().toString());
                    mPresenter.displayMe(user);
                }
            }

            @Override
            public void onFailure(Call<OtherUser> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }

    @Override
    public void getUser(String userId) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<OtherUser> call = usersAPIService.getUser(userId, accessToken);
        call.enqueue(new Callback<OtherUser>() {
            @Override
            public void onResponse(Call<OtherUser> call, Response<OtherUser> response) {
                int statusCode = response.code();
                Log.v(TAG,"getUser: Status code " + statusCode + " body " + response.raw());
                if (statusCode < 300) {
                    OtherUser user = response.body();
                    Log.v(TAG, response.raw().toString());
                    mPresenter.displayUser(user);
                }
            }

            @Override
            public void onFailure(Call<OtherUser> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }

    @Override
    public void blockUser(String userId) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<Boolean> call = usersAPIService.blockUser(userId, accessToken);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                Boolean isBlocked = response.body();
                if(isBlocked) {
                    mPresenter.sendToHome();
                } else {
                    Log.d(TAG, "onResponse: was already blocked, now you unblocked him");
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.v(TAG,"Failure");
            }
        });
    }

    @Override
    public void complainUser(String userId, String comment) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<Void> call = complainsAPIService.complain(userId, USER, accessToken, new ComplaintObject(comment));
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                int statusCode = response.code();
                if (statusCode < 300) {
                    Log.d(TAG, "onResponse: ok");
                }
                Log.v(TAG,"complainUser: Status code " + statusCode + " body " + response.raw());
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.v(TAG,"Failure");
            }
        });
    }
}

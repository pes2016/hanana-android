package com.hanana.app.profile.model;

import java.io.File;

public interface IProfileModel {
    void uploadAvatar(File imageFile);
    void updateBio(String bio);
    void togglePrivacity();
    void toggleFollowing(String userId);
    void getUser(String userId);
    void getMe();
    void blockUser(String userId);
    void complainUser(String userId, String comment);
}

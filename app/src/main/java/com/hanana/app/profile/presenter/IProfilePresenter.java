package com.hanana.app.profile.presenter;

import com.hanana.app.DataModels.OtherUser;

import java.io.File;

public interface IProfilePresenter {
    void uploadAvatar(File imageFile);
    void updateBio(String bio);
    void refreshAvatar();
    void getMe();
    void displayMe(OtherUser user);
    void getUser(String userId);
    void displayUser(OtherUser user);
    void togglePrivacity();
    void togglePrivateButton();
    void activatePrivateButton();
    void toggleFollowing(String userId);
    void toggleFollowingButton();
    void activateFollowingButton();
    void blockUser(String userId);
    void sendToHome();
    void complainUser(String userId, String comment);
}

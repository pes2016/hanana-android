package com.hanana.app.Views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.hanana.app.R;
import com.hanana.app.Utils.ActiveUser;
import com.hanana.app.fullDiscover.view.FullDiscoverView;
import com.hanana.app.interests.view.IInterestsView;
import com.hanana.app.interests.view.InterestsView;

import static com.hanana.app.Utils.Constants.IS_CHOOSING;

public class FullInterestsView extends AppCompatActivity {
    private IInterestsView mInterestsView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_fragment);
        mInterestsView = InterestsView.newInstance(IS_CHOOSING, ActiveUser.getInstance().get_Id());
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.frame, (Fragment) mInterestsView, "interestsView")
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.interests_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_continue:
                mInterestsView.updateUserInterests();
                openHomeActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openHomeActivity() {
        Intent i = new Intent(getApplicationContext(),FullDiscoverView.class);
        startActivity(i);
    }
}

package com.hanana.app.Views;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.hanana.app.Utils.ActiveUser;
import com.hanana.app.R;
import com.hanana.app.RESTInterface.EventsAPI;
import com.hanana.app.Utils.Constants;
import com.hanana.app.event.view.EventView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.hanana.app.Utils.Constants.ID;
import static com.hanana.app.Utils.Constants.SCAN_ACTIVITY_NBR;

public class ScanHandler extends BaseActivity implements View.OnClickListener {
    private EventsAPI apiService;
    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Constants.SERVER_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    private TextView helpText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_handler);
        helpText = (TextView) findViewById(R.id.textview_scan);
        Button scan = (Button) findViewById(R.id.button_scan);
        scan.setOnClickListener(this);
    }

    @Override
    protected int whoIAm() {
        return SCAN_ACTIVITY_NBR;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Toast.makeText(this, getString(R.string.cancelled), Toast.LENGTH_LONG).show();
            } else {
                final String eventId = result.getContents();

                apiService = retrofit.create(EventsAPI.class);
                Call<Boolean> call = apiService.confirmQR(eventId, ActiveUser.getInstance().getAccess_token());
                call.enqueue(new Callback<Boolean>() {
                    @Override
                    public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                        Boolean isAttending = response.body();
                        if(response.code() > 300){
                            setErrorText();
                        }else if(!isAttending){
                            setUnavailableEvent();
                        }
                        else{
                            Intent intent = new Intent(getApplicationContext(), EventView.class);
                            intent.putExtra(ID, eventId);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onFailure(Call<Boolean> call, Throwable t) {
                        setErrorText();
                    }

                });
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onClick(View v) {
        Log.d("OC", "onClick: ");
        if(v.getId() == R.id.button_scan) {
            new IntentIntegrator(this).setCaptureActivity(ScanView.class).initiateScan();
        }
    }


    private void setErrorText() {
        helpText.setVisibility(View.VISIBLE);
        helpText.setText(getString(R.string.scan_error_string));
    }

    private void setUnavailableEvent() {
        helpText.setVisibility(View.VISIBLE);
        helpText.setText(getString(R.string.scan_event_unavailable));
    }
}

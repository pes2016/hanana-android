package com.hanana.app.Views;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.VisibleRegion;
import com.hanana.app.Adapters.IMapFragment;
import com.hanana.app.Adapters.OnFragmentRequestDataListener;
import com.hanana.app.DataModels.EventListItem;
import com.hanana.app.R;
import com.hanana.app.event.view.EventView;

import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.hanana.app.Utils.Constants.ID;


public class EventsMapView extends Fragment implements IMapFragment, OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnCameraIdleListener{
    private static final double EARTH_RADIUS = 3958.75; // Earth radius;
    private static final int METER_CONVERSION = 1609;

    private static final String TAG = "EventsMapView";
    private static final long DEBOUNCE_MS = 1000;

    private MapView mMapView;
    private GoogleMap mGoogleMap;
    private long mLastMovementTime = 0;
    private float mLastZoom = 13;
    private OnFragmentRequestDataListener mListener;

    public EventsMapView() {
        // Required empty public constructor
    }
    //TODO: Hide keyboard when map is shown

    public static EventsMapView newInstance() {
        return new EventsMapView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_events_map_view, container,
                false);
        mMapView = (MapView) v.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMapView.getMapAsync(this);
        return v;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "onMapReady: ");
        mGoogleMap = googleMap;
        double latitude = 41.38879;
        double longitude = 2.15899;

        mGoogleMap.setOnMarkerClickListener(this);
        mGoogleMap.setOnCameraIdleListener(this);
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,longitude), mLastZoom));
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Intent intent = new Intent(getApplicationContext(), EventView.class);
        intent.putExtra(ID, marker.getTitle());
        getActivity().startActivity(intent);
        return true;
    }


    public void displayEvents(List<EventListItem> l) {
        Log.d(TAG, "displayEvents: size " + l.size());
        mGoogleMap.clear();
        for (EventListItem e : l) {
            double latitude = Double.parseDouble(e.getLat());
            double longitude = Double.parseDouble(e.getLon());

            MarkerOptions marker = new MarkerOptions().position(
                    new LatLng(latitude, longitude)).title(e.get_Id());
            marker.icon(BitmapDescriptorFactory
                    .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
            mGoogleMap.addMarker(marker);
        }
    }

    @Override
    public void onCameraIdle() {
        float zoom = mGoogleMap.getCameraPosition().zoom;

        Log.d(TAG, "onCameraIdle: zoom is " + zoom);
        CameraPosition cameraPosition = mGoogleMap.getCameraPosition();
        long now = System.currentTimeMillis();
        if (now - mLastMovementTime > DEBOUNCE_MS) {
            mLastZoom = zoom;
            mLastMovementTime = System.currentTimeMillis();

            double lat = cameraPosition.target.latitude;
            double lon = cameraPosition.target.longitude;

            double dist_w = getDistance();

            mListener.getMapEvents(lat, lon, dist_w, this);
            Log.d(TAG, "onCameraIdle: LAT " + lat + " LON " + lon);

        } else {
            Log.d(TAG, "onCameraIdle: TOO FAST COWBOY");
        }
    }

    public double distanceFrom(double lat1, double lng1, double lat2, double lng2) {
        // Return distance between 2 points, stored as 2 pair location;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double dist = EARTH_RADIUS * c;
        return Double.valueOf(dist * METER_CONVERSION).floatValue();
    }

    @Override
    public LatLng getLatLng() {
        return mGoogleMap.getCameraPosition().target;
    }

    @Override
    public double getDistance() {
        VisibleRegion visibleRegion = mGoogleMap.getProjection().getVisibleRegion();
        LatLng nearLeft = visibleRegion.nearLeft;
        LatLng nearRight = visibleRegion.nearRight;
        LatLng farLeft = visibleRegion.farLeft;
        LatLng farRight = visibleRegion.farRight;
        double dist_w = distanceFrom(nearLeft.latitude, nearLeft.longitude, nearRight.latitude, nearRight.longitude);
        double dist_h = distanceFrom(farLeft.latitude, farLeft.longitude, farRight.latitude, farRight.longitude);
        double mean = (dist_w + dist_h) / 2.0;
        Log.d(TAG, "DISTANCE WIDTH: " + dist_w + " DISTANCE HEIGHT: " + dist_h + " AND RETURNING " + mean);
        return mean;
    }



    @Override
    public void onAttach(Context context) {
        Log.d(TAG, "onAttach: ");
        super.onAttach(context);
        if (context instanceof OnFragmentRequestDataListener) {
            mListener = (OnFragmentRequestDataListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentRequestDataListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}

package com.hanana.app.Views;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.hanana.app.Adapters.ChatListAdapter;
import com.hanana.app.R;
import com.sendbird.android.BaseChannel;
import com.sendbird.android.BaseMessage;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.GroupChannelListQuery;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;

import java.util.Arrays;
import java.util.List;

import static com.hanana.app.Utils.Constants.CHAT_LIST_ACTIVITY_NUMBER;

public class ChatListView extends BaseActivity {
    private static final String TAG = "ChatListView";
    private static final String identifier = "SendBirdGroupChannelList";
    private static final int REQUEST_INVITE_USERS = 100;
    private ListView mListView;
    private ChatListAdapter mAdapter;
    private GroupChannelListQuery mQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_chat_list);
        super.setNavigationItemClicked(whoIAm());
        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        /*toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.chat_list_title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/

        initializeUI();
    }

    private void initializeUI() {
        mListView = (ListView) findViewById(R.id.channelList);
        mListView.setAdapter(mAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                GroupChannel channel = mAdapter.getItem(position);
                Intent intent = new Intent(getApplicationContext(), ChatView.class);
                intent.putExtra("channel_url", channel.getUrl());
                startActivity(intent);
            }
        });

        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount >= (int) (totalItemCount * 0.8f)) {
                    loadNextChannels();
                }
            }
        });

        mAdapter = new ChatListAdapter(this);
        mListView.setAdapter(mAdapter);
    }

    private void loadNextChannels() {
        if (mQuery == null || mQuery.isLoading()) {
            return;
        }

        if (!mQuery.hasNext()) {
            return;
        }

        mQuery.next(new GroupChannelListQuery.GroupChannelListQueryResultHandler() {
            @Override
            public void onResult(List<GroupChannel> list, SendBirdException e) {
                if (e != null) {
                    Toast.makeText(getApplicationContext(), "" + e.getCode() + ":" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    return;
                }
                mAdapter.addAll(list);
                mAdapter.notifyDataSetChanged();

                if (mAdapter.getCount() == 0) {
                    Toast.makeText(getApplicationContext(), "No channels found.", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void create(final String[] userIds) {
        View view = getLayoutInflater().inflate(R.layout.sendbird_view_group_create_channel, null);
        final EditText chName = (EditText) view.findViewById(R.id.etxt_chname);
        final CheckBox distinct = (CheckBox) view.findViewById(R.id.chk_distinct);

        new AlertDialog.Builder(this)
                .setView(view)
                .setTitle("Create Group Channel")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        GroupChannel.createChannelWithUserIds(Arrays.asList(userIds), distinct.isChecked(), chName.getText().toString(), null, null, new GroupChannel.GroupChannelCreateHandler() {
                            @Override
                            public void onResult(GroupChannel groupChannel, SendBirdException e) {
                                if (e != null) {
                                    Toast.makeText(getApplicationContext(), "" + e.getCode() + ":" + e.getMessage(), Toast.LENGTH_SHORT).show();
                                    return;
                                }

                                mAdapter.replace(groupChannel);
                            }

                        });
                    }
                })
                .setNegativeButton("Cancel", null).create().show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_INVITE_USERS) {
                String[] userIds = data.getStringArrayExtra("user_ids");
                create(userIds);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        SendBird.removeChannelHandler(identifier);
    }

    @Override
    public void onResume() {
        super.onResume();

        SendBird.addChannelHandler(identifier, new SendBird.ChannelHandler() {
            @Override
            public void onMessageReceived(BaseChannel baseChannel, BaseMessage baseMessage) {
                if (baseChannel instanceof GroupChannel) {
                    GroupChannel groupChannel = (GroupChannel) baseChannel;
                    mAdapter.replace(groupChannel);
                }
            }

            @Override
            public void onUserJoined(GroupChannel groupChannel, User user) {
                // Member changed. Refresh group channel item.
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onUserLeft(GroupChannel groupChannel, User user) {
                // Member changed. Refresh group channel item.
                mAdapter.notifyDataSetChanged();
            }
        });


        mAdapter.clear();
        mAdapter.notifyDataSetChanged();

        mQuery = GroupChannel.createMyGroupChannelListQuery();
        mQuery.setIncludeEmpty(true);
        loadNextChannels();
    }




    @Override
    protected int whoIAm() {
        return CHAT_LIST_ACTIVITY_NUMBER;
    }
}

package com.hanana.app.Views;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hanana.app.R;
import com.hanana.app.Adapters.TabControllers.ListFragmentAdapter;

public class EventsListView extends Fragment {
    private AppCompatActivity myContext;

    public EventsListView() {
        // Required empty public constructor
    }

    public static EventsListView newInstance() {
        return new EventsListView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_events_list_view, container, false);
        TabLayout tabLayout = (TabLayout) rootview.findViewById(R.id.inner_tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.suggested)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.populars)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.friends)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) rootview.findViewById(R.id.inner_pager);
        final ListFragmentAdapter adapter = new ListFragmentAdapter
                (myContext, tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(2);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        return rootview;
    }

    @Override
    public void onAttach(Context context) {
        myContext = (AppCompatActivity) context;
        super.onAttach(context);
    }

}

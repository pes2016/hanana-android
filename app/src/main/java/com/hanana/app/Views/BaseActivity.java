package com.hanana.app.Views;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.hanana.app.R;
import com.hanana.app.Utils.ActiveUser;
import com.hanana.app.Utils.DataBase;
import com.hanana.app.Utils.ImageLoader;
import com.hanana.app.createEvent.view.CreateEventView;
import com.hanana.app.fullDiscover.view.FullDiscoverView;
import com.hanana.app.fullEvents.view.FullEventsView;
import com.hanana.app.login.view.LoginView;
import com.hanana.app.myEvents.view.MyEventsView;
import com.hanana.app.profile.view.ProfileView;
import com.hanana.app.ranking.view.RankingView;
import com.hanana.app.settings.view.SettingsView;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public abstract class BaseActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final Map<Integer, Class> viewsMap;
    static {
        Map<Integer, Class> aMap = new HashMap<>();
        aMap.put(R.id.nav_discover, FullDiscoverView.class);
        aMap.put(R.id.nav_events, FullEventsView.class);
        aMap.put(R.id.nav_my_events, MyEventsView.class);
        aMap.put(R.id.nav_ranking, RankingView.class);
        aMap.put(R.id.nav_qr, ScanHandler.class);

        aMap.put(R.id.nav_profile, ProfileView.class);
        aMap.put(R.id.nav_chat, ChatListView.class);
        aMap.put(R.id.nav_create, CreateEventView.class);

        aMap.put(R.id.nav_settings, SettingsView.class);
        aMap.put(R.id.nav_qr, ScanHandler.class);

        viewsMap = Collections.unmodifiableMap(aMap);
    }
    private DrawerLayout drawer;
    private NavigationView navigationView;

    @Override
    public void setContentView(int layoutResID)
    {
        DrawerLayout fullView = (DrawerLayout) getLayoutInflater().inflate(R.layout.activity_base, null);
        FrameLayout activityContainer = (FrameLayout) fullView.findViewById(R.id.content_base);


        getLayoutInflater().inflate(layoutResID, activityContainer, true);
        super.setContentView(fullView);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                InputMethodManager inputMethodManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        ImageView profilePic = (ImageView) headerView.findViewById(R.id.nav_profile_pic);
        TextView fullName = (TextView) headerView.findViewById(R.id.nav_full_name);
        ImageLoader.loadImageCircleNoFit(this, ActiveUser.getInstance().getAvatar(), profilePic);
        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ProfileView.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        fullName.setText(ActiveUser.getInstance().getName());
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id != R.id.nav_logout) {
            intentActivity(viewsMap.get(id), !item.isChecked());
        }
        else {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    setNavigationItemClicked(whoIAm());
                }
            });
            alertDialogBuilder.setTitle(R.string.logout);
            alertDialogBuilder
                    .setMessage(R.string.logout_message)
                    .setPositiveButton(R.string.close, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            closeSession();
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void intentActivity(Class view, boolean ok) {
        if (ok) {
            Intent intent = new Intent(getApplicationContext(), view);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    protected void setNavigationItemClicked(int position){
        navigationView.getMenu().getItem(position).setChecked(true);
    }

    @SuppressWarnings("WeakerAccess")
    public void closeSession() {
        DataBase bd = DataBase.getInstance();
        bd.deleteSession();
        ActiveUser.deleteSession();
        Intent anIntent = new Intent(getApplicationContext(), LoginView.class);
        anIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(anIntent);
        finish();
    }

    protected String getRealPathFromURI(Uri uri) {
        String result;
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) {
            result = uri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    protected void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);

            } else {

                ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
            }
        }
    }

    protected void hideKeyboard(){
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    protected abstract int whoIAm();
}

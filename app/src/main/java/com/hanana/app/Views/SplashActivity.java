package com.hanana.app.Views;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.hanana.app.DataModels.User;
import com.hanana.app.RESTInterface.UsersAPI;
import com.hanana.app.Utils.ActiveUser;
import com.hanana.app.Utils.Constants;
import com.hanana.app.Utils.DataBase;
import com.hanana.app.fullDiscover.view.FullDiscoverView;
import com.hanana.app.login.view.LoginView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SplashActivity extends AppCompatActivity {


    private UsersAPI apiService;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        context = this;

        DataBase bd = DataBase.getInstance();
        if (bd.activeSesion()) {
            Log.v("SplashActivity","activeSesion");
            String accessToken = bd.getAccessToken();
            this.apiService = retrofit.create(UsersAPI.class);
            Call<User> call = apiService.me(accessToken);
            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    int statusCode = response.code();
                    Log.v("SplashActivity","Status code " + statusCode + " body " + response.raw());
                    if (statusCode < 300) {
                        User user = response.body();
                        ActiveUser current = ActiveUser.getInstance();
                        current.create(user);
                        loadHome();
                    }
                    //TODO tratar caso especial statusCode 401 (unauthorized)
                    else {
                        loadLogin();
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    loadLogin();
                }
            });
        }
        else {
            loadLogin();
        }
    }

    private void loadLogin() {
        Intent intent = new Intent(context, LoginView.class);
        startActivity(intent);
        finish();
    }

    private void loadHome() {
        Intent intent = new Intent(context, FullDiscoverView.class);
        startActivity(intent);
        finish();
    }
}

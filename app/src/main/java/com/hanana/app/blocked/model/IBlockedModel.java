package com.hanana.app.blocked.model;

public interface IBlockedModel {
    void getBlocked(int page);
    void unBlock(String id, int position);
}

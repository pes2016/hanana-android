package com.hanana.app.blocked.presenter;

import com.hanana.app.DataModels.UsersList;
import com.hanana.app.blocked.model.BlockedModel;
import com.hanana.app.blocked.model.IBlockedModel;
import com.hanana.app.blocked.view.IBlockedView;

public class BlockedPresenter implements IBlockedPresenter {
    private IBlockedView mView;
    private IBlockedModel mModel;


    public BlockedPresenter(IBlockedView view) {
        super();
        this.mView = view;
        this.mModel = new BlockedModel(this);
    }

    @Override
    public void displayBlocked(UsersList users, int page) {
        mView.displayBlocked(users,page);
    }

    @Override
    public void getBlocked(int page) {
        mModel.getBlocked(page);
    }

    @Override
    public void removeBlocked(int position) {
        mView.removeBlocked(position);
    }

    @Override
    public void unBlock(String id, int position) {
        mModel.unBlock(id, position);
    }

}

package com.hanana.app.blocked.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.hanana.app.Adapters.Behaviors.BlockedBehavior;
import com.hanana.app.Adapters.Behaviors.BlockedListStartBehavior;
import com.hanana.app.Adapters.Behaviors.IStartBehavior;
import com.hanana.app.Adapters.GenericAdapter;
import com.hanana.app.Adapters.GenericFragment;
import com.hanana.app.Adapters.IGenericAdapter;
import com.hanana.app.Adapters.IGenericFragment;
import com.hanana.app.Adapters.IMapFragment;
import com.hanana.app.DataModels.UsersList;
import com.hanana.app.R;
import com.hanana.app.blocked.presenter.BlockedPresenter;
import com.hanana.app.blocked.presenter.IBlockedPresenter;

import static com.hanana.app.Utils.Constants.BLOCKED_HOLDER_TYPE;

public class BlockedView extends AppCompatActivity implements IBlockedView{
    public final static String TAG = "BlockedView";
    private IGenericFragment blockedFragment;
    private IBlockedPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toolbar_single_fragment);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mPresenter = new BlockedPresenter(this);

        IGenericAdapter adapter = new GenericAdapter(
                this,
                R.layout.blocked_item,
                BLOCKED_HOLDER_TYPE,
                new BlockedBehavior()
        );

        IStartBehavior blockedListStartBehavior = new BlockedListStartBehavior();

        blockedFragment = new GenericFragment();
        blockedFragment.initialize(adapter);
        blockedFragment.setStartBehavior(blockedListStartBehavior);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame, (Fragment) blockedFragment, "BlockedView")
                .commit();
    }

    @Override
    public void getData(int page) {
        mPresenter.getBlocked(page);
    }

    @Override
    public void displayBlocked(UsersList users, int page) {
        if(page != 0){
            blockedFragment.appendData(users.getUsers());
        }else{
            blockedFragment.setData(users.getUsers());
        }
    }

    @Override
    public void unBlock(String id, int position) {
        mPresenter.unBlock(id, position);
    }

    @Override
    public void removeBlocked(int position) {
        blockedFragment.removeItemAt(position);
    }

    @Override
    public void getSuggestedEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getPopularEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getFriendsEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getGroups(int page, IGenericFragment fragment) {

    }

    @Override
    public void getEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getFutureEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getPastEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getUsers(int page, IGenericFragment fragment) {

    }

    @Override
    public void getMapEvents(double lat, double lon, double distance, IMapFragment fragment) {

    }
}

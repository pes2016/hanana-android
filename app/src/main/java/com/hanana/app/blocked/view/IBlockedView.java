package com.hanana.app.blocked.view;

import com.hanana.app.Adapters.OnFragmentRequestDataListener;
import com.hanana.app.DataModels.UsersList;

public interface IBlockedView extends OnFragmentRequestDataListener{
    void unBlock(String id, int position);
    void displayBlocked(UsersList users, int page);
    void removeBlocked(int position);
}

package com.hanana.app.blocked.presenter;

import com.hanana.app.DataModels.UsersList;

public interface IBlockedPresenter {
    void unBlock(String id, int position);
    void getBlocked(int page);
    void displayBlocked(UsersList users, int page);
    void removeBlocked(int position);
}

package com.hanana.app.blocked.model;

import android.util.Log;

import com.hanana.app.DataModels.SearchObject;
import com.hanana.app.DataModels.UsersList;
import com.hanana.app.RESTInterface.UsersAPI;
import com.hanana.app.Utils.ActiveUser;
import com.hanana.app.Utils.Constants;
import com.hanana.app.blocked.presenter.IBlockedPresenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BlockedModel implements IBlockedModel{
    private static final String TAG = "BlockedModel";

    private IBlockedPresenter mPresenter;
    private UsersAPI apiService;

    public BlockedModel(IBlockedPresenter presenter) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mPresenter = presenter;
        apiService = retrofit.create(UsersAPI.class);
    }

    @Override
    public void getBlocked(final int page) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<UsersList> call = apiService.getBlocked(accessToken, new SearchObject(page));
        call.enqueue(new Callback<UsersList>() {
            @Override
            public void onResponse(Call<UsersList> call, Response<UsersList> response) {
                UsersList users = response.body();
                mPresenter.displayBlocked(users,page);
            }

            @Override
            public void onFailure(Call<UsersList> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }


    @Override
    public void unBlock(String id, final int position) {
        Call<Boolean> call = apiService.blockUser(id, ActiveUser.getInstance().getAccess_token());
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                int statusCode = response.code();
                Log.v(TAG,"onResponse unBlock: Status code " + statusCode);
                if (statusCode < 300) {
                    Boolean isBlocked = response.body();
                    if (!isBlocked) {
                        Log.d(TAG, "onResponse: unblocked");
                        mPresenter.removeBlocked(position);
                    }
                    else {
                        Log.d(TAG, "onResponse: wasn't blocked, now you blocked him");
                    }
                }
                else {
                    Log.d(TAG, "onResponse unBlock: not ok");
                }

            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
            }

        });
    }
}

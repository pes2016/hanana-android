package com.hanana.app.fullDiscover.view;

import com.hanana.app.Adapters.OnFragmentRequestDataListener;
import com.hanana.app.DataModels.GroupList;
import com.hanana.app.DataModels.UsersList;

public interface IFullDiscoverView extends OnFragmentRequestDataListener{
    void displayGroups(GroupList groups, int page);
    void displayUsers(UsersList users, int page);
}

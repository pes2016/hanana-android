package com.hanana.app.fullDiscover.presenter;

import com.hanana.app.DataModels.GroupList;
import com.hanana.app.DataModels.UsersList;
import com.hanana.app.fullDiscover.model.FullDiscoverModel;
import com.hanana.app.fullDiscover.model.IFullDiscoverModel;
import com.hanana.app.fullDiscover.view.IFullDiscoverView;

public class FullDiscoverPresenter implements IFullDiscoverPresenter {
    private IFullDiscoverView mView;
    private IFullDiscoverModel mModel;

    public FullDiscoverPresenter(IFullDiscoverView view) {
        this.mView = view;
        this.mModel = new FullDiscoverModel(this);
    }

    @Override
    public void getGroups(String text, int page) {
        mModel.searchGroups(text, page);
    }

    @Override
    public void displayGroups(GroupList groups, int page) {
        mView.displayGroups(groups,page);
    }

    @Override
    public void getUsers(String text, int page) {
        mModel.searchUsers(text, page);
    }

    @Override
    public void displayUsers(UsersList users, int page) {
        mView.displayUsers(users,page);
    }
}

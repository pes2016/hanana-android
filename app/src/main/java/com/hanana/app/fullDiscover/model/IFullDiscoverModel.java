package com.hanana.app.fullDiscover.model;

public interface IFullDiscoverModel {
    void searchGroups(String text, int page);
    void searchUsers(String text, int page);
}

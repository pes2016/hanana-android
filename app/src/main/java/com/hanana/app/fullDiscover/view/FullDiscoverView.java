package com.hanana.app.fullDiscover.view;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import com.hanana.app.Adapters.IGenericFragment;
import com.hanana.app.Adapters.IMapFragment;
import com.hanana.app.Adapters.TabControllers.UsersGroupsFragmentAdapter;
import com.hanana.app.DataModels.GroupList;
import com.hanana.app.DataModels.UsersList;
import com.hanana.app.R;
import com.hanana.app.Views.BaseActivity;
import com.hanana.app.fullDiscover.presenter.FullDiscoverPresenter;
import com.hanana.app.fullDiscover.presenter.IFullDiscoverPresenter;

import static com.hanana.app.Utils.Constants.DISCOVER_ACTIVITY_NBR;

public class FullDiscoverView extends BaseActivity implements IFullDiscoverView {
    private static final String TAG = "FullDiscoverView";
    private IFullDiscoverPresenter mPresenter;
    private IGenericFragment groupsFragment, usersFragment;
    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.search_view);
        super.setNavigationItemClicked(whoIAm());
        initialize(this);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.users)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.groups)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final UsersGroupsFragmentAdapter adapter = new UsersGroupsFragmentAdapter
                (this, tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void initialize(IFullDiscoverView view) {
        mPresenter = new FullDiscoverPresenter(view);
    }

    @Override
    protected int whoIAm() {
        return DISCOVER_ACTIVITY_NBR;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(item);

        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String newText) {
                performSearch(newText);
                return true;
            }

            public boolean onQueryTextSubmit(String query) {
                hideKeyboard();
                performSearch(query);
                return true;
            }

        };
        searchView.setOnQueryTextListener(queryTextListener);

        return true;
    }

    private void performSearch(String text) {
        mPresenter.getGroups(text, 0);
        mPresenter.getUsers(text, 0);
    }

    @Override
    public void getGroups(int page, IGenericFragment fragment) {
        if (groupsFragment == null) {
            groupsFragment = fragment;
        }

        String text = "";

        if (searchView != null) {
            text = searchView.getQuery().toString();
        }

        mPresenter.getGroups(text, page);
    }

    @Override
    public void displayGroups(GroupList groups, int page) {
        if (page != 0) {
            groupsFragment.appendData(groups.getGroups());
        } else {
            groupsFragment.setData(groups.getGroups());
        }
    }

    @Override
    public void getUsers(int page, IGenericFragment fragment) {
        if (usersFragment == null) {
            usersFragment = fragment;
        }
        String text = "";

        if (searchView != null) {
            text = searchView.getQuery().toString();
        }

        mPresenter.getUsers(text, page);
    }

    @Override
    public void displayUsers(UsersList users, int page) {
        if (page != 0) {
            usersFragment.appendData(users.getUsers());
        } else {
            usersFragment.setData(users.getUsers());
        }
    }

    @Override
    public void getFriendsEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getSuggestedEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getPopularEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getFutureEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getPastEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getMapEvents(double lat, double lon, double distance, IMapFragment fragment) {

    }

    @Override
    public void getData(int page) {

    }
}

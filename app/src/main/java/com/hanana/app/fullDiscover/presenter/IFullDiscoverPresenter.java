package com.hanana.app.fullDiscover.presenter;

import com.hanana.app.DataModels.GroupList;
import com.hanana.app.DataModels.UsersList;

public interface IFullDiscoverPresenter {
    void getGroups(String text, int page);
    void displayGroups(GroupList groups, int page);
    void getUsers(String text, int page);
    void displayUsers(UsersList users, int page);
}

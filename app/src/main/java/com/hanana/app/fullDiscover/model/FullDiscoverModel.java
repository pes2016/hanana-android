package com.hanana.app.fullDiscover.model;

import android.util.Log;

import com.hanana.app.Utils.ActiveUser;
import com.hanana.app.DataModels.GroupList;
import com.hanana.app.DataModels.SearchObject;
import com.hanana.app.DataModels.UsersList;
import com.hanana.app.RESTInterface.GroupsAPI;
import com.hanana.app.RESTInterface.UsersAPI;
import com.hanana.app.Utils.Constants;
import com.hanana.app.fullDiscover.presenter.IFullDiscoverPresenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FullDiscoverModel implements IFullDiscoverModel {
    private static final String TAG = "FullDiscoverModel";
    private IFullDiscoverPresenter mPresenter;
    private GroupsAPI apiServiceGroups;
    private UsersAPI apiServiceUsers;

    public FullDiscoverModel(IFullDiscoverPresenter fullDiscoverPresenter) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.mPresenter = fullDiscoverPresenter;
        this.apiServiceGroups = retrofit.create(GroupsAPI.class);
        this.apiServiceUsers = retrofit.create(UsersAPI.class);
    }

    @Override
    public void searchGroups(String text, final int page) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        SearchObject query = new SearchObject(text, page);
        Call<GroupList> call = apiServiceGroups.searchGroups(accessToken, query);
        call.enqueue(new Callback<GroupList>() {
            @Override
            public void onResponse(Call<GroupList> call, Response<GroupList> response) {
                GroupList groups = response.body();
                Log.v(TAG, response.raw().toString());
                mPresenter.displayGroups(groups,page);
            }

            @Override
            public void onFailure(Call<GroupList> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }

    @Override
    public void searchUsers(String text, final int page) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        SearchObject query = new SearchObject(text, page);
        Call<UsersList> call = apiServiceUsers.searchUsers(accessToken, query);
        call.enqueue(new Callback<UsersList>() {
            @Override
            public void onResponse(Call<UsersList> call, Response<UsersList> response) {
                UsersList users = response.body();
                Log.v(TAG, response.raw().toString());
                mPresenter.displayUsers(users, page);
            }

            @Override
            public void onFailure(Call<UsersList> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }
}

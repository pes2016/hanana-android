package com.hanana.app.fullEvents.view;

import com.hanana.app.Adapters.OnFragmentRequestDataListener;
import com.hanana.app.DataModels.EventsList;

public interface IFullEventsView extends OnFragmentRequestDataListener {
    void displayEvents(EventsList events, int type, int page);
}

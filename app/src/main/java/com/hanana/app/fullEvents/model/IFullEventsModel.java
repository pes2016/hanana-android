package com.hanana.app.fullEvents.model;

public interface IFullEventsModel {
    void getMapEvents(String text, double lat, double lon, double distance);
    void getSuggestedEvents(String text, int page);
    void getPopularEvents(String text, int page);
    void getFriendsEvents(String text, int page);
}

package com.hanana.app.fullEvents.view;

import android.support.design.widget.TabLayout;

import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.model.LatLng;
import com.hanana.app.Adapters.IGenericFragment;
import com.hanana.app.Adapters.IMapFragment;
import com.hanana.app.Views.BaseActivity;
import com.hanana.app.DataModels.EventsList;
import com.hanana.app.R;
import com.hanana.app.Adapters.TabControllers.ListMapFragmentAdapter;
import com.hanana.app.fullEvents.presenter.FullEventsPresenter;
import com.hanana.app.fullEvents.presenter.IFullEventsPresenter;

import static com.hanana.app.Utils.Constants.EVENTS_ACTIVITY_NBR;
import static com.hanana.app.Utils.Constants.FRIENDS;
import static com.hanana.app.Utils.Constants.MAP;
import static com.hanana.app.Utils.Constants.POPULAR;
import static com.hanana.app.Utils.Constants.SUGGESTED;

public class FullEventsView extends BaseActivity implements IFullEventsView {
    private static final String TAG = "FullEventsView";
    private IFullEventsPresenter mPresenter;
    private IGenericFragment suggestedFragment, popularFragment, friendsFragment;
    private IMapFragment mapFragment;
    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.search_view);
        super.setNavigationItemClicked(whoIAm());
        initialize(this);


        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.lists)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.map)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final ListMapFragmentAdapter adapter = new ListMapFragmentAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void initialize(IFullEventsView view) {
        mPresenter = new FullEventsPresenter(view);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(item);

        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String newText) {
                performSearch(newText);
                return true;
            }

            public boolean onQueryTextSubmit(String query) {
                hideKeyboard();
                performSearch(query);
                return true;
            }

        };
        searchView.setOnQueryTextListener(queryTextListener);

        return true;
    }

    @Override
    protected int whoIAm() {
        return EVENTS_ACTIVITY_NBR;
    }






    private void performSearch(String text){
        LatLng target = mapFragment.getLatLng();
        double distance = mapFragment.getDistance();

        mPresenter.getMapEvents(text, target.latitude, target.longitude, distance);
        mPresenter.getSuggestedEvents(text,0);
        mPresenter.getPopularEvents(text,0);
        mPresenter.getFriendsEvents(text,0);
    }

    @Override
    public void displayEvents(EventsList events, int type, int page) {
        switch (type) {
            case MAP:
                mapFragment.displayEvents(events.getEvents());
                break;
            case SUGGESTED:
                if (page != 0){
                    suggestedFragment.appendData(events.getEvents());

                }else{
                    suggestedFragment.setData(events.getEvents());
                }
                break;
            case POPULAR:
                if (page != 0){
                    popularFragment.appendData(events.getEvents());
                }else{
                    popularFragment.setData(events.getEvents());
                }
                break;

            case FRIENDS:
                if (page != 0){
                    friendsFragment.appendData(events.getEvents());
                }else{
                    friendsFragment.setData(events.getEvents());
                }
                break;
        }
    }

    @Override
    public void getSuggestedEvents(int page, IGenericFragment fragment) {
        if (suggestedFragment == null) {
            suggestedFragment = fragment;
        }
        String text = "";

        if(searchView != null){
            text = searchView.getQuery().toString();
        }
        mPresenter.getSuggestedEvents(text, page);
    }

    @Override
    public void getPopularEvents(int page, IGenericFragment fragment) {
        if (popularFragment == null) {
            popularFragment = fragment;
        }
        String text = "";

        if(searchView != null){
            text = searchView.getQuery().toString();
        }

        mPresenter.getPopularEvents(text, page);
    }

    @Override
    public void getFriendsEvents(int page, IGenericFragment fragment) {
        if (friendsFragment == null) {
            friendsFragment = fragment;
        }
        String text = "";

        if(searchView != null){
            text = searchView.getQuery().toString();
        }
        mPresenter.getFriendsEvents(text, page);
    }

    @Override
    public void getMapEvents(double lat, double lon, double distance, IMapFragment fragment) {
        if(mapFragment == null){
            mapFragment = fragment;
        }
        String text = "";

        if(searchView != null){
            text = searchView.getQuery().toString();
        }
        mPresenter.getMapEvents(text, lat, lon, distance);
    }

    @Override
    public void getData(int page) {

    }

    @Override
    public void getGroups(int page, IGenericFragment fragment) {

    }

    @Override
    public void getEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getFutureEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getPastEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getUsers(int page, IGenericFragment fragment) {

    }
}

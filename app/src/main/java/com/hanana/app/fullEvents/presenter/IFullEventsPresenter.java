package com.hanana.app.fullEvents.presenter;

import com.hanana.app.DataModels.EventsList;

public interface IFullEventsPresenter {

    void getMapEvents(String text, double lat, double lon, double distance);

    void loadEvents(EventsList events, int type, int page);

    void getSuggestedEvents(String text, int page);

    void getPopularEvents(String text, int page);

    void getFriendsEvents(String text, int page);
}

package com.hanana.app.fullEvents.model;

import android.util.Log;

import com.hanana.app.DataModels.MapSearchObject;
import com.hanana.app.Utils.ActiveUser;
import com.hanana.app.DataModels.EventsList;
import com.hanana.app.DataModels.SearchObject;
import com.hanana.app.RESTInterface.EventsAPI;
import com.hanana.app.Utils.Constants;
import com.hanana.app.fullEvents.presenter.IFullEventsPresenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.hanana.app.Utils.Constants.FRIENDS;
import static com.hanana.app.Utils.Constants.MAP;
import static com.hanana.app.Utils.Constants.POPULAR;
import static com.hanana.app.Utils.Constants.SUGGESTED;

public class FullEventsModel implements IFullEventsModel {
    private static final String TAG = "FullEventsModel";
    private IFullEventsPresenter mPresenter;
    private EventsAPI apiService;

    public FullEventsModel(IFullEventsPresenter fullEventsPresenter) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mPresenter = fullEventsPresenter;
        apiService = retrofit.create(EventsAPI.class);
    }

    @Override
    public void getMapEvents(String text, double lat, double lon, double distance) {
        Log.d(TAG, "getMapEvents: ");
        String accessToken = ActiveUser.getInstance().getAccess_token();
        MapSearchObject query = new MapSearchObject(text, lat, lon, distance);
        Call<EventsList> call = apiService.getMapEvents(accessToken, query);
        call.enqueue(new Callback<EventsList>() {
            @Override
            public void onResponse(Call<EventsList> call, Response<EventsList> response) {
                EventsList events = response.body();
                Log.v(TAG, response.raw().toString());
                mPresenter.loadEvents(events, MAP, 0);
            }

            @Override
            public void onFailure(Call<EventsList> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }

    @Override
    public void getSuggestedEvents(String text, final int page) {
        Log.d(TAG, "getSuggestedEvents: ");
        String accessToken = ActiveUser.getInstance().getAccess_token();
        SearchObject query = new SearchObject(text, page);
        Call<EventsList> call = apiService.getSuggestedEvents(accessToken, query);
        call.enqueue(new Callback<EventsList>() {
            @Override
            public void onResponse(Call<EventsList> call, Response<EventsList> response) {
                EventsList events = response.body();
                Log.v(TAG, response.raw().toString());
                mPresenter.loadEvents(events, SUGGESTED, page);
            }

            @Override
            public void onFailure(Call<EventsList> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }

    @Override
    public void getPopularEvents(String text, final int page) {
        Log.d(TAG, "getPopularEvents: ");
        String accessToken = ActiveUser.getInstance().getAccess_token();
        SearchObject query = new SearchObject(text, page);
        Call<EventsList> call = apiService.getPopularEvents(accessToken, query);
        call.enqueue(new Callback<EventsList>() {
            @Override
            public void onResponse(Call<EventsList> call, Response<EventsList> response) {
                EventsList events = response.body();
                Log.v(TAG, response.raw().toString());
                mPresenter.loadEvents(events, POPULAR, page);
            }

            @Override
            public void onFailure(Call<EventsList> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }

    @Override
    public void getFriendsEvents(String text, final int page) {
        Log.d(TAG, "getFriendsEvents: ");
        String accessToken = ActiveUser.getInstance().getAccess_token();
        SearchObject query = new SearchObject(text, page);
        Call<EventsList> call = apiService.getFriendsEvents(accessToken, query);
        call.enqueue(new Callback<EventsList>() {
            @Override
            public void onResponse(Call<EventsList> call, Response<EventsList> response) {
                EventsList events = response.body();
                Log.v(TAG, response.raw().toString());
                mPresenter.loadEvents(events, FRIENDS, page);
            }

            @Override
            public void onFailure(Call<EventsList> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }
}

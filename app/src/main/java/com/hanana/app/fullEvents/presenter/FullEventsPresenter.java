package com.hanana.app.fullEvents.presenter;

import com.hanana.app.DataModels.EventsList;
import com.hanana.app.fullEvents.model.FullEventsModel;
import com.hanana.app.fullEvents.model.IFullEventsModel;
import com.hanana.app.fullEvents.view.IFullEventsView;

public class FullEventsPresenter implements IFullEventsPresenter {
    private IFullEventsView mView;
    private IFullEventsModel mModel;

    public FullEventsPresenter(IFullEventsView view) {
        this.mView = view;
        this.mModel = new FullEventsModel(this);
    }

    @Override
    public void getMapEvents(String text, double lat, double lon, double distance) {
        mModel.getMapEvents(text, lat, lon, distance);
    }

    @Override
    public void loadEvents(EventsList events, int type, int page) {
        mView.displayEvents(events, type, page);
    }

    @Override
    public void getSuggestedEvents(String text, int page) {
        mModel.getSuggestedEvents(text, page);
    }

    @Override
    public void getPopularEvents(String text, int page) {
        mModel.getPopularEvents(text, page);
    }

    @Override
    public void getFriendsEvents(String text, int page) {
        mModel.getFriendsEvents(text, page);
    }
}

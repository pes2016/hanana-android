package com.hanana.app.myEvents.presenter;

import com.hanana.app.DataModels.EventsList;

public interface IMyEventsPresenter {
    void getEvents(int type, int page);
    void loadEvents(EventsList events, int type, int page);
}

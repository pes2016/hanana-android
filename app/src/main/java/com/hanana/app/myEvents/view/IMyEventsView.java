package com.hanana.app.myEvents.view;

import com.hanana.app.Adapters.OnFragmentRequestDataListener;
import com.hanana.app.DataModels.EventsList;
import com.hanana.app.DataModels.GroupList;
import com.hanana.app.DataModels.UsersList;

public interface IMyEventsView extends OnFragmentRequestDataListener{
    void displayEvents(EventsList events, int type, int page);
}

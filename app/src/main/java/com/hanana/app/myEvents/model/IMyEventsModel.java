package com.hanana.app.myEvents.model;

public interface IMyEventsModel {
    void getEvents(int type, int page);
}

package com.hanana.app.myEvents.presenter;

import com.hanana.app.DataModels.EventsList;
import com.hanana.app.myEvents.model.IMyEventsModel;
import com.hanana.app.myEvents.model.MyEventsModel;
import com.hanana.app.myEvents.view.IMyEventsView;

public class MyEventsPresenter implements IMyEventsPresenter {
    private IMyEventsView mView;
    private IMyEventsModel mModel;

    public MyEventsPresenter(IMyEventsView view) {
        this.mView = view;
        this.mModel = new MyEventsModel(this);
    }

    @Override
    public void getEvents(int type, int page) {
        mModel.getEvents(type, page);
    }

    @Override
    public void loadEvents(EventsList events, int type, int page) {
        mView.displayEvents(events, type, page);
    }
}

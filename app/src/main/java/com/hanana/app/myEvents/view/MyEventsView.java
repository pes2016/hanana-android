package com.hanana.app.myEvents.view;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.hanana.app.Adapters.IGenericFragment;
import com.hanana.app.Adapters.IMapFragment;
import com.hanana.app.Adapters.TabControllers.GroupFragmentAdapter;
import com.hanana.app.DataModels.EventsList;
import com.hanana.app.R;
import com.hanana.app.Views.BaseActivity;
import com.hanana.app.myEvents.presenter.IMyEventsPresenter;
import com.hanana.app.myEvents.presenter.MyEventsPresenter;

import static com.hanana.app.Utils.Constants.MY_EVENTS_ACTIVITY_NBR;
import static com.hanana.app.Utils.Constants.NEXT;
import static com.hanana.app.Utils.Constants.PAST;

public class MyEventsView extends BaseActivity implements IMyEventsView {
    private static final String TAG = "MyEventsView";
    private IMyEventsPresenter mPresenter;
    private IGenericFragment nextFragment, pastFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_my_events_view);
        super.setNavigationItemClicked(whoIAm());
        initialize(this);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.next_events)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.past_events)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final GroupFragmentAdapter adapter = new GroupFragmentAdapter
                (this, tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void initialize(IMyEventsView view) {
        mPresenter = new MyEventsPresenter(view);
    }

    @Override
    protected int whoIAm() {
        return MY_EVENTS_ACTIVITY_NBR;
    }


    @Override
    public void getFutureEvents(int page, IGenericFragment fragment) {
        if (nextFragment == null) {
            nextFragment = fragment;
        }
        mPresenter.getEvents(NEXT, page);
    }

    @Override
    public void getPastEvents(int page, IGenericFragment fragment) {
        if (pastFragment == null) {
            pastFragment = fragment;
        }
        mPresenter.getEvents(PAST, page);
    }

    @Override
    public void displayEvents(EventsList events, int type, int page) {
        if (type == NEXT) {
            if (page != 0) {
                nextFragment.appendData(events.getEvents());
            } else {
                nextFragment.setData(events.getEvents());
            }
        }
        else if (type == PAST) {
            if (page != 0) {
                pastFragment.appendData(events.getEvents());
            } else {
                pastFragment.setData(events.getEvents());
            }
        }
    }

    @Override
    public void getGroups(int page, IGenericFragment fragment) {

    }

    @Override
    public void getUsers(int page, IGenericFragment fragment) {

    }

    @Override
    public void getFriendsEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getSuggestedEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getPopularEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getMapEvents(double lat, double lon, double distance, IMapFragment fragment) {

    }

    @Override
    public void getData(int page) {

    }
}

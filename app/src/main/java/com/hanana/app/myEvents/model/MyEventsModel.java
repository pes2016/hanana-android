package com.hanana.app.myEvents.model;

import android.util.Log;

import com.hanana.app.DataModels.EventsList;
import com.hanana.app.DataModels.MapSearchObject;
import com.hanana.app.DataModels.SearchObject;
import com.hanana.app.RESTInterface.EventsAPI;
import com.hanana.app.Utils.ActiveUser;
import com.hanana.app.Utils.Constants;
import com.hanana.app.fullEvents.presenter.IFullEventsPresenter;
import com.hanana.app.myEvents.presenter.IMyEventsPresenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.hanana.app.Utils.Constants.FRIENDS;
import static com.hanana.app.Utils.Constants.FUTURE_EVENT;
import static com.hanana.app.Utils.Constants.MAP;
import static com.hanana.app.Utils.Constants.NEXT;
import static com.hanana.app.Utils.Constants.PAST_EVENT;
import static com.hanana.app.Utils.Constants.POPULAR;
import static com.hanana.app.Utils.Constants.SUGGESTED;

public class MyEventsModel implements IMyEventsModel {
    private static final String TAG = "MyEventsModel";
    private IMyEventsPresenter mPresenter;
    private EventsAPI apiService;

    public MyEventsModel(IMyEventsPresenter myEventsPresenter) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mPresenter = myEventsPresenter;
        apiService = retrofit.create(EventsAPI.class);
    }

    @Override
    public void getEvents(final int type, final int page) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        String id = ActiveUser.getInstance().get_Id();
        SearchObject query = new SearchObject(page);
        String url;
        if (type == NEXT) {
            url = FUTURE_EVENT;
        }
        else {
            url = PAST_EVENT;
        }
        Call<EventsList> call = apiService.getUserEvents(id, url, accessToken, query);
        call.enqueue(new Callback<EventsList>() {
            @Override
            public void onResponse(Call<EventsList> call, Response<EventsList> response) {
                EventsList events = response.body();
                Log.v(TAG, response.raw().toString());
                mPresenter.loadEvents(events, type, page);
            }

            @Override
            public void onFailure(Call<EventsList> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }
}

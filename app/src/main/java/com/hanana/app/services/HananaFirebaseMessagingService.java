package com.hanana.app.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonElement;
import com.hanana.app.DataModels.NotificationDAO;
import com.hanana.app.R;
import com.hanana.app.Utils.ActiveUser;
import com.hanana.app.Utils.NotificationParser;
import com.hanana.app.Views.ChatView;
import com.hanana.app.Views.SplashActivity;

/**
 * Created by marcos on 27/12/2016.
 */

public class HananaFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FireMessagingService";

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.d(TAG, "onTaskRemoved: FirebaseMessagingService");
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy: FirebaseMessagingService destroyed");
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "onMessageReceived: Message received");
        if (ActiveUser.getInstance().getPushNotifications()) {
            Log.d(TAG, "onMessageReceived: " + remoteMessage.getData().toString());
            sendNotification(NotificationParser.getPayload(remoteMessage));
        } else {
            Log.d(TAG, "onMessageReceived: Push Notifications disabled");
        }
    }

    private void sendNotification(NotificationDAO notification) {
        Notification builtNotification = buildNotification(notification);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, builtNotification);
    }

    private Notification buildNotification(NotificationDAO notification) {
        if (notification.isChat()) {
            Intent intent = new Intent(this, ChatView.class);
            intent.putExtra("channel_url", notification.getExtra("channel_url"));
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(notification.getTitle())
                    .setContentText(notification.getMessage())
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
            return notificationBuilder.build();
        } else {
            Intent intent = new Intent(this, SplashActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(notification.getTitle())
                    .setContentText(notification.getMessage())
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
            return notificationBuilder.build();
        }
    }

}

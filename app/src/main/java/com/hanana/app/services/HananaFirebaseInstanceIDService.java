package com.hanana.app.services;

import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;

/**
 * Created by marcos on 27/12/2016.
 */

public class HananaFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = "FireInstanceIDService";

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.d(TAG, "onTaskRemoved: FirebaseInstanceIdService");
    }


    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy: FirebaseInstanceIdService destroyed");
    }

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        SendBird.registerPushTokenForCurrentUser(refreshedToken, new SendBird.RegisterPushTokenWithStatusHandler() {
            @Override
            public void onRegistered(SendBird.PushTokenRegistrationStatus pushTokenRegistrationStatus, SendBirdException e) {
                if (e != null) {
                    Log.d(TAG, "onTokenRefresh - onRegistered SendBird: "  + e.getCode() + ":" + e.getMessage());
                    return;
                }

                if (pushTokenRegistrationStatus == SendBird.PushTokenRegistrationStatus.PENDING) {
                    Log.d(TAG, "onRegistered: " + "Connection required to register push token.");
                }
            }
        });
    }




}

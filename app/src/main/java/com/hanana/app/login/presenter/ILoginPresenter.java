package com.hanana.app.login.presenter;

import android.content.Intent;

import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;

public interface ILoginPresenter {
    void loginFacebook();
    void facebookCallBack(int requestCode, int resultCode, Intent data);
    void registerFromTwitter(TwitterAuthConfig authConfig, TwitterAuthToken authToken);
}

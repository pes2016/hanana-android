package com.hanana.app.login.view;

public interface ILoginView {

    void loadHomeActivity();

    void loadInterestsActivity();
}

package com.hanana.app.login.model;

import com.hanana.app.DataModels.User;

public interface ILoginModel {
    void saveSession(String access_token, String refresh_token);
    void saveSessionInThread(String access_token, String refresh_token);
    void loadActiveUser(User user);
}

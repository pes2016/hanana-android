package com.hanana.app.login.presenter;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.hanana.app.DataModels.FacebookUserData;
import com.hanana.app.DataModels.User;
import com.hanana.app.RESTInterface.UsersAPI;
import com.hanana.app.Utils.Constants;
import com.hanana.app.login.model.ILoginModel;
import com.hanana.app.login.model.LoginModel;
import com.hanana.app.login.view.ILoginView;
import com.twitter.sdk.android.core.OAuthSigning;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Collections;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.hanana.app.Utils.Constants.SERVER_REGISTER_TWITTER;
import static com.hanana.app.Utils.Constants.SERVER_URL;

public class LoginPresenter implements ILoginPresenter {
    private final static String TAG ="LoginPresenter";
    private ILoginView mView;
    private ILoginModel mModel;
    private CallbackManager callbackManager;

    private UsersAPI apiService;

    public LoginPresenter(ILoginView view) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.mView = view;
        this.mModel = new LoginModel(this);
        this.callbackManager = CallbackManager.Factory.create();
        this.apiService = retrofit.create(UsersAPI.class);

        LoginManager.getInstance().registerCallback(this.callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        AccessToken accessToken = loginResult.getAccessToken();
                        signUpIntoFacebook(accessToken.getToken(),accessToken.getUserId());
                    }
                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.v("Error", exception.getMessage().toLowerCase());
                    }
                });
    }


    private void signUpIntoFacebook(String token, String userId){
        FacebookUserData fb = new FacebookUserData(token,userId);
        Call<User> call = apiService.signUpFacebook(fb);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User currentUser = response.body();
                mModel.saveSession(currentUser.getAccess_token(),currentUser.getRefresh_token());
                mModel.loadActiveUser(currentUser);
                if(currentUser.isFirstLogin()) {
                    mView.loadInterestsActivity();
                }
                else {
                    mView.loadHomeActivity();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.v(TAG,"Error on facebook "  + t.getMessage());
            }
        });
    }

    public void registerFromTwitter(TwitterAuthConfig authConfig, TwitterAuthToken authToken){
        OAuthSigning oauthSigning = new OAuthSigning(authConfig, authToken);
        final Map<String, String> authHeaders = oauthSigning.getOAuthEchoHeadersForVerifyCredentials();

        //Body of your click handler
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Log.v(TAG, "THREAD CALL");

                URL url = null;
                try {
                    url = new URL(SERVER_URL + SERVER_REGISTER_TWITTER);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                HttpURLConnection connection = null;
                try {
                    assert url != null;
                    connection = (HttpURLConnection) url.openConnection();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    assert connection != null;
                    connection.setRequestMethod("GET");
                } catch (ProtocolException e) {
                    e.printStackTrace();
                }
                for (Map.Entry<String, String> entry : authHeaders.entrySet()) {
                    connection.setRequestProperty(entry.getKey(), entry.getValue());
                }

                try {
                    InputStream in = new BufferedInputStream(connection.getInputStream());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                    StringBuilder result = new StringBuilder();
                    String line;
                    while((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    JsonObject jsonObject = new JsonParser().parse(result.toString()).getAsJsonObject();
                    Gson gson = new Gson();
                    User currentUser = gson.fromJson(jsonObject.toString(), User.class);

                    mModel.saveSessionInThread(currentUser.getAccess_token(), currentUser.getRefresh_token());
                    mModel.loadActiveUser(currentUser);

                    boolean isFirstLogin = currentUser.isFirstLogin();
                    Log.d(TAG, "run: isfirstlogin " + isFirstLogin);
                    if(isFirstLogin ) {
                        mView.loadInterestsActivity();
                    }else{
                        mView.loadHomeActivity();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    connection.disconnect();
                }

            }

        });
        thread.start();
    }


    @Override
    public void loginFacebook() {
        LoginManager.getInstance().logInWithReadPermissions((Activity) mView, Collections.singletonList("public_profile"));
    }

    @Override
    public void facebookCallBack(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


}

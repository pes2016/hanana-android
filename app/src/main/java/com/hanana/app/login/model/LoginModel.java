package com.hanana.app.login.model;

import com.hanana.app.Utils.ActiveUser;
import com.hanana.app.Utils.DataBase;
import com.hanana.app.DataModels.User;
import com.hanana.app.login.presenter.ILoginPresenter;

public class LoginModel implements ILoginModel {
    private static final String TAG = "LoginModel";
    @SuppressWarnings("FieldCanBeLocal")
    private ILoginPresenter mPresenter;

    public LoginModel(ILoginPresenter loginPresenter) {
        this.mPresenter = loginPresenter;
    }

    @Override
    public void saveSession(String access_token, String refresh_token) {
        DataBase db = DataBase.getInstance();
        db.saveSession(access_token, refresh_token);
    }


    @Override
    public void saveSessionInThread(String access_token, String refresh_token) {
        DataBase db = DataBase.getInstance();
        db.saveSessionInThread(access_token, refresh_token);
     }

    public void loadActiveUser(User user) {
        ActiveUser current = ActiveUser.getInstance();
        current.create(user);
    }
}

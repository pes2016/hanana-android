package com.hanana.app.login.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.hanana.app.R;
import com.hanana.app.Views.FullInterestsView;
import com.hanana.app.fullDiscover.view.FullDiscoverView;
import com.hanana.app.login.presenter.ILoginPresenter;
import com.hanana.app.login.presenter.LoginPresenter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import static com.hanana.app.Utils.Constants.FACEBOOK_CODE;
import static com.hanana.app.Utils.Constants.TWITTER_CODE;

public class LoginView extends AppCompatActivity implements ILoginView {
    private final static String TAG = "LoginView";
    private TwitterLoginButton loginButtonTwitter;
    private ILoginPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_view);

        Button loginButtonFacebook = (Button) findViewById(R.id.facebookLoginButton);
        loginButtonFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.loginFacebook();
            }
        });

        loginButtonTwitter = (TwitterLoginButton) findViewById(R.id.twitterLoginButton);
        loginButtonTwitter.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                final TwitterSession session = result.data;

                //OAUTH THINGS
                TwitterAuthConfig authConfig = TwitterCore.getInstance().getAuthConfig();
                TwitterAuthToken authToken = session.getAuthToken();

                mPresenter.registerFromTwitter(authConfig, authToken);

            }

            @Override
            public void failure(TwitterException exception) {
                Log.d("TwitterKit", "Login with Twitter failure", exception);
            }
        });


        initialize(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FACEBOOK_CODE) {
            mPresenter.facebookCallBack(requestCode, resultCode, data);
        }
        else if (requestCode == TWITTER_CODE) {
            loginButtonTwitter.onActivityResult(requestCode, resultCode, data);
        }
        else {
            Log.d("requestCode", Integer.toString(requestCode));
        }
    }

    private void initialize(ILoginView view) {
        mPresenter = new LoginPresenter(view);
    }

    public void loadInterestsActivity() {
        Intent anIntent = new Intent(getApplicationContext(), FullInterestsView.class);
        startActivity(anIntent);
        this.finish();
    }

    public void loadHomeActivity() {
        Intent anIntent = new Intent(getApplicationContext(), FullDiscoverView.class);
        startActivity(anIntent);
        this.finish();
    }
}

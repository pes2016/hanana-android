package com.hanana.app.settings.presenter;

public interface ISettingsPresenter {
    void deleteAccount();
    void closeSession();
    void getBlocked();
    void setNumberBlocked(int blocked);
    void toggleNotifications();
    void toggleSwitch(Boolean enabled);
    void enableSwitch();
}

package com.hanana.app.settings.presenter;

import com.hanana.app.settings.model.ISettingsModel;
import com.hanana.app.settings.model.SettingsModel;
import com.hanana.app.settings.view.ISettingsView;

public class SettingsPresenter implements ISettingsPresenter {
    final static String TAG ="SettingsPresenter";
    private ISettingsView mView;
    private ISettingsModel mModel;

    public SettingsPresenter(ISettingsView mView) {
        this.mView = mView;
        this.mModel = new SettingsModel(this);
    }

    @Override
    public void deleteAccount() {
        mModel.deleteAccount();
    }

    @Override
    public void closeSession() {
        mView.closeSession();
    }

    @Override
    public void getBlocked() {
        mModel.getBlocked();
    }

    @Override
    public void setNumberBlocked(int blocked) {
        mView.setNumberBlocked(blocked);
    }

    @Override
    public void toggleNotifications() {
        mModel.toggleNotifications();
    }

    @Override
    public void enableSwitch() {
        mView.enableSwitch();
    }

    @Override
    public void toggleSwitch(Boolean enabled) {
        mView.toggleSwitch(enabled);
    }
}

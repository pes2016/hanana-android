package com.hanana.app.settings.model;

public interface ISettingsModel {
    void deleteAccount();
    void getBlocked();
    void toggleNotifications();
}

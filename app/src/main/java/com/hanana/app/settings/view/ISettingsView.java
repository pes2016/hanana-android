package com.hanana.app.settings.view;

public interface ISettingsView {
    void closeSession();
    void setNumberBlocked(int blocked);
    void enableSwitch();
    void toggleSwitch(Boolean checked);
}

package com.hanana.app.settings.model;

import android.util.Log;

import com.hanana.app.DataModels.SearchObject;
import com.hanana.app.DataModels.UsersList;
import com.hanana.app.Utils.ActiveUser;
import com.hanana.app.RESTInterface.UsersAPI;
import com.hanana.app.Utils.Constants;
import com.hanana.app.settings.presenter.ISettingsPresenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SettingsModel implements ISettingsModel {
    private static final String TAG = "SettingsModel";
    private ISettingsPresenter mPresenter;
    private UsersAPI apiService;

    public SettingsModel(ISettingsPresenter settingsPresenter) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.mPresenter = settingsPresenter;
        this.apiService = retrofit.create(UsersAPI.class);
    }

    @Override
    public void deleteAccount() {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<Void> call = apiService.closeAccount(accessToken);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                int statusCode = response.code();
                Log.v(TAG,"Status code " + statusCode + " body " + response.raw());
                if (statusCode < 300) {
                    mPresenter.closeSession();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.v(TAG,"Error on closeAccount"  + t.getMessage());
            }
        });
    }

    @Override
    public void getBlocked() {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<UsersList> call = apiService.getBlocked(accessToken, new SearchObject(-1));
        call.enqueue(new Callback<UsersList>() {
            @Override
            public void onResponse(Call<UsersList> call, Response<UsersList> response) {
                int statusCode = response.code();
                Log.v(TAG,"Status code " + statusCode + " body " + response.raw());
                int blocked = response.body().size();
                if (statusCode < 300) {
                    mPresenter.setNumberBlocked(blocked);
                }
            }

            @Override
            public void onFailure(Call<UsersList> call, Throwable t) {
                Log.v(TAG,"Error on getBlocked"  + t.getMessage());
            }
        });
    }

    @Override
    public void toggleNotifications() {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<Boolean> call = apiService.toggleNotifications(accessToken);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                int statusCode = response.code();
                if (statusCode < 300) {
                    Boolean enabled = response.body();
                    mPresenter.toggleSwitch(enabled);
                }
                else {
                    mPresenter.enableSwitch();
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.v(TAG,"Error on toggleNotifications"  + t.getMessage());
            }
        });
    }
}

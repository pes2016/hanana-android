package com.hanana.app.settings.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.hanana.app.R;
import com.hanana.app.Utils.ActiveUser;
import com.hanana.app.Views.BaseActivity;
import com.hanana.app.blocked.view.BlockedView;
import com.hanana.app.settings.presenter.ISettingsPresenter;
import com.hanana.app.settings.presenter.SettingsPresenter;

import static com.hanana.app.Utils.Constants.SETTINGS_ACTIVITY_NBR;

public class SettingsView extends BaseActivity implements ISettingsView, View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private final static String TAG = "SettingsView";
    private ISettingsPresenter mPresenter;
    private Activity context;
    private TextView blockedUsers;
    private Switch switchButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_settings_view);
        super.setNavigationItemClicked(whoIAm());
        initialize(this);

        context = this;

        findViewById(R.id.delete_account).setOnClickListener(this);
        blockedUsers = (TextView) findViewById(R.id.settings_blocked);
        switchButton = (Switch) findViewById(R.id.settings_switch);
        switchButton.setChecked(ActiveUser.getInstance().getPushNotifications());
        switchButton.setOnCheckedChangeListener(this);
    }

    private void initialize(ISettingsView view){
        mPresenter = new SettingsPresenter(view);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.getBlocked();
    }

    @Override
    protected int whoIAm() {
        return SETTINGS_ACTIVITY_NBR;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.delete_account:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle(R.string.close_account);
                alertDialogBuilder
                        .setMessage(R.string.close_account_message)
                        .setPositiveButton(R.string.close_account_ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mPresenter.deleteAccount();
                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                break;

            case R.id.settings_blocked:
                startActivity(new Intent(getApplicationContext(), BlockedView.class));
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switchButton.setEnabled(false);
        mPresenter.toggleNotifications();
    }

    @Override
    public void setNumberBlocked(int blocked) {
        blockedUsers.setText(getString(R.string.blocked_users) + " " + blocked);
        if (blocked != 0) {
            blockedUsers.setOnClickListener(this);
        }
    }

    @Override
    public void enableSwitch() {
        switchButton.setEnabled(true);
        switchButton.setOnCheckedChangeListener(null);
        switchButton.setChecked(!switchButton.isChecked());
        switchButton.setOnCheckedChangeListener(this);
    }

    @Override
    public void toggleSwitch(Boolean checked) {
        switchButton.setEnabled(true);
        switchButton.setOnCheckedChangeListener(null);
        switchButton.setChecked(checked);
        switchButton.setOnCheckedChangeListener(this);
        ActiveUser.getInstance().setPushNotifications(checked);
    }
}

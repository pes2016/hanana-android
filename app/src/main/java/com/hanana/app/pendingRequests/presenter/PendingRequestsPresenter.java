package com.hanana.app.pendingRequests.presenter;

import com.hanana.app.DataModels.UsersList;
import com.hanana.app.pendingRequests.model.IPendingRequestsModel;
import com.hanana.app.pendingRequests.model.PendingRequestsModel;
import com.hanana.app.pendingRequests.view.IPendingRequestsView;

public class PendingRequestsPresenter implements IPendingRequestsPresenter{
    private IPendingRequestsView mView;
    private IPendingRequestsModel mModel;


    public PendingRequestsPresenter(IPendingRequestsView view) {
        super();
        this.mView = view;
        this.mModel = new PendingRequestsModel(this);
    }


    @Override
    public void displayRequests(UsersList users, int page) {
        mView.displayRequests(users,page);
    }

    @Override
    public void declineRequest(String id, int position) {
        mModel.declineRequest(id, position);
    }

    @Override
    public void acceptRequest(String id, int position) {
        mModel.acceptRequest(id, position);
    }

    @Override
    public void getPendingRequests(int page) {
        mModel.getPendingRequests(page);
    }

    @Override
    public void removeRequest(int position) {
        mView.removeRequests(position);
    }
}

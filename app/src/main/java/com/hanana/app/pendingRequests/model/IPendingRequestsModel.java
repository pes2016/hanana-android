package com.hanana.app.pendingRequests.model;

public interface IPendingRequestsModel{
    void getPendingRequests(int page);
    void acceptRequest(String id, int position);
    void declineRequest(String id, int position);
}

package com.hanana.app.pendingRequests.model;

import android.util.Log;

import com.hanana.app.DataModels.SearchObject;
import com.hanana.app.Utils.ActiveUser;
import com.hanana.app.DataModels.UsersList;
import com.hanana.app.RESTInterface.UsersAPI;
import com.hanana.app.Utils.Constants;
import com.hanana.app.pendingRequests.presenter.IPendingRequestsPresenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PendingRequestsModel implements IPendingRequestsModel{
    private static final String TAG = "PendingRequestsModel";
    private IPendingRequestsPresenter mPresenter;
    private UsersAPI apiService;

    public PendingRequestsModel(IPendingRequestsPresenter presenter) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mPresenter = presenter;
        this.apiService = retrofit.create(UsersAPI.class);
    }

    public void getPendingRequests(final int page) {
        Call<UsersList> call = apiService.getPendingRequests(ActiveUser.getInstance().getAccess_token(), new SearchObject(page));
        call.enqueue(new Callback<UsersList>() {
            @Override
            public void onResponse(Call<UsersList> call, Response<UsersList> response) {
                UsersList list = response.body();
                Log.d("getpendingrequest", "onResponse: " + response.code());
                mPresenter.displayRequests(list,page);
            }

            @Override
            public void onFailure(Call<UsersList> call, Throwable t) {
                Log.v("getpendinrequest","Failure");
            }

        });

    }

    @Override
    public void acceptRequest(String id, final int position) {
        Call<Void> call = apiService.acceptRequest(id, ActiveUser.getInstance().getAccess_token());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                int statusCode = response.code();
                Log.v(TAG,"onResponse acceptRequest: Status code " + statusCode);
                if (statusCode < 300) {
                    mPresenter.removeRequest(position);
                }
                else {
                    Log.d(TAG, "onResponse acceptRequest: not ok");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
            }

        });
    }

    @Override
    public void declineRequest(String id, final int position) {
        Call<Void> call = apiService.declineRequest(id, ActiveUser.getInstance().getAccess_token());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                int statusCode = response.code();
                Log.v(TAG,"onResponse declineRequest: Status code " + statusCode);
                if (statusCode < 300) {
                    mPresenter.removeRequest(position);
                }
                else {
                    Log.d(TAG, "onResponse declineRequest: not ok");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
            }

        });
    }
}

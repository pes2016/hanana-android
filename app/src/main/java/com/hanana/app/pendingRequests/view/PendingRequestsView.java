package com.hanana.app.pendingRequests.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.hanana.app.Adapters.Behaviors.IStartBehavior;
import com.hanana.app.Adapters.Behaviors.RequestBehavior;
import com.hanana.app.Adapters.Behaviors.RequestListStartBehavior;
import com.hanana.app.Adapters.GenericAdapter;
import com.hanana.app.Adapters.GenericFragment;
import com.hanana.app.Adapters.IGenericAdapter;
import com.hanana.app.Adapters.IGenericFragment;
import com.hanana.app.Adapters.IMapFragment;
import com.hanana.app.DataModels.UsersList;
import com.hanana.app.R;
import com.hanana.app.pendingRequests.presenter.IPendingRequestsPresenter;
import com.hanana.app.pendingRequests.presenter.PendingRequestsPresenter;

import static com.hanana.app.Utils.Constants.REQUESTS_HOLDER_TYPE;

public class PendingRequestsView extends AppCompatActivity implements IPendingRequestsView{
    private final static String TAG = "PendingRequestsView";
    private IGenericFragment requestsFragment;
    private IPendingRequestsPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toolbar_single_fragment);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mPresenter = new PendingRequestsPresenter(this);

        IGenericAdapter adapter = new GenericAdapter(
                this,
                R.layout.request_item,
                REQUESTS_HOLDER_TYPE,
                new RequestBehavior()
        );

        IStartBehavior requestListStartBehavior = new RequestListStartBehavior();

        requestsFragment = new GenericFragment();
        requestsFragment.initialize(adapter);
        requestsFragment.setStartBehavior(requestListStartBehavior);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame, (Fragment) requestsFragment, "pendingRequestsView")
                .commit();
    }


    @Override
    public void getData(int page) {
        mPresenter.getPendingRequests(page);
    }

    @Override
    public void displayRequests(UsersList usersList, int page) {
        if(page != 0){
            requestsFragment.appendData(usersList.getUsers());
        }else{
            requestsFragment.setData(usersList.getUsers());
        }
    }

    @Override
    public void declineRequest(String id, int position) {
        mPresenter.declineRequest(id, position);
    }

    @Override
    public void acceptRequest(String id, int position) {
        mPresenter.acceptRequest(id, position);
    }

    @Override
    public void removeRequests(int position) {
        requestsFragment.removeItemAt(position);
    }

    @Override
    public void getSuggestedEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getPopularEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getFriendsEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getGroups(int page, IGenericFragment fragment) {

    }

    @Override
    public void getEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getFutureEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getPastEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getUsers(int page, IGenericFragment fragment) {

    }

    @Override
    public void getMapEvents(double lat, double lon, double distance, IMapFragment fragment) {

    }
}

package com.hanana.app.pendingRequests.presenter;

import com.hanana.app.DataModels.UsersList;

public interface IPendingRequestsPresenter {
    void getPendingRequests(int page);
    void displayRequests(UsersList users, int page);
    void declineRequest(String id, int position);
    void acceptRequest(String id, int position);
    void removeRequest(int position);
}

package com.hanana.app.pendingRequests.view;

import com.hanana.app.Adapters.OnFragmentRequestDataListener;
import com.hanana.app.DataModels.UsersList;

public interface IPendingRequestsView extends OnFragmentRequestDataListener {
    void displayRequests(UsersList users, int page);
    void declineRequest(String id, int position);
    void acceptRequest(String id, int position);
    void removeRequests(int position);
}

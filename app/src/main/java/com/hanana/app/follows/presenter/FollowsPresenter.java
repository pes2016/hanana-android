package com.hanana.app.follows.presenter;

import com.hanana.app.DataModels.User;
import com.hanana.app.follows.model.FollowsModel;
import com.hanana.app.follows.model.IFollowsModel;
import com.hanana.app.follows.view.IFollowsView;

import java.util.List;

import static com.hanana.app.Utils.Constants.EVENTS;
import static com.hanana.app.Utils.Constants.FOLLOWERS;
import static com.hanana.app.Utils.Constants.FOLLOWINGS;
import static com.hanana.app.Utils.Constants.GROUPS;


public class FollowsPresenter implements IFollowsPresenter{
    public final static String TAG = "FollowsPresenter";
    private IFollowsModel mModel;
    private IFollowsView mView;
    private int mWhich;

    public FollowsPresenter(IFollowsView view, int which, String entityId) {
        this.mModel = new FollowsModel(this, entityId);
        mView = view;
        mWhich = which;
    }

    @Override
    public void getData(int page) {
        switch (mWhich) {
            case FOLLOWERS:
                mModel.getUserFollowers(page);
                break;

            case FOLLOWINGS:
                mModel.getUserFollowings(page);
                break;

            case EVENTS:
                mModel.getEventAssistants(page);
                break;

            case GROUPS:
                mModel.getGroupFollowers(page);
                break;
        }
    }

    @Override
    public void displayUsers(List<User> users, int page) {
        mView.displayUsers(users,page);
    }

    @Override
    public void toggleFollowing(String userId, int position) {
        mModel.toggleFollowing(userId, position);
    }

    @Override
    public void toggleFollowButton(int position) {
        mView.toggleFollowButton(position);
    }
}

package com.hanana.app.follows.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.hanana.app.Adapters.Behaviors.FollowListStartBehavior;
import com.hanana.app.Adapters.Behaviors.FollowUserBehavior;
import com.hanana.app.Adapters.Behaviors.IStartBehavior;
import com.hanana.app.Adapters.GenericAdapter;
import com.hanana.app.Adapters.GenericFragment;
import com.hanana.app.Adapters.IGenericAdapter;
import com.hanana.app.Adapters.IGenericFragment;
import com.hanana.app.Adapters.IMapFragment;
import com.hanana.app.DataModels.User;
import com.hanana.app.R;
import com.hanana.app.follows.presenter.FollowsPresenter;
import com.hanana.app.follows.presenter.IFollowsPresenter;

import java.util.List;

import static com.hanana.app.Utils.Constants.EVENTS;
import static com.hanana.app.Utils.Constants.FOLLOWERS;
import static com.hanana.app.Utils.Constants.FOLLOWINGS;
import static com.hanana.app.Utils.Constants.FOLLOWS_HOLDER_TYPE;
import static com.hanana.app.Utils.Constants.GROUPS;
import static com.hanana.app.Utils.Constants.ID;
import static com.hanana.app.Utils.Constants.LIST_TYPE;

public class FollowsView extends AppCompatActivity implements IFollowsView {
    private IGenericFragment followsFragment;
    private IFollowsPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toolbar_single_fragment);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Bundle extras = getIntent().getExtras();
        String entityId = "";
        int which = FOLLOWERS;
        if (extras != null) {
            entityId = extras.getString(ID);
            which = extras.getInt(LIST_TYPE);
        }
        if (which == FOLLOWERS || which == GROUPS) {
            setTitle(getString(R.string.followers));
        }
        else if (which == FOLLOWINGS) {
            setTitle(getString(R.string.following));
        }
        else if (which == EVENTS) {
            setTitle(getString(R.string.assistants));
        }

        mPresenter = new FollowsPresenter(this, which, entityId);

        IGenericAdapter adapter = new GenericAdapter(
            this,
            R.layout.follow_item,
            FOLLOWS_HOLDER_TYPE,
            new FollowUserBehavior()
        );

        IStartBehavior followListStartBehavior = new FollowListStartBehavior(which);

        followsFragment = new GenericFragment();
        followsFragment.initialize(adapter);
        followsFragment.setStartBehavior(followListStartBehavior);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame, (Fragment) followsFragment, "followsView")
                .commit();
    }


    @Override
    public void getData(int page) {
        mPresenter.getData(page);
    }

    @Override
    public void displayUsers(List<User> users, int page) {
        if(page != 0){
            followsFragment.appendData(users);
        }
        else{
            followsFragment.setData(users);
        }
    }

    @Override
    public void toggleFollowing(String userId, int position) {
        mPresenter.toggleFollowing(userId, position);
    }

    @Override
    public void toggleFollowButton(int position) {
        followsFragment.onServerResponse(position);
    }

    @Override
    public void getSuggestedEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getPopularEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getFriendsEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getGroups(int page, IGenericFragment fragment) {

    }

    @Override
    public void getEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getFutureEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getPastEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getUsers(int page, IGenericFragment fragment) {

    }

    @Override
    public void getMapEvents(double lat, double lon, double distance, IMapFragment fragment) {

    }
}

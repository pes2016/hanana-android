package com.hanana.app.follows.model;

public interface IFollowsModel {
    void getUserFollowers(int userId);
    void getUserFollowings(int userId);
    void getEventAssistants(int page);
    void getGroupFollowers(int page);
    void toggleFollowing(String userId, int position);
}

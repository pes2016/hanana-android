package com.hanana.app.follows.view;

import com.hanana.app.Adapters.OnFragmentRequestDataListener;
import com.hanana.app.DataModels.User;

import java.util.List;


public interface IFollowsView extends OnFragmentRequestDataListener {
    void displayUsers(List<User> users, int page);
    void toggleFollowing(String userId, int position);
    void toggleFollowButton(int position);
}

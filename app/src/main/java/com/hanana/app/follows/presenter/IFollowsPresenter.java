package com.hanana.app.follows.presenter;

import com.hanana.app.DataModels.User;

import java.util.List;

public interface IFollowsPresenter {
    void displayUsers(List<User> users, int page);
    void toggleFollowing(String userId, int position);
    void toggleFollowButton(int position);
    void getData(int page);
}

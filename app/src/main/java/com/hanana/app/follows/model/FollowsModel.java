package com.hanana.app.follows.model;

import android.util.Log;

import com.hanana.app.DataModels.SearchObject;
import com.hanana.app.RESTInterface.EventsAPI;
import com.hanana.app.RESTInterface.GroupsAPI;
import com.hanana.app.RESTInterface.UsersAPI;
import com.hanana.app.Utils.ActiveUser;
import com.hanana.app.DataModels.UsersList;
import com.hanana.app.Utils.Constants;
import com.hanana.app.follows.presenter.IFollowsPresenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FollowsModel implements IFollowsModel{
    private static final String TAG = "FollowsModel";
    private UsersAPI usersAPIService;
    private EventsAPI eventsAPIService;
    private GroupsAPI groupsAPIService;
    private IFollowsPresenter mPresenter;
    private String mEntityId;

    public FollowsModel(IFollowsPresenter presenter, String entityId) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        usersAPIService = retrofit.create(UsersAPI.class);
        eventsAPIService = retrofit.create(EventsAPI.class);
        groupsAPIService = retrofit.create(GroupsAPI.class);
        mPresenter = presenter;
        mEntityId = entityId;
    }

    @Override
    public void getUserFollowers(final int page) {
        String accessToken = ActiveUser.getInstance().getAccess_token();

        Call<UsersList> call = usersAPIService.getFollowers(mEntityId, accessToken,new SearchObject(page));
        call.enqueue(new Callback<UsersList>() {
            @Override
            public void onResponse(Call<UsersList> call, Response<UsersList> response) {
                UsersList usersList = response.body();
                mPresenter.displayUsers(usersList.getUsers(), page);
            }

            @Override
            public void onFailure(Call<UsersList> call, Throwable t) {
                Log.v(TAG,"getUserFollowers Failure: " + t.getMessage());
            }
        });
    }

    @Override
    public void getUserFollowings(final int page) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<UsersList> call = usersAPIService.getFollowing(mEntityId, accessToken,new SearchObject(page));
        call.enqueue(new Callback<UsersList>() {
            @Override
            public void onResponse(Call<UsersList> call, Response<UsersList> response) {
                UsersList usersList = response.body();
                mPresenter.displayUsers(usersList.getUsers(),page);
            }

            @Override
            public void onFailure(Call<UsersList> call, Throwable t) {
                Log.v(TAG,"getUserFollowings Failure: " + t.getMessage());
            }
        });
    }

    @Override
    public void getEventAssistants(final int page) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<UsersList> call = eventsAPIService.getAssistants(mEntityId, accessToken, new SearchObject(page));
        call.enqueue(new Callback<UsersList>() {
            @Override
            public void onResponse(Call<UsersList> call, Response<UsersList> response) {
                int statusCode = response.code();
                UsersList usersList = response.body();
                mPresenter.displayUsers(usersList.getUsers(), page);
            }

            @Override
            public void onFailure(Call<UsersList> call, Throwable t) {
                Log.v(TAG,"getEventAssistants Failure: " + t.getMessage());
            }
        });
    }

    @Override
    public void getGroupFollowers(final int page) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<UsersList> call = groupsAPIService.getFollowers(mEntityId, accessToken, new SearchObject(page));
        call.enqueue(new Callback<UsersList>() {
            @Override
            public void onResponse(Call<UsersList> call, Response<UsersList> response) {
                int statusCode = response.code();
                UsersList usersList = response.body();
                mPresenter.displayUsers(usersList.getUsers(), page);
            }

            @Override
            public void onFailure(Call<UsersList> call, Throwable t) {
                Log.v(TAG,"getGroupFollowers Failure: " + t.getMessage());
            }
        });
    }

    @Override
    public void toggleFollowing(String userId, final int position) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<Void> call = usersAPIService.toggleFollowing(userId, accessToken);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                int statusCode = response.code();
                Log.v(TAG,"toggleFollowing: Status code " + statusCode + " body " + response.raw());
                if (statusCode < 300) {
                    mPresenter.toggleFollowButton(position);
                }
                else {
                    Log.d(TAG, "onResponse: not ok");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.v(TAG,"toggleFollowing Failure: " + t.getMessage());
            }

        });
    }
}

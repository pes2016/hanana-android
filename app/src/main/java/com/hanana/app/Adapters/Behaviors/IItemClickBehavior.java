package com.hanana.app.Adapters.Behaviors;

import android.content.Context;
import android.view.View;

public interface IItemClickBehavior {
    void execute(View view, Object o, int position, Context context);
}

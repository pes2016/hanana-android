package com.hanana.app.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hanana.app.DataModels.Interest;
import com.hanana.app.R;
import com.hanana.app.Utils.ImageLoader;
import com.hanana.app.Utils.SquareImageView;
import com.hanana.app.interests.view.IInterestsView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class InterestsAdapter extends RecyclerView.Adapter<InterestsAdapter.InterestHolder> {
    private final static String TAG = "InterestsAdapter";

    private boolean isProfile = false;
    private boolean itsMe;
    private List<Interest> interests;
    private Context context;
    private IInterestsView parent;

    public InterestsAdapter(Context context, IInterestsView parent, boolean canEdit) {
        super();
        this.context = context;
        this.parent = parent;
        this.interests = new ArrayList<>();
        this.itsMe = canEdit;
    }

    @Override
    public InterestHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.interest_item, null);
        return new InterestHolder(layoutView);
    }


    public void activeProfile(){
        this.isProfile = true;
    }

    @Override
    public void onBindViewHolder(InterestHolder holder, int position) {
        Interest interest = interests.get(position);
        ImageLoader.loadSquareImageCenterCropWidthHeight(context,interest.getUrl(),holder.interestPhoto,400,400);
        if (itsMe) {
            if(interest.isSelected()){
                Picasso.with(context).load(R.drawable.checked).into(holder.interestSelector);
            }else{
                Picasso.with(context).load(R.drawable.unchecked).into(holder.interestSelector);
            }
        }
        holder.interestName.setText(interest.getName());
    }

    @Override
    public int getItemCount() {
        return this.interests.size();
    }

    public void setData(List<Interest> interests){
        if (!itsMe) {
            for (Iterator<Interest> it = interests.listIterator(); it.hasNext(); ) {
                if (!it.next().isSelected()) {
                    it.remove();
                }
            }
        }
        this.interests = interests;
        notifyDataSetChanged();
    }

    public List<Interest> getData(){
        return this.interests;
    }

    class InterestHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public SquareImageView interestPhoto;
        public ImageView interestSelector;
        public TextView interestName;

        public InterestHolder(View itemView) {
            super(itemView);
            if (itsMe) {
                itemView.setOnClickListener(this);
            }
            interestSelector = (ImageView) itemView.findViewById(R.id.interestSelector);
            interestPhoto = (SquareImageView) itemView.findViewById(R.id.interestPhoto);
            interestName = (TextView)itemView.findViewById(R.id.interestName);
        }

        @Override
        public void onClick(View view) {
            Interest item = interests.get(getAdapterPosition());
            item.setSelected(!item.isSelected());
            if (isProfile) {
                parent.updateUserInterests();
            }
            notifyItemChanged(getAdapterPosition());
        }
    }

}

package com.hanana.app.Adapters.ViewHolders;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hanana.app.Adapters.IGenericAdapter;
import com.hanana.app.DataModels.Badge;
import com.hanana.app.R;
import com.hanana.app.Utils.ImageLoader;
import com.hanana.app.Utils.StringFormatter;

public class BadgeHolder extends GenericHolder {
    private ImageView badgeImage;
    private TextView badgeName, badgeDescription, badgePoints, badgeStatus;


    public BadgeHolder(View itemView) {
        super(itemView);
        badgeImage = (ImageView) itemView.findViewById(R.id.badgeImage);
        badgeName = (TextView) itemView.findViewById(R.id.badgeName);
        badgeDescription = (TextView) itemView.findViewById(R.id.badgeDescription);
        badgePoints = (TextView) itemView.findViewById(R.id.badgePoints);
        badgeStatus = (TextView) itemView.findViewById(R.id.badgeStatus);
    }

    @Override
    public void initialize(Object o, Context context, IGenericAdapter adapter) {
        super.initialize(o, context, adapter);
        Badge badge = (Badge) o;
        Log.d("BadgeHolder", "currentValue: " + badge.getCurrentValue() + " triggerValue: " + badge.getTriggerValue());
        ImageLoader.loadSquareImageCenterCropWidthHeight(context,badge.getImage(), badgeImage, 100, 100);
        badgeName.setText(badge.getName());
        badgeDescription.setText(badge.getDescription());
        badgePoints.setText(StringFormatter.getPointsWordFormatted(context, badge));
        if (badge.getCurrentValue() == 0) {
            badgeStatus.setText(context.getString(R.string.completed));
        }
        else {
            badgeStatus.setText(StringFormatter.getBadgeStatusWordFormatted(context, badge));
        }
    }
}
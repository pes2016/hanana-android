package com.hanana.app.Adapters;

import com.google.android.gms.maps.model.LatLng;
import com.hanana.app.DataModels.EventListItem;

import java.util.List;

public interface IMapFragment {
    LatLng getLatLng();
    double getDistance();
    void displayEvents(List<EventListItem> events);
}

package com.hanana.app.Adapters.Behaviors;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.hanana.app.DataModels.Rate;
import com.hanana.app.DataModels.User;
import com.hanana.app.R;
import com.hanana.app.event.view.IEventView;
import com.hanana.app.rates.view.IRatesView;

public class RateBehavior extends OpenUserBehavior implements OnServerResponse {
    private static final String TAG ="RateBehavior";

    @Override
    public void execute(View view, Object o, int position, Context context) {
        Rate item = (Rate) o;
        if (item.getHidden()) {
            if (view.getId() == R.id.show) {
                ((IRatesView) context).showComment(position);
            }
        }
        else if (view.getId() == R.id.upVote) {
            if (!item.isMine()) {
                ((IRatesView) context).upVote(item.get_Id(), position);
            }
        }
        else if (view.getId() == R.id.downVote) {
            if (!item.isMine()) {
                ((IRatesView) context).downVote(item.get_Id(), position);
            }
        }
        else {
            User author = new User(item.getUserId());
            super.execute(view, author, position, context);
        }
    }

    @Override
    public void onServerResponse(Object o) {
        Rate item = (Rate) o;
        if (item.getHidden()) {
            item.setHidden(false);
        }
        else {
            item.setJustVoted(true);
        }
    }
}

package com.hanana.app.Adapters.Behaviors;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.hanana.app.DataModels.User;
import com.hanana.app.R;
import com.hanana.app.blocked.view.IBlockedView;
import com.hanana.app.pendingRequests.view.IPendingRequestsView;

public class BlockedBehavior implements IItemClickBehavior {
    private static final String TAG = "BlockedBehavior";

    @Override
    public void execute(View view, Object o, int position, Context context) {
        User item = (User) o;
        if(view.getId() == R.id.unblock){
            Log.d(TAG, "onClick: accept");
            ((IBlockedView) context).unBlock(item.get_Id(), position);
        }
    }
}

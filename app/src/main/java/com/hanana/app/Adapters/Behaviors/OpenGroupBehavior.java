package com.hanana.app.Adapters.Behaviors;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.hanana.app.DataModels.Group;
import com.hanana.app.group.view.GroupView;

import static com.hanana.app.Utils.Constants.ID;

public class OpenGroupBehavior implements IItemClickBehavior {
    private static final String TAG ="OpenGroupBehavior";

    @Override
    public void execute(View view, Object o, int position, Context context) {
        Group item = (Group) o;
        Log.v(TAG, item.get_Id());
        Intent intent = new Intent(context, GroupView.class);
        intent.putExtra(ID, item.get_Id());
        context.startActivity(intent);
    }
}

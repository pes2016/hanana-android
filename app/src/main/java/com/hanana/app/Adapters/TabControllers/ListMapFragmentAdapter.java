package com.hanana.app.Adapters.TabControllers;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.hanana.app.Views.EventsListView;
import com.hanana.app.Views.EventsMapView;

public class ListMapFragmentAdapter extends FragmentPagerAdapter {
    private int mNumOfTabs;

    public ListMapFragmentAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                Log.v("ListMapFragmentAdapter","0");
                return EventsListView.newInstance();
            case 1:
                Log.v("ListMapFragmentAdapter","1");
                return EventsMapView.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

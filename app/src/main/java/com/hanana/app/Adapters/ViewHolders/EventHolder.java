package com.hanana.app.Adapters.ViewHolders;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hanana.app.Adapters.IGenericAdapter;
import com.hanana.app.DataModels.EventListItem;
import com.hanana.app.R;
import com.hanana.app.Utils.DateFormatter;
import com.hanana.app.Utils.ImageLoader;
import com.hanana.app.Utils.StringFormatter;

import java.text.ParseException;

public class EventHolder extends GenericHolder {
    private ImageView eventImage;
    private TextView eventName, eventDate, eventAssistants;


    public EventHolder(View itemView) {
        super(itemView);
        eventImage = (ImageView) itemView.findViewById(R.id.eventImage);
        eventName = (TextView) itemView.findViewById(R.id.eventName);
        eventDate = (TextView) itemView.findViewById(R.id.eventDate);
        eventAssistants = (TextView) itemView.findViewById(R.id.eventAssistants);
    }

    @Override
    public void initialize(Object o, Context context, IGenericAdapter adapter) {
        super.initialize(o, context, adapter);
        EventListItem event = (EventListItem) o;
        ImageLoader.loadSquareImageCenterCropWidthHeight(context,event.getImage(), eventImage, 100, 100);
        eventName.setText(event.getTitle());
        try {
            eventDate.setText(DateFormatter.getDDMMYYYY(event.getDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        eventAssistants.setText(StringFormatter.getAssistantsWordFormatted(context, event));
    }
}
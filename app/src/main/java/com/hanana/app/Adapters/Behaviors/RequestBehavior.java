package com.hanana.app.Adapters.Behaviors;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.hanana.app.DataModels.User;
import com.hanana.app.R;
import com.hanana.app.pendingRequests.view.IPendingRequestsView;
import com.hanana.app.profile.view.ProfileView;

import static com.hanana.app.Utils.Constants.ID;

public class RequestBehavior extends OpenUserBehavior {
    private static final String TAG = "RequestBehavior";

    @Override
    public void execute(View view, Object o, int position, Context context) {
        User item = (User) o;

        if(view.getId() == R.id.button_accept){
            Log.d(TAG, "onClick: accept");
            ((IPendingRequestsView) context).acceptRequest(item.get_Id(), position);
        }
        else if(view.getId() == R.id.button_decline){
            Log.d(TAG, "onClick: decline");
            ((IPendingRequestsView) context).declineRequest(item.get_Id(), position);
        }
        else {
            super.execute(view, o, position, context);
        }
    }
}

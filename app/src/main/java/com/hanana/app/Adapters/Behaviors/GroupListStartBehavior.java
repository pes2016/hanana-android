package com.hanana.app.Adapters.Behaviors;

import com.hanana.app.Adapters.IGenericFragment;
import com.hanana.app.Adapters.OnFragmentRequestDataListener;

public class GroupListStartBehavior implements IStartBehavior {
    private OnFragmentRequestDataListener mListener;

    @Override
    public void execute(int page, IGenericFragment fragment) {
        mListener.getGroups(page, fragment);
    }

    @Override
    public void setReceiver(OnFragmentRequestDataListener mListener) {
        this.mListener = mListener;
    }
}

package com.hanana.app.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hanana.app.Adapters.Behaviors.IStartBehavior;
import com.hanana.app.R;

import java.util.List;

public class GenericFragment extends Fragment implements IGenericFragment {
    private static final String TAG = "GenericFragment";
    private static final int NUM_COLUMNS = 1;
    private static final int VISIBLE_THRESHOLD = 5;

    private RecyclerView mRecyclerView;
    private IGenericAdapter mAdapter;
    private OnFragmentRequestDataListener mListener;
    private TextView mEmptyView;
    private IStartBehavior mStartBehavior;
    private SwipeRefreshLayout swipeContainer;


    //Paging variables
    private boolean loadingData = true;
    private int previousTotal = 0;
    private int currentPage = 0;
    final private IGenericFragment me = this;

    public GenericFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.pull_refresh_fragment_list_view, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.itemsList);
        mEmptyView = (TextView) rootView.findViewById(R.id.empty_view);
        final LinearLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), NUM_COLUMNS);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter((RecyclerView.Adapter) mAdapter);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(dy > 0) //check for scroll down
                {
                    int visibleItemCount = mLayoutManager.getChildCount();
                    int totalItemCount = mLayoutManager.getItemCount();
                    int firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

                    if (loadingData) {
                        if (totalItemCount > previousTotal) {
                            loadingData = false;
                            previousTotal = totalItemCount;
                        }
                    }
                    if (!loadingData && (totalItemCount - visibleItemCount)
                            <= (firstVisibleItem + VISIBLE_THRESHOLD)) {
                        // End has been reached
                        loadingData = true;
                        Log.d(TAG, "onScrolled: currentPage: " + currentPage);
                        ++currentPage;
                        // Do something
                        mStartBehavior.execute(currentPage,me);
                    }
                }

            }
        });


        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Do something
                currentPage = 0;
                mStartBehavior.execute(currentPage, me);
            }
        });

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        mStartBehavior.execute(currentPage, this);
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        Log.d(TAG, "onAttach: ");
        super.onAttach(context);
        if (context instanceof OnFragmentRequestDataListener) {
            mListener = (OnFragmentRequestDataListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        mStartBehavior.setReceiver(mListener);
    }

    @Override
    public void setData(List data) {
        Log.d(TAG, "setData: ");
        swipeContainer.setRefreshing(false);
        currentPage = 0;
        previousTotal = 0;
        loadingData = false;

        if (data.isEmpty()) {
            mRecyclerView.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
        }
        else {
            mRecyclerView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);
        }

        mAdapter.setData(data);
        mRecyclerView.invalidate();
    }

    @Override
    public void appendData(List data) {
        Log.d(TAG, "appendData: ");
        loadingData = false;
        mAdapter.appendData(data);
        mRecyclerView.invalidate();
    }

    @Override
    public void setStartBehavior(IStartBehavior startBehavior) {
        mStartBehavior = startBehavior;
    }

    @Override
    public void initialize(IGenericAdapter adapter) {
        mAdapter = adapter;
    }

    @Override
    public void removeItemAt(int position) {
        mAdapter.removeItemAt(position);
    }

    @Override
    public void onServerResponse(int position) {
        mAdapter.onServerResponse(position);
    }
}

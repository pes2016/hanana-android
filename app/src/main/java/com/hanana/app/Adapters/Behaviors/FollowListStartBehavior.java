package com.hanana.app.Adapters.Behaviors;

import com.hanana.app.Adapters.IGenericFragment;
import com.hanana.app.Adapters.OnFragmentRequestDataListener;

public class FollowListStartBehavior implements IStartBehavior {
    private OnFragmentRequestDataListener mListener;
    private int type;

    public FollowListStartBehavior(int type) {
        this.type = type;
    }

    @Override
    public void execute(int page, IGenericFragment fragment) {
        mListener.getData(page);
    }

    @Override
    public void setReceiver(OnFragmentRequestDataListener mListener) {
        this.mListener = mListener;
    }
}

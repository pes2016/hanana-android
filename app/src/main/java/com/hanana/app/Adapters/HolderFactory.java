package com.hanana.app.Adapters;

import android.content.Context;
import android.view.LayoutInflater;

import com.hanana.app.Adapters.ViewHolders.BadgeHolder;
import com.hanana.app.Adapters.ViewHolders.BlockedHolder;
import com.hanana.app.Adapters.ViewHolders.RateHolder;
import com.hanana.app.Adapters.ViewHolders.EventHolder;
import com.hanana.app.Adapters.ViewHolders.FollowsHolder;
import com.hanana.app.Adapters.ViewHolders.GroupHolder;
import com.hanana.app.Adapters.ViewHolders.IGenericHolder;
import com.hanana.app.Adapters.ViewHolders.RankingHolder;
import com.hanana.app.Adapters.ViewHolders.RequestsHolder;
import com.hanana.app.Adapters.ViewHolders.UserHolder;

import static com.hanana.app.Utils.Constants.BADGES_HOLDER_TYPE;
import static com.hanana.app.Utils.Constants.BLOCKED_HOLDER_TYPE;
import static com.hanana.app.Utils.Constants.RATE_HOLDER_TYPE;
import static com.hanana.app.Utils.Constants.EVENT_HOLDER_TYPE;
import static com.hanana.app.Utils.Constants.FOLLOWS_HOLDER_TYPE;
import static com.hanana.app.Utils.Constants.GROUP_HOLDER_TYPE;
import static com.hanana.app.Utils.Constants.RANKING_HOLDER_TYPE;
import static com.hanana.app.Utils.Constants.REQUESTS_HOLDER_TYPE;
import static com.hanana.app.Utils.Constants.USERS_HOLDER_TYPE;

public class HolderFactory {
    public static IGenericHolder getHolder(int layout, int type, Context context) {
        switch (type){
            case EVENT_HOLDER_TYPE:
                return new EventHolder(LayoutInflater.from(context).inflate(layout, null));
            case GROUP_HOLDER_TYPE:
                return new GroupHolder(LayoutInflater.from(context).inflate(layout, null));
            case USERS_HOLDER_TYPE:
                return new UserHolder(LayoutInflater.from(context).inflate(layout, null));
            case FOLLOWS_HOLDER_TYPE:
                return new FollowsHolder(LayoutInflater.from(context).inflate(layout, null));
            case REQUESTS_HOLDER_TYPE:
                return new RequestsHolder(LayoutInflater.from(context).inflate(layout, null));
            case BLOCKED_HOLDER_TYPE:
                return new BlockedHolder(LayoutInflater.from(context).inflate(layout, null));
            case BADGES_HOLDER_TYPE:
                return new BadgeHolder(LayoutInflater.from(context).inflate(layout, null));
            case RANKING_HOLDER_TYPE:
                return new RankingHolder(LayoutInflater.from(context).inflate(layout, null));
            case RATE_HOLDER_TYPE:
                return new RateHolder(LayoutInflater.from(context).inflate(layout, null));
        }
        return null;
    }
}

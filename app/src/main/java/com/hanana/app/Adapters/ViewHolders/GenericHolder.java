package com.hanana.app.Adapters.ViewHolders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.hanana.app.Adapters.IGenericAdapter;

public class GenericHolder extends RecyclerView.ViewHolder implements IGenericHolder {
    private final static String TAG = "GenericHolder";
    private IGenericAdapter mAdapter;

    public GenericHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
    }

    @Override
    public void initialize(Object o, Context context, IGenericAdapter adapter) {
        mAdapter = adapter;
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "onClick: position: " + getAdapterPosition());
        mAdapter.onItemClick(v, getAdapterPosition());
    }
}
package com.hanana.app.Adapters.TabControllers;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;

import com.hanana.app.Adapters.Behaviors.EventListStartBehavior;
import com.hanana.app.Adapters.GenericAdapter;
import com.hanana.app.Adapters.GenericFragment;
import com.hanana.app.Adapters.IGenericAdapter;
import com.hanana.app.Adapters.Behaviors.IItemClickBehavior;
import com.hanana.app.Adapters.Behaviors.IStartBehavior;
import com.hanana.app.Adapters.Behaviors.OpenEventBehavior;
import com.hanana.app.R;
import com.hanana.app.Utils.Constants;

public class ListFragmentAdapter extends FragmentPagerAdapter {
    private int mNumOfTabs;
    private Context mContext;
    private int mLayoutId;
    private IItemClickBehavior openEventBehavior;

    public ListFragmentAdapter(Context context, int NumOfTabs) {
        super(((AppCompatActivity) context).getSupportFragmentManager());
        mContext = context;
        mNumOfTabs = NumOfTabs;
        mLayoutId = R.layout.event_item;
        openEventBehavior = new OpenEventBehavior();
    }


    @Override
    public Fragment getItem(int position) {

        IGenericAdapter adapter = new GenericAdapter(
                mContext,
                mLayoutId,
                Constants.EVENT_HOLDER_TYPE,
                openEventBehavior
        );

        IStartBehavior eventListStartBehavior = new EventListStartBehavior(position);

        GenericFragment f = new GenericFragment();
        f.initialize(adapter);
        f.setStartBehavior(eventListStartBehavior);
        return f;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

package com.hanana.app.Adapters.ViewHolders;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hanana.app.Adapters.IGenericAdapter;
import com.hanana.app.DataModels.User;
import com.hanana.app.R;
import com.hanana.app.Utils.ImageLoader;
import com.hanana.app.Utils.StringFormatter;

public class RankingHolder extends GenericHolder {
    private ImageView userImage;
    private TextView userPosition, userName, userBadges, userPoints;

    public RankingHolder(View itemView) {
        super(itemView);
        userImage = (ImageView) itemView.findViewById(R.id.userImage);
        userPosition = (TextView) itemView.findViewById(R.id.userPosition);
        userName = (TextView) itemView.findViewById(R.id.userName);
        userBadges = (TextView) itemView.findViewById(R.id.userBadges);
        userPoints = (TextView) itemView.findViewById(R.id.userPoints);
    }

    @Override
    public void initialize(Object o, Context context, IGenericAdapter adapter) {
        super.initialize(o, context, adapter);
        User user = (User) o;
        userPosition.setText(String.valueOf(getAdapterPosition() + 1));
        ImageLoader.loadSquareImageCenterCropWidthHeight(context,user.getAvatar(),userImage,100,100);
        userName.setText(user.getName());
        userBadges.setText(StringFormatter.getNumberOfBadgesWordFormatted(context, user));
        userPoints.setText(StringFormatter.getPointsWordFormatted(context, user));
    }
}
package com.hanana.app.Adapters.ViewHolders;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hanana.app.Adapters.IGenericAdapter;
import com.hanana.app.DataModels.User;
import com.hanana.app.R;
import com.hanana.app.Utils.ActiveUser;
import com.hanana.app.Utils.ImageLoader;

public class FollowsHolder extends GenericHolder {
    private ImageView userImage, followButton;
    private TextView userName;

    public FollowsHolder(View itemView) {
        super(itemView);
        userImage = (ImageView) itemView.findViewById(R.id.userImage);
        userName = (TextView) itemView.findViewById(R.id.userName);
        followButton = (ImageView) itemView.findViewById(R.id.followButton);

        followButton.setOnClickListener(this);
    }

    @Override
    public void initialize(Object o, Context context, IGenericAdapter adapter) {
        super.initialize(o, context, adapter);
        User user = (User) o;
        ImageLoader.loadSquareImageCenterCropWidthHeight(context,user.getAvatar(),userImage,100,100);
        userName.setText(user.getName());
        if (user.get_Id().equals(ActiveUser.getInstance().get_Id())) {
            followButton.setOnClickListener(null);
        }
        else {
            if (user.getIsFollowing()) {
                followButton.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_cancel, null));
            } else {
                followButton.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_follow, null));
            }
        }
    }
}

package com.hanana.app.Adapters.Behaviors;

import com.hanana.app.Adapters.IGenericFragment;
import com.hanana.app.Adapters.OnFragmentRequestDataListener;

public class RateListStartBehavior implements IStartBehavior {
    private OnFragmentRequestDataListener mListener;

    @Override
    public void execute(int page, IGenericFragment fragment) {
        mListener.getData(page);
    }

    @Override
    public void setReceiver(OnFragmentRequestDataListener mListener) {
        this.mListener = mListener;
    }
}

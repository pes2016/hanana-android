package com.hanana.app.Adapters.ViewHolders;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hanana.app.Adapters.IGenericAdapter;
import com.hanana.app.DataModels.Rate;
import com.hanana.app.R;
import com.hanana.app.Utils.ActiveUser;
import com.hanana.app.Utils.ImageLoader;

public class RateHolder extends GenericHolder {
    private ImageView userImage, downVote, upVote;
    private TextView userName, rating, description, voted, numUpvotes, numDownvotes, hidden, show;
    private RelativeLayout row;

    public RateHolder(View itemView) {
        super(itemView);
        userImage = (ImageView) itemView.findViewById(R.id.userImage);
        userName = (TextView) itemView.findViewById(R.id.userName);
        downVote = (ImageView) itemView.findViewById(R.id.downVote);
        upVote = (ImageView) itemView.findViewById(R.id.upVote);
        rating = (TextView) itemView.findViewById(R.id.rating);
        description = (TextView) itemView.findViewById(R.id.description);
        voted = (TextView) itemView.findViewById(R.id.voted);
        numUpvotes = (TextView) itemView.findViewById(R.id.numberUpVotes);
        numDownvotes = (TextView) itemView.findViewById(R.id.numberDownVotes);
        hidden = (TextView) itemView.findViewById(R.id.hidden);
        show = (TextView) itemView.findViewById(R.id.show);
        row = (RelativeLayout) itemView.findViewById(R.id.row);
        downVote.setOnClickListener(this);
        upVote.setOnClickListener(this);
    }

    @Override
    public void initialize(Object o, Context context, IGenericAdapter adapter) {
        super.initialize(o, context, adapter);
        Rate rate = (Rate) o;
        if (rate.getHidden()) {
            row.setVisibility(View.GONE);
            hidden.setVisibility(View.VISIBLE);
            show.setVisibility(View.VISIBLE);
            show.setOnClickListener(this);
        }
        else {
            row.setVisibility(View.VISIBLE);
            hidden.setVisibility(View.GONE);
            show.setVisibility(View.GONE);
            ImageLoader.loadSquareImageCenterCropWidthHeight(context, rate.getAvatar(), userImage, 100, 100);
            userName.setText(rate.getName());
            rating.setText(Integer.toString(rate.getValue()));
            description.setText(rate.getComment());
            numUpvotes.setText(Integer.toString(rate.getNumberOfUpvotes()));
            numDownvotes.setText(Integer.toString(rate.getNumberOfDownvotes()));
            if (rate.isMine()) {
                upVote.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_thumb_up_grey, null));
                downVote.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_thumb_down_grey, null));
            } else {
                if (rate.isJustVoted()) {
                    upVote.setVisibility(View.GONE);
                    downVote.setVisibility(View.GONE);
                    voted.setVisibility(View.VISIBLE);
                } else {
                    if (rate.getIsUpvoted()) {
                        upVote.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_thumb_up_green, null));
                        numUpvotes.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.green, null));
                    } else if (rate.getIsDownvoted()) {
                        downVote.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_thumb_down_red, null));
                        numDownvotes.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.red, null));
                    }
                }
            }
        }
    }
}

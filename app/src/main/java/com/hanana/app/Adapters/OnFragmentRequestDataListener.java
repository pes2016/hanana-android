package com.hanana.app.Adapters;

public interface OnFragmentRequestDataListener {
    void getSuggestedEvents(int page, IGenericFragment fragment);
    void getPopularEvents(int page, IGenericFragment fragment);
    void getFriendsEvents(int page, IGenericFragment fragment);
    void getGroups(int page, IGenericFragment fragment);
    void getEvents(int page, IGenericFragment fragment);
    void getFutureEvents(int page, IGenericFragment fragment);
    void getPastEvents(int page, IGenericFragment fragment);
    void getUsers(int page, IGenericFragment fragment);
    void getMapEvents(double lat, double lon, double distance, IMapFragment fragment);
    void getData(int page);
}

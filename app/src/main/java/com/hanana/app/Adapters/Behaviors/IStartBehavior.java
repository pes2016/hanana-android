package com.hanana.app.Adapters.Behaviors;

import com.hanana.app.Adapters.IGenericFragment;
import com.hanana.app.Adapters.OnFragmentRequestDataListener;

public interface IStartBehavior {
    void execute(int page, IGenericFragment fragment);
    void setReceiver(OnFragmentRequestDataListener mListener);
}

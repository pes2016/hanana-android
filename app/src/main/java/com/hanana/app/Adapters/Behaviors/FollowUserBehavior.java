package com.hanana.app.Adapters.Behaviors;

import android.content.Context;
import android.view.View;

import com.hanana.app.DataModels.User;
import com.hanana.app.R;
import com.hanana.app.follows.view.IFollowsView;

public class FollowUserBehavior extends OpenUserBehavior implements OnServerResponse {
    private static final String TAG ="FollowUserBehavior";

    @Override
    public void execute(View view, Object o, int position, Context context) {
        User item = (User) o;
        if (view.getId() == R.id.followButton) {
            ((IFollowsView) context).toggleFollowing(item.get_Id(), position);
        }
        else {
            super.execute(view, o, position, context);
        }
    }

    @Override
    public void onServerResponse(Object o) {
        User item = (User) o;
        item.setIsFollowing(!item.getIsFollowing());
    }
}

package com.hanana.app.Adapters.ViewHolders;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hanana.app.Adapters.IGenericAdapter;
import com.hanana.app.DataModels.Group;
import com.hanana.app.R;
import com.hanana.app.Utils.ImageLoader;
import com.hanana.app.Utils.StringFormatter;

public class GroupHolder extends GenericHolder {
    private ImageView groupImage;
    private TextView groupName, groupFollowers, groupEvents;


    public GroupHolder(View itemView) {
        super(itemView);
        groupImage = (ImageView) itemView.findViewById(R.id.groupImage);
        groupName = (TextView) itemView.findViewById(R.id.groupName);
        groupFollowers = (TextView) itemView.findViewById(R.id.groupFollowers);
        groupEvents = (TextView) itemView.findViewById(R.id.groupEvents);
    }

    @Override
    public void initialize(Object o, Context context, IGenericAdapter adapter) {
        super.initialize(o, context, adapter);
        Group group = (Group) o;
        ImageLoader.loadSquareImageCenterCropWidthHeight(context,group.getImage(),groupImage,100,100);
        groupName.setText(group.getName());
        groupFollowers.setText(StringFormatter.getNumberOfFollowersWordFormatted(context, group));
        groupEvents.setText(StringFormatter.getNumberOfEventsWordFormatted(context, group));
    }
}
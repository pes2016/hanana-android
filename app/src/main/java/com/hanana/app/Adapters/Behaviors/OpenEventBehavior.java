package com.hanana.app.Adapters.Behaviors;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.hanana.app.DataModels.EventListItem;
import com.hanana.app.event.view.EventView;

import static com.hanana.app.Utils.Constants.ID;

public class OpenEventBehavior implements IItemClickBehavior {
    private static final String TAG ="OpenEventBehavior";

    @Override
    public void execute(View view, Object o, int position, Context context) {
        EventListItem item = (EventListItem) o;
        Log.v(TAG, item.get_Id());
        Intent intent = new Intent(context, EventView.class);
        intent.putExtra(ID, item.get_Id());
        context.startActivity(intent);
    }
}

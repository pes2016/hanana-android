package com.hanana.app.Adapters.Behaviors;

import com.hanana.app.Adapters.IGenericFragment;
import com.hanana.app.Adapters.OnFragmentRequestDataListener;

import static com.hanana.app.Utils.Constants.FRIENDS;
import static com.hanana.app.Utils.Constants.NEXT;
import static com.hanana.app.Utils.Constants.PAST;
import static com.hanana.app.Utils.Constants.POPULAR;
import static com.hanana.app.Utils.Constants.PROFILE;
import static com.hanana.app.Utils.Constants.SUGGESTED;

public class EventListStartBehavior implements IStartBehavior {
    private OnFragmentRequestDataListener mListener;
    private int type;

    public EventListStartBehavior(int type) {
        this.type = type;
    }

    @Override
    public void execute(int page, IGenericFragment fragment) {

        switch (type){
            case SUGGESTED:
                mListener.getSuggestedEvents(page, fragment);
                break;
            case POPULAR:
                mListener.getPopularEvents(page, fragment);
                break;
            case FRIENDS:
                mListener.getFriendsEvents(page, fragment);
                break;
            case PROFILE:
                mListener.getEvents(page, fragment);
                break;
            case NEXT:
                mListener.getFutureEvents(page, fragment);
                break;
            case PAST:
                mListener.getPastEvents(page, fragment);
                break;
        }
    }

    @Override
    public void setReceiver(OnFragmentRequestDataListener mListener) {
        this.mListener = mListener;
    }
}

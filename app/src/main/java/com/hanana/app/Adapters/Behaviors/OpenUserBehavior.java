package com.hanana.app.Adapters.Behaviors;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.hanana.app.DataModels.User;
import com.hanana.app.profile.view.ProfileView;

import static com.hanana.app.Utils.Constants.ID;

public class OpenUserBehavior implements IItemClickBehavior {
    private static final String TAG ="OpenUserBehavior";

    @Override
    public void execute(View view, Object o, int position, Context context) {
        User item = (User) o;
        Log.v(TAG, item.get_Id());
        Intent intent = new Intent(context, ProfileView.class);
        intent.putExtra(ID, item.get_Id());
        context.startActivity(intent);
    }
}

package com.hanana.app.Adapters;

import android.view.View;

import java.util.List;

public interface IGenericAdapter {
    void removeItemAt(int position);
    void setData(List data);
    void appendData(List data);
    void onItemClick(View view, int position);
    void onServerResponse(int position);
}

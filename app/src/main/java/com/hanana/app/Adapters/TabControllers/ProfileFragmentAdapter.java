package com.hanana.app.Adapters.TabControllers;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.hanana.app.Adapters.Behaviors.EventListStartBehavior;
import com.hanana.app.Adapters.Behaviors.GroupListStartBehavior;
import com.hanana.app.Adapters.Behaviors.IItemClickBehavior;
import com.hanana.app.Adapters.Behaviors.IStartBehavior;
import com.hanana.app.Adapters.Behaviors.OpenEventBehavior;
import com.hanana.app.Adapters.Behaviors.OpenGroupBehavior;
import com.hanana.app.Adapters.GenericAdapter;
import com.hanana.app.Adapters.GenericFragment;
import com.hanana.app.Adapters.IGenericAdapter;
import com.hanana.app.R;
import com.hanana.app.Utils.Constants;
import com.hanana.app.interests.view.InterestsView;

import static com.hanana.app.Utils.Constants.IS_MY_PROFILE;
import static com.hanana.app.Utils.Constants.IS_OTHERS_PROFILE;
import static com.hanana.app.Utils.Constants.PROFILE;

public class ProfileFragmentAdapter extends FragmentPagerAdapter {
    private int mNumOfTabs;
    private Context mContext;
    private IItemClickBehavior openGroupBehavior, openEventBehavior;
    private boolean mOwnProfile;
    private String mUserId;

    public ProfileFragmentAdapter(Context context, int NumOfTabs, boolean isMyProfile, String userId) {
        super(((AppCompatActivity) context).getSupportFragmentManager());
        mContext = context;
        mNumOfTabs = NumOfTabs;
        mOwnProfile = isMyProfile;
        mUserId = userId;
        openGroupBehavior = new OpenGroupBehavior();
        openEventBehavior = new OpenEventBehavior();
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                Log.v("ProfileFragmentAdapter","0");

                IGenericAdapter adapter = new GenericAdapter(
                        mContext,
                        R.layout.group_item,
                        Constants.GROUP_HOLDER_TYPE,
                        openGroupBehavior
                );

                IStartBehavior groupListStartBehavior = new GroupListStartBehavior();

                GenericFragment f = new GenericFragment();

                f.initialize(adapter);
                f.setStartBehavior(groupListStartBehavior);
                return f;
                //return GroupsListRecyclerView.newInstance("","");
            case 1:
                Log.v("ProfileFragmentAdapter","1");

                IGenericAdapter adapter2 = new GenericAdapter(
                        mContext,
                        R.layout.event_item,
                        Constants.EVENT_HOLDER_TYPE,
                        openEventBehavior
                );

                IStartBehavior eventListStartBehavior = new EventListStartBehavior(PROFILE);

                GenericFragment f2 = new GenericFragment();
                f2.initialize(adapter2);
                f2.setStartBehavior(eventListStartBehavior);
                return f2;
            case 2:
                Log.v("ProfileFragmentAdapter","2");
                InterestsView tab3;
                if (mOwnProfile) {
                    tab3 = InterestsView.newInstance(IS_MY_PROFILE, mUserId);
                }
                else {
                    tab3 = InterestsView.newInstance(IS_OTHERS_PROFILE, mUserId);
                }
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

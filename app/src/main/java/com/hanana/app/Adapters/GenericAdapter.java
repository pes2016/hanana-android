package com.hanana.app.Adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.View;
import android.view.ViewGroup;

import com.hanana.app.Adapters.Behaviors.IItemClickBehavior;
import com.hanana.app.Adapters.Behaviors.OnServerResponse;
import com.hanana.app.Adapters.ViewHolders.IGenericHolder;

import java.util.ArrayList;
import java.util.List;

public class GenericAdapter extends Adapter implements IGenericAdapter {
    private int mLayoutId;
    private Context mContext;
    private List mData;
    private final int mHolderType;
    private IItemClickBehavior mBehavior;


    public GenericAdapter(Context context, int layoutId, int type, IItemClickBehavior behaviour) {
        super();
        mContext = context;
        mLayoutId = layoutId;
        mData = new ArrayList();
        mBehavior = behaviour;
        mHolderType = type;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return (RecyclerView.ViewHolder) HolderFactory.getHolder(mLayoutId, mHolderType, parent.getContext());
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((IGenericHolder) holder).initialize(mData.get(position), mContext, this);
    }

    @Override
    public void removeItemAt(int position) {
        if (mData.size() > position) {
            mData.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, getItemCount());
        }
    }


    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List data) {
        if (data != null) {
            mData = data;
            notifyDataSetChanged();
        }
        else {
            mData = new ArrayList();
        }
    }

    @Override
    public void appendData(List data) {
        int size = mData.size();
        mData.addAll(data);
        notifyItemRangeInserted(size, data.size());
    }

    public void onItemClick(View view, int position){
        mBehavior.execute(view, mData.get(position), position, mContext);
    };

    @Override
    public void onServerResponse(int position) {
        if (mBehavior instanceof OnServerResponse) {
            ((OnServerResponse) mBehavior).onServerResponse(mData.get(position));
            notifyItemChanged(position);
        }
    }
}

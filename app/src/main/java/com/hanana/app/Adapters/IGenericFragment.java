package com.hanana.app.Adapters;

import com.hanana.app.Adapters.Behaviors.IStartBehavior;

import java.util.List;

public interface IGenericFragment {
    void setData(List data);
    void appendData(List data);
    void setStartBehavior(IStartBehavior startBehavior);
    void initialize(IGenericAdapter adapter);
    void removeItemAt(int position);
    void onServerResponse(int position);
}

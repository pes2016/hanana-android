package com.hanana.app.Adapters.TabControllers;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.hanana.app.Adapters.Behaviors.GroupListStartBehavior;
import com.hanana.app.Adapters.Behaviors.IItemClickBehavior;
import com.hanana.app.Adapters.Behaviors.IStartBehavior;
import com.hanana.app.Adapters.Behaviors.OpenGroupBehavior;
import com.hanana.app.Adapters.Behaviors.OpenUserBehavior;
import com.hanana.app.Adapters.Behaviors.UserListStartBehavior;
import com.hanana.app.Adapters.GenericAdapter;
import com.hanana.app.Adapters.GenericFragment;
import com.hanana.app.Adapters.IGenericAdapter;
import com.hanana.app.R;
import com.hanana.app.Utils.Constants;

public class UsersGroupsFragmentAdapter extends FragmentPagerAdapter {
    private int mNumOfTabs;
    private Context mContext;
    private IItemClickBehavior openGroupBehavior, openUserBehavior;

    public UsersGroupsFragmentAdapter(Context context, int NumOfTabs) {
        super(((AppCompatActivity) context).getSupportFragmentManager());
        mContext = context;
        this.mNumOfTabs = NumOfTabs;
        openGroupBehavior = new OpenGroupBehavior();
        openUserBehavior = new OpenUserBehavior();
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                Log.v("UGFragmentAdapter","0");

                IGenericAdapter adapter = new GenericAdapter(
                        mContext,
                        R.layout.user_item,
                        Constants.USERS_HOLDER_TYPE,
                        openUserBehavior
                );

                IStartBehavior userListStartBehavior = new UserListStartBehavior();

                GenericFragment f = new GenericFragment();

                f.initialize(adapter);
                f.setStartBehavior(userListStartBehavior);
                return f;
            case 1:
                IGenericAdapter adapter2 = new GenericAdapter(
                        mContext,
                        R.layout.group_item,
                        Constants.GROUP_HOLDER_TYPE,
                        openGroupBehavior
                );

                IStartBehavior groupListStartBehavior = new GroupListStartBehavior();

                GenericFragment f2 = new GenericFragment();

                f2.initialize(adapter2);
                f2.setStartBehavior(groupListStartBehavior);
                return f2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

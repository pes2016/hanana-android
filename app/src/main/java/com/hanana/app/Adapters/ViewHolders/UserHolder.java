package com.hanana.app.Adapters.ViewHolders;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hanana.app.Adapters.IGenericAdapter;
import com.hanana.app.DataModels.User;
import com.hanana.app.R;
import com.hanana.app.Utils.ImageLoader;
import com.hanana.app.Utils.StringFormatter;

public class UserHolder extends GenericHolder {
    private ImageView userImage;
    private TextView userName, userFollowers, userCommonFollowers;

    public UserHolder(View itemView) {
        super(itemView);
        userImage = (ImageView) itemView.findViewById(R.id.userImage);
        userName = (TextView) itemView.findViewById(R.id.userName);
        userFollowers = (TextView) itemView.findViewById(R.id.userFollowers);
        userCommonFollowers = (TextView) itemView.findViewById(R.id.userCommonFollowers);
    }

    @Override
    public void initialize(Object o, Context context, IGenericAdapter adapter) {
        super.initialize(o, context, adapter);
        User user = (User) o;
        ImageLoader.loadSquareImageCenterCropWidthHeight(context,user.getAvatar(),userImage,100,100);
        userName.setText(user.getName());
        userFollowers.setText(StringFormatter.getNumberOfFollowersWordFormatted(context, user));
        userCommonFollowers.setText(StringFormatter.getNumberOfFollowersInCommonWordFormatted(context, user));
    }
}
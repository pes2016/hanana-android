package com.hanana.app.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.ThemedSpinnerAdapter;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hanana.app.R;
import com.hanana.app.Utils.ChatHelper;
import com.hanana.app.Utils.ImageLoader;
import com.sendbird.android.AdminMessage;
import com.sendbird.android.BaseMessage;
import com.sendbird.android.FileMessage;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.SendBird;
import com.sendbird.android.User;
import com.sendbird.android.UserMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by marcos on 27/12/2016.
 */

public class MessageListAdapter extends BaseAdapter {
    private static final int TYPE_UNSUPPORTED = 0;
    private static final int TYPE_USER_MESSAGE = 1;
    private static final int TYPE_ADMIN_MESSAGE = 2;
    private static final int TYPE_FILE_MESSAGE = 3;
    private static final int TYPE_TYPING_INDICATOR = 4;

    private final Context mContext;
    private final LayoutInflater mInflater;
    private final ArrayList<Object> mItemList;
    private final GroupChannel mGroupChannel;

    public MessageListAdapter(Context context, GroupChannel channel) {
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mItemList = new ArrayList<>();
        mGroupChannel = channel;
    }

    @Override
    public int getCount() {
        return mItemList.size() + (mGroupChannel.isTyping() ? 1 : 0);
    }

    @Override
    public Object getItem(int position) {
        if (position >= mItemList.size()) {
            List<User> members = mGroupChannel.getTypingMembers();
            ArrayList<String> names = new ArrayList<>();
            for (User member : members) {
                names.add(member.getNickname());
            }

            return names;
        }
        return mItemList.get(position);
    }

    public void delete(Object object) {
        mItemList.remove(object);
    }

    public void clear() {
        mItemList.clear();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void insertMessage(BaseMessage message) {
        mItemList.add(0, message);
    }

    public void appendMessage(BaseMessage message) {
        mItemList.add(message);
    }

    @Override
    public int getItemViewType(int position) {
        if (position >= mItemList.size()) {
            return TYPE_TYPING_INDICATOR;
        }

        Object item = mItemList.get(position);
        if (item instanceof UserMessage) {
            return TYPE_USER_MESSAGE;
        } else if (item instanceof FileMessage) {
            return TYPE_FILE_MESSAGE;
        } else if (item instanceof AdminMessage) {
            return TYPE_ADMIN_MESSAGE;
        }

        return TYPE_UNSUPPORTED;
    }

    @Override
    public int getViewTypeCount() {
        return 5;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        final Object item = getItem(position);

        if (convertView == null || ((ViewHolder) convertView.getTag()).getViewType() != getItemViewType(position)) {
            viewHolder = new ViewHolder();
            viewHolder.setViewType(getItemViewType(position));

            switch (getItemViewType(position)) {
                case TYPE_UNSUPPORTED:
                    convertView = new View(mInflater.getContext());
                    convertView.setTag(viewHolder);
                    break;
                case TYPE_USER_MESSAGE: {
                    TextView tv;
                    ImageView iv;
                    View v;

                    convertView = mInflater.inflate(R.layout.sendbird_view_group_user_message, parent, false);

                    v = convertView.findViewById(R.id.left_container);
                    viewHolder.setView("left_container", v);
                    iv = (ImageView) convertView.findViewById(R.id.img_left_thumbnail);
                    viewHolder.setView("left_thumbnail", iv);
                    tv = (TextView) convertView.findViewById(R.id.txt_left);
                    viewHolder.setView("left_message", tv);
                    tv = (TextView) convertView.findViewById(R.id.txt_left_name);
                    viewHolder.setView("left_name", tv);
                    tv = (TextView) convertView.findViewById(R.id.txt_left_time);
                    viewHolder.setView("left_time", tv);

                    v = convertView.findViewById(R.id.right_container);
                    viewHolder.setView("right_container", v);
                    iv = (ImageView) convertView.findViewById(R.id.img_right_thumbnail);
                    viewHolder.setView("right_thumbnail", iv);
                    tv = (TextView) convertView.findViewById(R.id.txt_right);
                    viewHolder.setView("right_message", tv);
                    tv = (TextView) convertView.findViewById(R.id.txt_right_name);
                    viewHolder.setView("right_name", tv);
                    tv = (TextView) convertView.findViewById(R.id.txt_right_time);
                    viewHolder.setView("right_time", tv);

                    convertView.setTag(viewHolder);
                    break;
                }
                case TYPE_ADMIN_MESSAGE: {
                    convertView = mInflater.inflate(R.layout.sendbird_view_admin_message, parent, false);
                    viewHolder.setView("message", convertView.findViewById(R.id.txt_message));
                    convertView.setTag(viewHolder);
                    break;
                }
                case TYPE_FILE_MESSAGE: {
                    TextView tv;
                    ImageView iv;
                    View v;

                    convertView = mInflater.inflate(R.layout.sendbird_view_group_file_message, parent, false);

                    v = convertView.findViewById(R.id.left_container);
                    viewHolder.setView("left_container", v);
                    iv = (ImageView) convertView.findViewById(R.id.img_left_thumbnail);
                    viewHolder.setView("left_thumbnail", iv);
                    iv = (ImageView) convertView.findViewById(R.id.img_left);
                    viewHolder.setView("left_image", iv);
                    tv = (TextView) convertView.findViewById(R.id.txt_left_name);
                    viewHolder.setView("left_name", tv);
                    tv = (TextView) convertView.findViewById(R.id.txt_left_time);
                    viewHolder.setView("left_time", tv);

                    v = convertView.findViewById(R.id.right_container);
                    viewHolder.setView("right_container", v);
                    iv = (ImageView) convertView.findViewById(R.id.img_right_thumbnail);
                    viewHolder.setView("right_thumbnail", iv);
                    iv = (ImageView) convertView.findViewById(R.id.img_right);
                    viewHolder.setView("right_image", iv);
                    tv = (TextView) convertView.findViewById(R.id.txt_right_name);
                    viewHolder.setView("right_name", tv);
                    tv = (TextView) convertView.findViewById(R.id.txt_right_time);
                    viewHolder.setView("right_time", tv);

                    convertView.setTag(viewHolder);
                    break;
                }
                case TYPE_TYPING_INDICATOR: {
                    convertView = mInflater.inflate(R.layout.sendbird_view_group_typing_indicator, parent, false);
                    viewHolder.setView("message", convertView.findViewById(R.id.txt_message));
                    convertView.setTag(viewHolder);
                    break;
                }
            }
        }

        viewHolder = (ViewHolder) convertView.getTag();
        switch (getItemViewType(position)) {
            case TYPE_UNSUPPORTED:
                break;
            case TYPE_USER_MESSAGE:
                UserMessage message = (UserMessage) item;
                if (message.getSender().getUserId().equals(SendBird.getCurrentUser().getUserId())) {
                    viewHolder.getView("left_container", View.class).setVisibility(View.GONE);
                    viewHolder.getView("right_container", View.class).setVisibility(View.VISIBLE);

                    ChatHelper.displayUrlImage(viewHolder.getView("right_thumbnail", ImageView.class), ImageLoader.URLFormatter(message.getSender().getProfileUrl()), true);
                    viewHolder.getView("right_name", TextView.class).setText(message.getSender().getNickname());
                    viewHolder.getView("right_message", TextView.class).setText(message.getMessage());
                    viewHolder.getView("right_time", TextView.class).setText(ChatHelper.getDisplayDateTime(mContext, message.getCreatedAt()));

                } else {
                    viewHolder.getView("left_container", View.class).setVisibility(View.VISIBLE);
                    viewHolder.getView("right_container", View.class).setVisibility(View.GONE);
                    ChatHelper.displayUrlImage(viewHolder.getView("left_thumbnail", ImageView.class), ImageLoader.URLFormatter(message.getSender().getProfileUrl()), true);
                    viewHolder.getView("left_name", TextView.class).setText(message.getSender().getNickname());
                    viewHolder.getView("left_message", TextView.class).setText(message.getMessage());
                    viewHolder.getView("left_time", TextView.class).setText(ChatHelper.getDisplayDateTime(mContext, message.getCreatedAt()));
                }
                break;
            case TYPE_ADMIN_MESSAGE:
                AdminMessage adminMessage = (AdminMessage) item;
                viewHolder.getView("message", TextView.class).setText(Html.fromHtml(adminMessage.getMessage()));
                break;
            case TYPE_FILE_MESSAGE:
                final FileMessage fileLink = (FileMessage) item;

                if (fileLink.getSender().getUserId().equals(SendBird.getCurrentUser().getUserId())) {
                    viewHolder.getView("left_container", View.class).setVisibility(View.GONE);
                    viewHolder.getView("right_container", View.class).setVisibility(View.VISIBLE);

                    ChatHelper.displayUrlImage(viewHolder.getView("right_thumbnail", ImageView.class), fileLink.getSender().getProfileUrl(), true);
                    viewHolder.getView("right_name", TextView.class).setText(fileLink.getSender().getNickname());
                    if (fileLink.getType().toLowerCase().startsWith("image")) {
                        ChatHelper.displayUrlImage(viewHolder.getView("right_image", ImageView.class), fileLink.getUrl());
                    } else {
                        viewHolder.getView("right_image", ImageView.class).setImageResource(R.drawable.sendbird_icon_file);
                    }
                    viewHolder.getView("right_time", TextView.class).setText(ChatHelper.getDisplayDateTime(mContext, fileLink.getCreatedAt()));

                    viewHolder.getView("right_container").setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            new AlertDialog.Builder(mContext)
                                    .setTitle(R.string.file_download_title)
                                    .setMessage(R.string.file_download)
                                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            try {
                                                ChatHelper.downloadUrl(fileLink.getUrl(), fileLink.getName(), mContext);
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    })
                                    .setNegativeButton(R.string.no, null)
                                    .create()
                                    .show();
                        }
                    });
                } else {
                    viewHolder.getView("left_container", View.class).setVisibility(View.VISIBLE);
                    viewHolder.getView("right_container", View.class).setVisibility(View.GONE);

                    ChatHelper.displayUrlImage(viewHolder.getView("left_thumbnail", ImageView.class), fileLink.getSender().getProfileUrl(), true);
                    viewHolder.getView("left_name", TextView.class).setText(fileLink.getSender().getNickname());
                    if (fileLink.getType().toLowerCase().startsWith("image")) {
                        ChatHelper.displayUrlImage(viewHolder.getView("left_image", ImageView.class), fileLink.getUrl());
                    } else {
                        viewHolder.getView("left_image", ImageView.class).setImageResource(R.drawable.sendbird_icon_file);
                    }
                    viewHolder.getView("left_time", TextView.class).setText(ChatHelper.getDisplayDateTime(mContext, fileLink.getCreatedAt()));

                    viewHolder.getView("left_container").setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            new AlertDialog.Builder(mContext)
                                    .setTitle(R.string.file_download_title)
                                    .setMessage(R.string.file_download)
                                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            try {
                                                ChatHelper.downloadUrl(fileLink.getUrl(), fileLink.getName(), mContext);
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    })
                                    .setNegativeButton(R.string.no, null)
                                    .create()
                                    .show();
                        }
                    });
                }
                break;

            case TYPE_TYPING_INDICATOR: {
                int itemCount = ((List) item).size();
                String typeMsg = ((List) item).get(0)
                        + ((itemCount > 1) ? " +" + (itemCount - 1) : "")
                        + ((itemCount > 1) ? " are " : " is ")
                        + "typing...";
                viewHolder.getView("message", TextView.class).setText(typeMsg);
                break;
            }
        }

        return convertView;
    }

    private class ViewHolder {
        private Hashtable<String, View> holder = new Hashtable<>();
        private int type;

        public int getViewType() {
            return this.type;
        }

        public void setViewType(int type) {
            this.type = type;
        }

        public void setView(String k, View v) {
            holder.put(k, v);
        }

        public View getView(String k) {
            return holder.get(k);
        }

        public <T> T getView(String k, Class<T> type) {
            return type.cast(getView(k));
        }
    }
}



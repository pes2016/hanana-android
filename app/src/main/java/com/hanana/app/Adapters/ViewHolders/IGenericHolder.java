package com.hanana.app.Adapters.ViewHolders;

import android.content.Context;
import android.view.View;

import com.hanana.app.Adapters.IGenericAdapter;

public interface IGenericHolder extends View.OnClickListener{
    void initialize(Object o, Context context, IGenericAdapter adapter);
}

package com.hanana.app.badges.presenter;

import com.hanana.app.DataModels.BadgesList;
import com.hanana.app.badges.model.BadgesModel;
import com.hanana.app.badges.model.IBadgesModel;
import com.hanana.app.badges.view.IBadgesView;

public class BadgesPresenter implements IBadgesPresenter {
    public final static String TAG = "BadgesPresenter";
    private IBadgesModel mModel;
    private IBadgesView mView;

    public BadgesPresenter(IBadgesView view, String entityId) {
        this.mModel = new BadgesModel(this, entityId);
        mView = view;
    }

    @Override
    public void displayBadges(BadgesList badges, int page) {
        mView.displayBadges(badges,page);
    }

    @Override
    public void getData(int page) {
        mModel.getBadges(page);
    }
}

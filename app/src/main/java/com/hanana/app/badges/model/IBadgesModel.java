package com.hanana.app.badges.model;

public interface IBadgesModel {
    void getBadges(int page);
}

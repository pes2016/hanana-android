package com.hanana.app.badges.presenter;

import com.hanana.app.DataModels.Badge;
import com.hanana.app.DataModels.BadgesList;
import com.hanana.app.DataModels.User;

import java.util.List;

public interface IBadgesPresenter {
    void displayBadges(BadgesList badges, int page);
    void getData(int page);
}

package com.hanana.app.badges.model;

import android.util.Log;

import com.hanana.app.DataModels.BadgesList;
import com.hanana.app.DataModels.SearchObject;
import com.hanana.app.RESTInterface.UsersAPI;
import com.hanana.app.Utils.ActiveUser;
import com.hanana.app.Utils.Constants;
import com.hanana.app.badges.presenter.IBadgesPresenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BadgesModel implements IBadgesModel {
    private static final String TAG = "BadgesModel";
    private UsersAPI apiService;
    private IBadgesPresenter mPresenter;
    private String mEntityId;

    public BadgesModel(IBadgesPresenter presenter, String entityId) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.apiService = retrofit.create(UsersAPI.class);
        this.mPresenter = presenter;
        mEntityId = entityId;
    }

    @Override
    public void getBadges(final int page) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<BadgesList> call = apiService.getBadges(mEntityId, accessToken, new SearchObject(page));
        call.enqueue(new Callback<BadgesList>() {
            @Override
            public void onResponse(Call<BadgesList> call, Response<BadgesList> response) {
                int statusCode = response.code();
                Log.v(TAG,"getBadges: Status code " + statusCode + " body " + response.raw());
                BadgesList badgesList = response.body();
                mPresenter.displayBadges(badgesList, page);
            }

            @Override
            public void onFailure(Call<BadgesList> call, Throwable t) {
                Log.v(TAG,"getBadges Failure: " + t.getMessage());
            }

        });

    }
}

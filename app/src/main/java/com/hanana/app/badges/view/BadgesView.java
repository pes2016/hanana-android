package com.hanana.app.badges.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.hanana.app.Adapters.Behaviors.BadgeBehavior;
import com.hanana.app.Adapters.Behaviors.BadgeListStartBehavior;
import com.hanana.app.Adapters.Behaviors.IStartBehavior;
import com.hanana.app.Adapters.GenericAdapter;
import com.hanana.app.Adapters.GenericFragment;
import com.hanana.app.Adapters.IGenericAdapter;
import com.hanana.app.Adapters.IGenericFragment;
import com.hanana.app.Adapters.IMapFragment;
import com.hanana.app.DataModels.BadgesList;
import com.hanana.app.R;
import com.hanana.app.badges.presenter.BadgesPresenter;
import com.hanana.app.badges.presenter.IBadgesPresenter;

import static com.hanana.app.Utils.Constants.BADGES_HOLDER_TYPE;
import static com.hanana.app.Utils.Constants.ID;

public class BadgesView extends AppCompatActivity implements IBadgesView {
    private IGenericFragment badgesFragment;
    private IBadgesPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toolbar_single_fragment);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Bundle extras = getIntent().getExtras();
        String entityId = "";
        if (extras != null) {
            entityId = extras.getString(ID);
        }

        mPresenter = new BadgesPresenter(this, entityId);

        IGenericAdapter adapter = new GenericAdapter(
            this,
            R.layout.badge_item,
            BADGES_HOLDER_TYPE,
            new BadgeBehavior()
        );

        IStartBehavior badgeListStartBehavior = new BadgeListStartBehavior();

        badgesFragment = new GenericFragment();
        badgesFragment.initialize(adapter);
        badgesFragment.setStartBehavior(badgeListStartBehavior);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame, (Fragment) badgesFragment, "badgesView")
                .commit();
    }


    @Override
    public void getData(int page) {
        mPresenter.getData(page);
    }

    @Override
    public void displayBadges(BadgesList badges, int page) {
        if(page != 0){
            badgesFragment.appendData(badges.getBadges());
        }else{
            badgesFragment.setData(badges.getBadges());
        }
    }

    @Override
    public void getSuggestedEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getPopularEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getFriendsEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getGroups(int page, IGenericFragment fragment) {

    }

    @Override
    public void getEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getFutureEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getPastEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getUsers(int page, IGenericFragment fragment) {

    }

    @Override
    public void getMapEvents(double lat, double lon, double distance, IMapFragment fragment) {

    }
}

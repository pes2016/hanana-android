package com.hanana.app.badges.view;

import com.hanana.app.Adapters.OnFragmentRequestDataListener;
import com.hanana.app.DataModels.BadgesList;


public interface IBadgesView extends OnFragmentRequestDataListener {
    void displayBadges(BadgesList badges, int page);
}

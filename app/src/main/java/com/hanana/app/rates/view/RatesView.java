package com.hanana.app.rates.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.hanana.app.Adapters.Behaviors.IStartBehavior;
import com.hanana.app.Adapters.Behaviors.RateBehavior;
import com.hanana.app.Adapters.Behaviors.RateListStartBehavior;
import com.hanana.app.Adapters.GenericAdapter;
import com.hanana.app.Adapters.GenericFragment;
import com.hanana.app.Adapters.IGenericAdapter;
import com.hanana.app.Adapters.IGenericFragment;
import com.hanana.app.Adapters.IMapFragment;
import com.hanana.app.DataModels.RatesList;
import com.hanana.app.R;
import com.hanana.app.rates.presenter.IRatesPresenter;
import com.hanana.app.rates.presenter.RatesPresenter;

import static com.hanana.app.Utils.Constants.ID;
import static com.hanana.app.Utils.Constants.RATE_HOLDER_TYPE;

public class RatesView extends AppCompatActivity implements IRatesView {
    private IGenericFragment ratesFragment;
    private IRatesPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toolbar_single_fragment);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Bundle extras = getIntent().getExtras();
        String entityId = "";
        if (extras != null) {
            entityId = extras.getString(ID);
        }
        setTitle(getString(R.string.rates));

        mPresenter = new RatesPresenter(this, entityId);

        IGenericAdapter adapter = new GenericAdapter(
                this,
                R.layout.rate_item,
                RATE_HOLDER_TYPE,
                new RateBehavior()
        );

        IStartBehavior rateListStartBehavior = new RateListStartBehavior();

        ratesFragment = new GenericFragment();
        ratesFragment.initialize(adapter);
        ratesFragment.setStartBehavior(rateListStartBehavior);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame, (Fragment) ratesFragment, "RatesView")
                .commit();
    }


    @Override
    public void getData(int page) {
        mPresenter.getRates(page);
    }

    @Override
    public void displayRates(RatesList rates, int page) {
        if(page != 0){
            ratesFragment.appendData(rates.getRates());
        }
        else{
            ratesFragment.setData(rates.getRates());
        }
    }

    @Override
    public void downVote(String id, int position) {
        mPresenter.downVote(id, position);
    }

    @Override
    public void upVote(String id, int position) {
        mPresenter.upVote(id, position);
    }

    @Override
    public void notifyVoted(int position) {
        ratesFragment.onServerResponse(position);
    }

    @Override
    public void showComment(int position) {
        ratesFragment.onServerResponse(position);
    }

    @Override
    public void getSuggestedEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getPopularEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getFriendsEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getGroups(int page, IGenericFragment fragment) {

    }

    @Override
    public void getEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getFutureEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getPastEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getUsers(int page, IGenericFragment fragment) {

    }

    @Override
    public void getMapEvents(double lat, double lon, double distance, IMapFragment fragment) {

    }
}

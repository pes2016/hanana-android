package com.hanana.app.rates.presenter;

import com.hanana.app.DataModels.RatesList;
import com.hanana.app.rates.model.IRatesModel;
import com.hanana.app.rates.model.RatesModel;
import com.hanana.app.rates.view.IRatesView;

public class RatesPresenter implements IRatesPresenter {
    private IRatesView mView;
    private IRatesModel mModel;

    public RatesPresenter(IRatesView view, String entityId) {
        super();
        mView = view;
        mModel = new RatesModel(this, entityId);
    }

    @Override
    public void downVote(String id, int position) {
        mModel.downVote(id, position);
    }

    @Override
    public void upVote(String id, int position) {
        mModel.upVote(id, position);
    }

    @Override
    public void getRates(int page) {
        mModel.getRates(page);
    }

    @Override
    public void displayRates(RatesList rates, int page) {
        mView.displayRates(rates, page);
    }

    @Override
    public void notifyVoted(int position) {
        mView.notifyVoted(position);
    }
}

package com.hanana.app.rates.model;

public interface IRatesModel {
    void downVote(String id, int position);
    void upVote(String id, int position);
    void getRates(int page);
}

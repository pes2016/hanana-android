package com.hanana.app.rates.view;

import com.hanana.app.Adapters.OnFragmentRequestDataListener;
import com.hanana.app.DataModels.RatesList;

public interface IRatesView extends OnFragmentRequestDataListener {
    void displayRates(RatesList rates, int page);
    void upVote(String id, int position);
    void downVote(String id, int position);
    void notifyVoted(int position);
    void showComment(int position);
}

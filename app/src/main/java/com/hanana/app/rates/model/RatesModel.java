package com.hanana.app.rates.model;

import android.util.Log;

import com.hanana.app.DataModels.ComplaintObject;
import com.hanana.app.DataModels.Event;
import com.hanana.app.DataModels.RateObject;
import com.hanana.app.DataModels.RatesList;
import com.hanana.app.DataModels.SearchObject;
import com.hanana.app.RESTInterface.ComplainsAPI;
import com.hanana.app.RESTInterface.EventsAPI;
import com.hanana.app.Utils.ActiveUser;
import com.hanana.app.Utils.Constants;
import com.hanana.app.createEvent.presenter.ICreateEventPresenter;
import com.hanana.app.event.model.IEventModel;
import com.hanana.app.event.presenter.IEventPresenter;
import com.hanana.app.rates.presenter.IRatesPresenter;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.hanana.app.Utils.Constants.EVENT;

public class RatesModel implements IRatesModel{
    private IRatesPresenter mPresenter;
    private final static String TAG = "RatesModel";
    private EventsAPI eventsAPIService;
    private String mEventId;


    public RatesModel(IRatesPresenter presenter, String eventId) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mPresenter = presenter;
        eventsAPIService = retrofit.create(EventsAPI.class);
        mEventId = eventId;
    }

    @Override
    public void getRates(final int page) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<RatesList> call = eventsAPIService.getRates(mEventId, accessToken, new SearchObject(page));
        call.enqueue(new Callback<RatesList>() {
            @Override
            public void onResponse(Call<RatesList> call, Response<RatesList> response) {
                int statusCode = response.code();
                if (statusCode < 300) {
                    RatesList rates = response.body();
                    mPresenter.displayRates(rates, page);
                }
            }

            @Override
            public void onFailure(Call<RatesList> call, Throwable t) {
                Log.v(TAG,"Failure");
            }

        });
    }

    @Override
    public void downVote(String id, final int position) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<Boolean> call = eventsAPIService.downVoteRate(mEventId, id, accessToken);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                int statusCode = response.code();
                if (statusCode < 300) {
                    mPresenter.notifyVoted(position);
                }
                Log.v(TAG,"downVote: Status code " + statusCode + " body " + response.raw());
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.v(TAG,"Failure");
            }

        });
    }

    @Override
    public void upVote(String id, final int position) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<Boolean> call = eventsAPIService.upVoteRate(mEventId, id, accessToken);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                int statusCode = response.code();
                if (statusCode < 300) {
                    mPresenter.notifyVoted(position);
                }
                Log.v(TAG,"upVote: Status code " + statusCode + " body " + response.raw());
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.v(TAG,"Failure");
            }

        });
    }
}

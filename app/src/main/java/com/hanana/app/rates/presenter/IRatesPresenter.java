package com.hanana.app.rates.presenter;

import com.hanana.app.DataModels.RatesList;

public interface IRatesPresenter {
    void getRates(int page);
    void upVote(String id, int position);
    void downVote(String id, int position);
    void displayRates(RatesList rates, int page);
    void notifyVoted(int position);
}

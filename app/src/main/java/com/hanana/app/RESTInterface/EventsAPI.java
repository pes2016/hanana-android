package com.hanana.app.RESTInterface;

import com.hanana.app.DataModels.RatesList;
import com.hanana.app.DataModels.Event;
import com.hanana.app.DataModels.EventsList;
import com.hanana.app.DataModels.MapSearchObject;
import com.hanana.app.DataModels.SearchObject;
import com.hanana.app.DataModels.RateObject;
import com.hanana.app.DataModels.UsersList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static com.hanana.app.Utils.Constants.SERVER_EVENTS_ASSISTANTS;
import static com.hanana.app.Utils.Constants.SERVER_EVENTS_DOWNVOTE;
import static com.hanana.app.Utils.Constants.SERVER_EVENTS_FRIENDS;
import static com.hanana.app.Utils.Constants.SERVER_EVENTS_MAP;
import static com.hanana.app.Utils.Constants.SERVER_EVENTS_POPULAR;
import static com.hanana.app.Utils.Constants.SERVER_EVENTS_RATES;
import static com.hanana.app.Utils.Constants.SERVER_EVENTS_SHARE;
import static com.hanana.app.Utils.Constants.SERVER_EVENTS_SUGGESTED;
import static com.hanana.app.Utils.Constants.SERVER_EVENTS_UPVOTE;
import static com.hanana.app.Utils.Constants.SERVER_EVENT_CONFIRM_QR;
import static com.hanana.app.Utils.Constants.SERVER_EVENTS_CREATE;
import static com.hanana.app.Utils.Constants.SERVER_EVENTS_PRIVATE_UPDATE_IMAGE;
import static com.hanana.app.Utils.Constants.SERVER_RATE_EVENT;
import static com.hanana.app.Utils.Constants.SERVER_SINGLE_EVENT;
import static com.hanana.app.Utils.Constants.SERVER_TOGGLE_ASSIST;
import static com.hanana.app.Utils.Constants.SERVER_USER_EVENTS;

public interface EventsAPI {

    @POST(SERVER_EVENTS_MAP)
    Call<EventsList> getMapEvents(@Query("access_token") String access_token, @Body MapSearchObject query);

    @POST(SERVER_EVENTS_SUGGESTED)
    Call<EventsList> getSuggestedEvents(@Query("access_token") String access_token, @Body SearchObject query);

    @POST(SERVER_EVENTS_POPULAR)
    Call<EventsList> getPopularEvents(@Query("access_token") String access_token, @Body SearchObject query);

    @POST(SERVER_EVENTS_FRIENDS)
    Call<EventsList> getFriendsEvents(@Query("access_token") String access_token, @Body SearchObject query);

    @POST(SERVER_EVENTS_ASSISTANTS)
    Call<UsersList> getAssistants(@Path("id") String id, @Query("access_token") String access_token, @Body SearchObject query);

    @POST(SERVER_USER_EVENTS)
    Call<EventsList> getUserEvents(@Path("id") String id, @Path("type") String type, @Query("access_token") String access_token, @Body SearchObject query);

    @GET(SERVER_SINGLE_EVENT)
    Call<Event> getEvent(@Path("id") String id, @Query("access_token") String access_token);

    @POST(SERVER_TOGGLE_ASSIST)
    Call<Boolean> toggleAssist(@Path("id") String id, @Query("access_token") String access_token);

    @POST(SERVER_RATE_EVENT)
    Call<Void> rateEvent(@Path("id") String id, @Body RateObject rate, @Query("access_token") String access_token);

    @POST(SERVER_EVENTS_RATES)
    Call<RatesList> getRates(@Path("id") String id, @Query("access_token") String access_token, @Body SearchObject query);

    @POST(SERVER_EVENTS_DOWNVOTE)
    Call<Boolean> downVoteRate(@Path("eventId") String eventId, @Path("id") String id, @Query("access_token") String access_token);

    @POST(SERVER_EVENTS_UPVOTE)
    Call<Boolean> upVoteRate(@Path("eventId") String eventId, @Path("id") String id, @Query("access_token") String access_token);

    @POST(SERVER_EVENT_CONFIRM_QR)
    Call<Boolean> confirmQR(@Path("id") String id, @Query("access_token") String access_token);

    @POST(SERVER_EVENTS_SHARE)
    Call<Void> share(@Path("id") String id, @Query("access_token") String accessToken);

    @POST(SERVER_EVENTS_CREATE)
    Call<Event> createEvent(
            @Body Event event,
            @Query("access_token") String access_token);

    @Multipart
    @POST(SERVER_EVENTS_PRIVATE_UPDATE_IMAGE)
    Call<Event> updateAvatar(
            @Path("id") String id,
            @Part MultipartBody.Part file,
            @Query("access_token") String access_token);
}

package com.hanana.app.RESTInterface;

import com.hanana.app.DataModels.Group;
import com.hanana.app.DataModels.GroupList;
import com.hanana.app.DataModels.SearchObject;
import com.hanana.app.DataModels.UsersList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static com.hanana.app.Utils.Constants.SERVER_GROUPS_DISCOVER;
import static com.hanana.app.Utils.Constants.SERVER_GROUP_FOLLOW;
import static com.hanana.app.Utils.Constants.SERVER_GROUP_FOLLOWERS;
import static com.hanana.app.Utils.Constants.SERVER_GROUP_SHARE;
import static com.hanana.app.Utils.Constants.SERVER_SINGLE_GROUP;

public interface GroupsAPI {

    @POST(SERVER_GROUPS_DISCOVER)
    Call<GroupList> searchGroups(@Query("access_token") String access_token, @Body SearchObject query);

    @GET(SERVER_SINGLE_GROUP)
    Call<Group> getGroup(@Path("id") String id, @Query("access_token") String access_token);

    @POST(SERVER_GROUP_FOLLOW)
    Call<Boolean> toggleFollowing(@Path("id") String id, @Query("access_token") String access_token);

    @POST(SERVER_GROUP_FOLLOWERS)
    Call<UsersList> getFollowers(@Path("id") String id, @Query("access_token") String access_token, @Body SearchObject query);

    @POST(SERVER_GROUP_SHARE)
    Call<Void> share(@Path("id") String id, @Query("access_token") String accessToken);
}

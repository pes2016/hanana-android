package com.hanana.app.RESTInterface;

import com.hanana.app.DataModels.ComplaintObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static com.hanana.app.Utils.Constants.SERVER_COMPLAINTS;

public interface ComplainsAPI {

    @POST(SERVER_COMPLAINTS)
    Call<Void> complain(@Path("id") String id, @Path("type") String type, @Query("access_token") String access_token, @Body ComplaintObject complaint);
}

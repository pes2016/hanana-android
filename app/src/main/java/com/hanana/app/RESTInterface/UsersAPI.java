package com.hanana.app.RESTInterface;

import com.hanana.app.DataModels.BadgesList;
import com.hanana.app.DataModels.OtherUser;
import com.hanana.app.DataModels.User;
import com.hanana.app.DataModels.SearchObject;
import com.hanana.app.DataModels.Biography;
import com.hanana.app.DataModels.FacebookUserData;
import com.hanana.app.DataModels.Interest;
import com.hanana.app.DataModels.UsersList;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static com.hanana.app.Utils.Constants.SERVER_ACCEPT_REQUEST;
import static com.hanana.app.Utils.Constants.SERVER_CLOSE_ACCOUNT;
import static com.hanana.app.Utils.Constants.SERVER_DECLINE_REQUEST;
import static com.hanana.app.Utils.Constants.SERVER_GET_PENDING_REQUESTS;
import static com.hanana.app.Utils.Constants.SERVER_ME;
import static com.hanana.app.Utils.Constants.SERVER_ME_BLOCKED;
import static com.hanana.app.Utils.Constants.SERVER_MY_INTERESTS;
import static com.hanana.app.Utils.Constants.SERVER_RANKING;
import static com.hanana.app.Utils.Constants.SERVER_TOGGLE_NOTIFICATIONS;
import static com.hanana.app.Utils.Constants.SERVER_USER_BADGES;
import static com.hanana.app.Utils.Constants.SERVER_USER_BLOCK;
import static com.hanana.app.Utils.Constants.SERVER_USER_FOLLOW;
import static com.hanana.app.Utils.Constants.SERVER_USER_FOLLOWERS;
import static com.hanana.app.Utils.Constants.SERVER_USER_FOLLOWING;
import static com.hanana.app.Utils.Constants.SERVER_USER_INTERESTS;
import static com.hanana.app.Utils.Constants.SERVER_REGISTER_FACEBOOK;
import static com.hanana.app.Utils.Constants.SERVER_SINGLE_USER;
import static com.hanana.app.Utils.Constants.SERVER_TOGGLE_PRIVACITY;
import static com.hanana.app.Utils.Constants.SERVER_UPDATE_BIO;
import static com.hanana.app.Utils.Constants.SERVER_UPLOAD_AVATAR;
import static com.hanana.app.Utils.Constants.SERVER_USERS_DISCOVER;

public interface UsersAPI {

    @POST(SERVER_REGISTER_FACEBOOK)
    Call<User> signUpFacebook(@Body FacebookUserData data);

    @GET(SERVER_CLOSE_ACCOUNT)
    Call<Void> closeAccount(@Query("access_token") String access_token);

    @GET(SERVER_ME)
    Call<User> me(@Query("access_token") String access_token);

    @GET(SERVER_SINGLE_USER)
    Call<OtherUser> getUser(@Path("id") String id, @Query("access_token") String access_token);

    @POST(SERVER_UPDATE_BIO)
    Call<Void> updateBio(@Query("access_token") String access_token, @Body Biography bio);

    @GET(SERVER_TOGGLE_PRIVACITY)
    Call<Void> togglePrivacity(@Query("access_token") String access_token);

    @POST(SERVER_USER_FOLLOW)
    Call<Void> toggleFollowing(@Path("id") String id, @Query("access_token") String access_token);

    @GET(SERVER_USER_INTERESTS)
    Call<List<Interest>> getInterests(@Path("id") String id, @Query("access_token") String access_token);

    @POST(SERVER_MY_INTERESTS)
    Call<Void> setUserInterests(@Query("access_token") String access_token, @Body List<Interest> interests);

    @Multipart
    @POST(SERVER_UPLOAD_AVATAR)
    Call<ResponseBody> uploadAvatar(
            @Part MultipartBody.Part file,
            @Query("access_token") String access_token);

    @POST(SERVER_USERS_DISCOVER)
    Call<UsersList> searchUsers(@Query("access_token") String access_token, @Body SearchObject query);

    @POST(SERVER_USER_FOLLOWERS)
    Call<UsersList> getFollowers(@Path("id") String id, @Query("access_token") String accessToken, @Body SearchObject query);

    @POST(SERVER_USER_FOLLOWING)
    Call<UsersList> getFollowing(@Path("id") String id, @Query("access_token") String accessToken, @Body SearchObject query);


    @POST(SERVER_ACCEPT_REQUEST)
    Call<Void> acceptRequest(@Path("id") String id, @Query("access_token") String access_token);

    @POST(SERVER_DECLINE_REQUEST)
    Call<Void> declineRequest(@Path("id") String id ,@Query("access_token") String access_token);

    @POST(SERVER_GET_PENDING_REQUESTS)
    Call<UsersList> getPendingRequests(@Query("access_token") String access_toke, @Body SearchObject query);

    @POST(SERVER_USER_BLOCK)
    Call<Boolean> blockUser(@Path("id") String id, @Query("access_token") String access_token);

    @POST(SERVER_ME_BLOCKED)
    Call<UsersList> getBlocked(@Query("access_token") String access_token, @Body SearchObject query);

    @POST(SERVER_USER_BADGES)
    Call<BadgesList> getBadges(@Path("id") String id, @Query("access_token") String access_token, @Body SearchObject query);

    @POST(SERVER_RANKING)
    Call<UsersList> getRanking(@Query("access_token") String access_token, @Body SearchObject query);

    @POST(SERVER_TOGGLE_NOTIFICATIONS)
    Call<Boolean> toggleNotifications(@Query("access_token") String access_token);

}
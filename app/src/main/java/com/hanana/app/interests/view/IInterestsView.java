package com.hanana.app.interests.view;

import com.hanana.app.DataModels.Interest;

import java.util.List;



public interface IInterestsView {
    void showUserIntersts(List<Interest> l);
    void updateUserInterests();
}

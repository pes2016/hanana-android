package com.hanana.app.interests.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hanana.app.DataModels.Interest;
import com.hanana.app.R;
import com.hanana.app.Adapters.InterestsAdapter;
import com.hanana.app.interests.presenter.IInterestsPresenter;
import com.hanana.app.interests.presenter.InterestsPresenter;

import java.util.List;

import static com.hanana.app.Utils.Constants.IS_MY_PROFILE;
import static com.hanana.app.Utils.Constants.IS_OTHERS_PROFILE;

public class InterestsView extends Fragment implements IInterestsView{
    private final static String TAG = "InterestsView";

    private static final String WHERE = "where";
    private static final String USER_ID = "userId";

    private int mWhere;
    private String mUserId;

    private IInterestsPresenter mPresenter;
    private InterestsAdapter interestAdapter;
    private RecyclerView recyclerView;
    private TextView emptyView;

    public InterestsView() {
        // Required empty public constructor
    }

    public static InterestsView newInstance(int where, String userId) {
        InterestsView fragment = new InterestsView();
        Bundle args = new Bundle();
        args.putInt(WHERE, where);
        args.putString(USER_ID, userId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mWhere = getArguments().getInt(WHERE);
            mUserId = getArguments().getString(USER_ID);
        }
        mPresenter = new InterestsPresenter(this, mUserId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview =  inflater.inflate(R.layout.fragment_list_view, container, false);
        recyclerView = (RecyclerView) rootview.findViewById(R.id.itemsList);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        emptyView = (TextView) rootview.findViewById(R.id.empty_view);
        interestAdapter = new InterestsAdapter(getActivity(),this, mWhere != IS_OTHERS_PROFILE);
        recyclerView.setAdapter(interestAdapter);

        if(mWhere == IS_MY_PROFILE){
            interestAdapter.activeProfile();
        }

        return rootview;
    }

    @Override
    public void showUserIntersts(List<Interest> l) {
        if (mWhere == IS_OTHERS_PROFILE) {
            boolean empty = true;
            for (Interest i : l) {
                if (i.isSelected()) {
                    empty = false;
                    break;
                }
            }
            if (empty) {
                recyclerView.setVisibility(View.GONE);
                emptyView.setVisibility(View.VISIBLE);
            }
            else {
                recyclerView.setVisibility(View.VISIBLE);
                emptyView.setVisibility(View.GONE);
            }
        }
        interestAdapter.setData(l);
        recyclerView.invalidate();
    }

    @Override
    public void updateUserInterests() {
        if(interestAdapter != null) {
            mPresenter.updateUserInterests(interestAdapter.getData());
        }
        else{
            Log.d(TAG, "updateUserInterests: adapter is null");
        }
    }
}

package com.hanana.app.interests.model;

import android.util.Log;

import com.hanana.app.Utils.ActiveUser;
import com.hanana.app.DataModels.Interest;
import com.hanana.app.RESTInterface.UsersAPI;
import com.hanana.app.Utils.Constants;
import com.hanana.app.interests.presenter.IInterestsPresenter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class InterestsModel implements IInterestsModel{
    private static final String TAG = "InterestsModel";
    private UsersAPI apiService;
    private List<Interest> userInterests;
    private IInterestsPresenter mPresenter;

    public InterestsModel(IInterestsPresenter presenter) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.apiService = retrofit.create(UsersAPI.class);
        this.mPresenter = presenter;
    }

    @Override
    public void getUserInterests(String userId) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<List<Interest>> call = apiService.getInterests(userId, accessToken);
        call.enqueue(new Callback<List<Interest>>() {
            @Override
            public void onResponse(Call<List<Interest>> call, Response<List<Interest>> response) {
                userInterests = response.body();
                mPresenter.showUserInterests(userInterests);
            }

            @Override
            public void onFailure(Call<List<Interest>> call, Throwable t) {
                Log.v(TAG,"Failure");
            }

        });
    }

    @Override
    public void setUserInterests(List<Interest> l) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<Void> call = apiService.setUserInterests(accessToken, l);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                int statusCode = response.code();
                Log.v(TAG,"Status code " + statusCode + " body " + response.raw());
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }
}

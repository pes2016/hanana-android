package com.hanana.app.interests.model;

import com.hanana.app.DataModels.Interest;

import java.util.List;

public interface IInterestsModel {
    void getUserInterests(String userId);
    void setUserInterests(List<Interest> l );
}

package com.hanana.app.interests.presenter;

import com.hanana.app.DataModels.Interest;

import java.util.List;

public interface IInterestsPresenter {
    void showUserInterests(List<Interest> userInterest);
    void updateUserInterests(List<Interest> userInterest);
}

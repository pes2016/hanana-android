package com.hanana.app.interests.presenter;

import com.hanana.app.DataModels.Interest;
import com.hanana.app.interests.model.IInterestsModel;
import com.hanana.app.interests.model.InterestsModel;
import com.hanana.app.interests.view.IInterestsView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class InterestsPresenter implements  IInterestsPresenter{
    public final static String TAG = "InterestsPresenter";

    private IInterestsModel mModel;
    private IInterestsView mView;

    public InterestsPresenter(IInterestsView mView, String userId) {
        super();
        this.mModel = new InterestsModel(this);
        this.mModel.getUserInterests(userId);
        this.mView = mView;
    }

    @Override
    public void showUserInterests(List<Interest> userInterest) {
        mView.showUserIntersts(userInterest);
    }

    @Override
    public void updateUserInterests(List<Interest> userInterests) {
        List<Interest> filtered = new ArrayList<>();
        Iterator<Interest> iter = userInterests.iterator();
        //noinspection WhileLoopReplaceableByForEach
        while (iter.hasNext()) {
            Interest interest = iter.next();
            if (interest.isSelected()) {
                filtered.add(interest);
            }
        }
        mModel.setUserInterests(filtered);
    }

}

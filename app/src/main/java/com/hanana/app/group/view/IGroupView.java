package com.hanana.app.group.view;

import com.hanana.app.Adapters.OnFragmentRequestDataListener;
import com.hanana.app.DataModels.Group;

public interface IGroupView extends OnFragmentRequestDataListener{
    void displayGroup(Group group);
    void activateButton();
    void toggleButton();
}

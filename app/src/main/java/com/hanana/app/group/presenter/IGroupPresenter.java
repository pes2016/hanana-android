package com.hanana.app.group.presenter;

import com.hanana.app.DataModels.Group;

public interface IGroupPresenter {
    void getGroup(String groupId);
    void displayGroup(Group group);
    void toggleFollowing(String groupId);
    void activateButton();
    void toggleButton();
    void complainGroup(String groupId, String comment);
    void hasShared(String groupId);
}

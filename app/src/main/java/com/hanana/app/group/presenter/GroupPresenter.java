package com.hanana.app.group.presenter;

import com.hanana.app.DataModels.Group;
import com.hanana.app.group.model.GroupModel;
import com.hanana.app.group.model.IGroupModel;
import com.hanana.app.group.view.IGroupView;

public class GroupPresenter implements IGroupPresenter {
    private IGroupView mView;
    private IGroupModel mModel;

    public GroupPresenter(IGroupView view) {
        this.mView = view;
        this.mModel = new GroupModel(this);
    }

    @Override
    public void getGroup(String groupId) {
        mModel.getGroup(groupId);
    }

    @Override
    public void displayGroup(Group group) {
        mView.displayGroup(group);
    }

    @Override
    public void toggleFollowing(String groupId) {
        mModel.toggleFollowing(groupId);
    }

    @Override
    public void activateButton() {
        mView.activateButton();
    }

    @Override
    public void toggleButton() {
        mView.toggleButton();
    }

    @Override
    public void complainGroup(String groupId, String comment) {
        mModel.complainGroup(groupId, comment);
    }

    @Override
    public void hasShared(String groupId) {
        mModel.hasShared(groupId);
    }
}

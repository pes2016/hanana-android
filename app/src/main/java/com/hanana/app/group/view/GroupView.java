package com.hanana.app.group.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.ShareActionProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.hanana.app.Adapters.IGenericFragment;
import com.hanana.app.Adapters.IMapFragment;
import com.hanana.app.Views.BaseActivity;
import com.hanana.app.DataModels.Group;
import com.hanana.app.R;
import com.hanana.app.Adapters.TabControllers.GroupFragmentAdapter;
import com.hanana.app.Utils.ImageLoader;
import com.hanana.app.Utils.StringFormatter;
import com.hanana.app.follows.view.FollowsView;
import com.hanana.app.group.presenter.GroupPresenter;
import com.hanana.app.group.presenter.IGroupPresenter;

import static com.hanana.app.Utils.Constants.GROUPS;
import static com.hanana.app.Utils.Constants.ID;
import static com.hanana.app.Utils.Constants.LIST_TYPE;

public class GroupView extends BaseActivity implements IGroupView, View.OnClickListener {
    private final static String TAG = "GroupView";
    private IGroupPresenter mPresenter;
    private IGenericFragment nextFragment, pastFragment;
    private Group group;
    private ImageView groupCoverImage, groupImage, groupFollowButton;
    private TextView groupName, groupFollowers, groupEvents, groupDescription;
    private ShareActionProvider mShareActionProvider;
    private String groupId;
    private Context context;
    private Intent shareIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_group_view);

        context = this;

        initializeUI();

        TabLayout tabLayout = (TabLayout) findViewById(R.id.groupTabLayout);
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.next_events)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.past_events)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.groupPager);
        final GroupFragmentAdapter adapter = new GroupFragmentAdapter
                (context, tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        initialize(this);
    }

    @Override
    protected int whoIAm() {
        return 0;
    }

    private void initialize(IGroupView view) {
        mPresenter = new GroupPresenter(view);
        Bundle extras = getIntent().getExtras();
        groupId = "";
        if (extras != null) {
            groupId = extras.getString(ID, "");
        }
        else {
            Log.d(TAG, "initialize: extras are null");
        }
        mPresenter.getGroup(groupId);
    }

    private void initializeUI() {
        groupCoverImage = (ImageView) findViewById(R.id.groupCoverImage);
        groupImage = (ImageView) findViewById(R.id.groupImage);
        groupName = (TextView) findViewById(R.id.groupName);
        groupFollowers = (TextView) findViewById(R.id.groupFollowers);
        groupEvents = (TextView) findViewById(R.id.groupEvents);
        groupDescription = (TextView) findViewById(R.id.groupDescription);
        groupFollowButton = (ImageView) findViewById(R.id.groupFollowButton);
        groupFollowButton.setOnClickListener(this);
        groupFollowers.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.groupFollowButton:
                mPresenter.toggleFollowing(group.get_Id());
                groupFollowButton.setClickable(false);
                break;

            case R.id.groupFollowers:
                Intent i = new Intent(context, FollowsView.class);
                i.putExtra(ID,group.get_Id());
                i.putExtra(LIST_TYPE, GROUPS);
                startActivity(i);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate menu resource file.
        getMenuInflater().inflate(R.menu.group_single_menu, menu);
        // Locate MenuItem with ShareActionProvider
        MenuItem item = menu.findItem(R.id.menu_item_share);
        // Fetch and store ShareActionProvider
        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);

        if (shareIntent != null) {
            Log.d(TAG, "onCreateOptionsMenu: setShareIntent before menu");
            mShareActionProvider.setShareIntent(shareIntent);
        }

        mShareActionProvider.setOnShareTargetSelectedListener(new ShareActionProvider.OnShareTargetSelectedListener() {
            @Override
            public boolean onShareTargetSelected(ShareActionProvider source, Intent intent) {
                Log.d(TAG, "onShareTargetSelected: hasShared");
                mPresenter.hasShared(groupId);
                return false;
            }
        });
        // Return true to display menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_report) {
            LayoutInflater inflater = getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.report_dialog, null);

            AlertDialog.Builder reportDialog = new AlertDialog.Builder(context);

            reportDialog.setTitle(R.string.report_dialog)
                    .setView(dialogView)
                    .setPositiveButton(getString(R.string.send), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            Log.d(TAG, "onAccept: ");
                            String comment = ((EditText)dialogView.findViewById(R.id.dialog_report_desc)).getText().toString();
                            mPresenter.complainGroup(groupId, comment);
                        }
                    });
            reportDialog.create();
            reportDialog.show();
        }
        return true;
    }

    // Call to update the share intent
    private void setShareIntent() {
        Log.d(TAG, "setShareIntent: start set share intent");
        shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, StringFormatter.getGroupShareString(this,group));

        if (mShareActionProvider != null) {
            Log.d(TAG, "setShareIntent: share provider is ok");
            mShareActionProvider.setShareIntent(shareIntent);
        } else {
            Log.d(TAG, "setShareIntent: share provider is null");
        }
    }

    @Override
    public void activateButton() {
        groupFollowButton.setClickable(true);
    }

    @Override
    public void toggleButton() {
        if(group.isFollowing()){
            groupFollowButton.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_group_follow, null));
            group.setFollowing(false);
        } else {
            groupFollowButton.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_cancel, null));
            group.setFollowing(true);
        }
        groupFollowButton.setClickable(true);
    }

    @Override
    public void displayGroup(Group groupModel) {
        if (groupModel == null) {
            Log.v(TAG, "GROUP IS NULL");
            return;
        }
        group = groupModel;
        setShareIntent();

        ImageLoader.loadImage(this, group.getImage(), groupCoverImage);
        ImageLoader.loadSquareImageCenterCropWidthHeight(this,group.getImage(),groupImage,110,110);
        groupName.setText(group.getName());

        groupFollowers.setText(StringFormatter.getNumberOfFollowersWordFormatted(this,group));
        groupEvents.setText(StringFormatter.getNumberOfEventsWordFormatted(this,group));
        groupDescription.setText(group.getDescription());

        if (group.isFollowing()) {
            groupFollowButton.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_cancel, null));
        }
        else {
            groupFollowButton.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_group_follow, null));
        }
        //TODO presenter + model
        if (nextFragment != null) nextFragment.setData(group.getFutureEvents());
        if (pastFragment != null) pastFragment.setData(group.getPastEvents());
    }

    @Override
    public void getFutureEvents(int page, IGenericFragment fragment) {
        if (nextFragment == null) {
            nextFragment = fragment;
        }
        if (group != null) nextFragment.setData(group.getFutureEvents());
        //TODO presenter + model
        //TODO allow paging requests
    }

    @Override
    public void getPastEvents(int page, IGenericFragment fragment) {
        if (pastFragment == null) {
            pastFragment = fragment;
        }
        if (group != null) pastFragment.setData(group.getPastEvents());
        //TODO presenter + model
        //TODO allow paging requests
    }

    @Override
    public void getEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getSuggestedEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getPopularEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getFriendsEvents(int page, IGenericFragment fragment) {

    }

    @Override
    public void getGroups(int page, IGenericFragment fragment) {

    }

    @Override
    public void getUsers(int page, IGenericFragment fragment) {

    }

    @Override
    public void getMapEvents(double lat, double lon, double distance, IMapFragment fragment) {

    }

    @Override
    public void getData(int page) {

    }
}

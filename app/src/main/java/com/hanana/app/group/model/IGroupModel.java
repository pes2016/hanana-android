package com.hanana.app.group.model;

public interface IGroupModel {
    void getGroup(String groupId);
    void toggleFollowing(String groupId);
    void complainGroup(String groupId, String comment);
    void hasShared(String groupId);
}

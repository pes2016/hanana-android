package com.hanana.app.group.model;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessaging;
import com.hanana.app.DataModels.ComplaintObject;
import com.hanana.app.DataModels.Group;
import com.hanana.app.RESTInterface.ComplainsAPI;
import com.hanana.app.RESTInterface.GroupsAPI;
import com.hanana.app.Utils.ActiveUser;
import com.hanana.app.Utils.Constants;
import com.hanana.app.group.presenter.IGroupPresenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.hanana.app.Utils.Constants.GROUP;

public class GroupModel implements IGroupModel {
    private final static String TAG ="GroupModel";
    private IGroupPresenter mPresenter;
    private GroupsAPI groupsAPIService;
    private ComplainsAPI complainsAPIService;

    public GroupModel(IGroupPresenter groupPresenter) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.mPresenter = groupPresenter;
        this.groupsAPIService = retrofit.create(GroupsAPI.class);
        this.complainsAPIService = retrofit.create(ComplainsAPI.class);
    }

    @Override
    public void getGroup(String groupId) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<Group> call = groupsAPIService.getGroup(groupId, accessToken);
        call.enqueue(new Callback<Group>() {
            @Override
            public void onResponse(Call<Group> call, Response<Group> response) {
                Group group = response.body();
                Log.v(TAG, response.raw().toString());
                mPresenter.displayGroup(group);
            }

            @Override
            public void onFailure(Call<Group> call, Throwable t) {
                Log.e(TAG + " failure", t.getMessage());
            }
        });
    }

    @Override
    public void toggleFollowing(final String groupId) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<Boolean> call = groupsAPIService.toggleFollowing(groupId, accessToken);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                int statusCode = response.code();
                Log.d(TAG, "onResponse: toggleFollowing status:" + statusCode);
                if (statusCode < 300) {
                    Boolean following = response.body();
                    String url = "/topics/notificationsgroup" + groupId;
                    if (following) {
                        FirebaseMessaging.getInstance().subscribeToTopic(url);
                    }
                    else {
                        FirebaseMessaging.getInstance().unsubscribeFromTopic(url);
                    }
                    mPresenter.toggleButton();
                }
                else {
                    mPresenter.activateButton();
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.e(TAG + " failure", t.getMessage());
            }
        });
    }

    @Override
    public void complainGroup(String groupId, String comment) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<Void> call = complainsAPIService.complain(groupId, GROUP, accessToken, new ComplaintObject(comment));
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                int statusCode = response.code();
                if (statusCode < 300) {
                    Log.d(TAG, "onResponse: ok");
                }
                Log.v(TAG,"complainGroup: Status code " + statusCode + " body " + response.raw());
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.v(TAG,"Failure");
            }
        });
    }

    @Override
    public void hasShared(String groupId) {
        String accessToken = ActiveUser.getInstance().getAccess_token();
        Call<Void> call = groupsAPIService.share(groupId, accessToken);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                int statusCode = response.code();
                Log.v(TAG,"hasShared: Status code " + statusCode);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.v(TAG,"Failure");
            }
        });
    }
}

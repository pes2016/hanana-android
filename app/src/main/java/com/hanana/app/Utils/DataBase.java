package com.hanana.app.Utils;

import android.util.Log;

import com.hanana.app.DataModels.Session;

import io.realm.Realm;

public class DataBase {
    private static DataBase instance = null;
    private Realm realm;

    private DataBase() {
        realm = Realm.getDefaultInstance();
    }

    public static DataBase getInstance() {
        if (instance == null) {
            instance = new DataBase();
        }
        return instance;
    }

    public boolean activeSesion() {
        return !realm.isEmpty();
    }

    public void saveSession(String access_token, String refresh_token) {
        realm.beginTransaction();
        realm.delete(Session.class);
        Session s = realm.createObject(Session.class);
        s.setAccess_token(access_token);
        s.setRefresh_token(refresh_token);
        realm.commitTransaction();
    }

    public void saveSessionInThread(String access_token, String refresh_token) {
        Realm realmAux = Realm.getDefaultInstance();
        realmAux.beginTransaction();
        realmAux.delete(Session.class);
        Session s = realmAux.createObject(Session.class);
        s.setAccess_token(access_token);
        s.setRefresh_token(refresh_token);
        realmAux.commitTransaction();
        realmAux.close();
    }

    public void deleteSession() {
        realm.beginTransaction();
        realm.delete(Session.class);
        realm.commitTransaction();
    }

    public String getAccessToken() {
        Session session = realm.where(Session.class).findFirst();
        return session.getAccess_token();
    }

    public void finalize() throws Throwable {
        if (realm != null) {
            realm.close();
        }
        super.finalize();
    }
}

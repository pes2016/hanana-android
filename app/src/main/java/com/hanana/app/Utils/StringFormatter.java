package com.hanana.app.Utils;

import android.content.Context;

import com.hanana.app.DataModels.Badge;
import com.hanana.app.DataModels.Event;
import com.hanana.app.DataModels.Group;
import com.hanana.app.DataModels.IEvent;
import com.hanana.app.DataModels.INumberOfBadges;
import com.hanana.app.DataModels.INumberOfFollowers;
import com.hanana.app.DataModels.IPoints;
import com.hanana.app.DataModels.OtherUser;
import com.hanana.app.DataModels.User;
import com.hanana.app.R;
import com.hanana.app.profile.view.ProfileView;

import java.text.ParseException;


public class StringFormatter {

    public static String getEventShareString(Context context, IEvent event){
        String hour = "";
        try {
            hour = DateFormatter.getHoursString(event.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String date = "";
        try {
            date = DateFormatter.getDDMMYYYY(event.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return event.getTitle()
                + "\n" + getFriendsWordFormatted(context,event)
                + "\n" + getAssistantsWordFormatted(context,event)
                + "\n" + date + " " + hour
                + "\n" + context.getString(R.string.install_hanana_copy);
    }

    public static String getGroupShareString(Context context, Group group) {
        return group.getName()
                + "\n" + group.getDescription()
                + "\n" + getNumberOfEventsWordFormatted(context,group)
                + "\n" + getNumberOfFollowersWordFormatted(context,group)
                + "\n" + context.getString(R.string.install_hanana_copy);
    }

    public static String getAssistantsWordFormatted(Context c, IEvent event){
        String assistantsWord;
        if(event.getNumberOfAssistants() == 1) {
            assistantsWord = c.getString(R.string.assistant);
        }
        else {
            assistantsWord = c.getString(R.string.assistants);
        }
        return event.getNumberOfAssistants() + " " + assistantsWord;
    }

    public static String getFriendsWordFormatted(Context context, IEvent event){
        String friendsWord;
        if (event.getNumberOfFriends() == 1) {
            friendsWord = context.getString(R.string.friend);
        }
        else {
            friendsWord = context.getString(R.string.friends);
        }
        return event.getNumberOfFriends() + " " + friendsWord;
    }

    public static String getNumberOfFollowersWordFormatted(Context context, INumberOfFollowers item){
        String aux;
        if (item.getNumberOfFollowers() == 1) {
            aux = context.getString(R.string.follower);
        }
        else {
            aux = context.getString(R.string.followers);
        }
        return item.getNumberOfFollowers() + " " + aux;
    }

    public static String getNumberOfFollowersInCommonWordFormatted(Context context, User item){
        String aux;
        if (item.getNumberOfFollowersInCommon() == 1) {
            aux = context.getString(R.string.follower_common);
        }
        else {
            aux = context.getString(R.string.followers_common);
        }
        return item.getNumberOfFollowersInCommon() + " " + aux;
    }

    public static String getNumberOfEventsWordFormatted(Context context, Group group){
        String aux;
        if (group.getNumberOfEvents() == 1) {
            aux = context.getString(R.string.event);
        }
        else {
            aux = context.getString(R.string.events);
        }
        return group.getNumberOfEvents() + " " + aux;
    }

    public static String getNumberOfBadgesWordFormatted(Context context, INumberOfBadges item) {
        String aux;
        if (item.getNumberOfBadges() == 1) {
            aux = context.getString(R.string.badge);
        }
        else {
            aux = context.getString(R.string.badges);
        }
        return item.getNumberOfBadges() + " " + aux;
    }

    public static String getNumberOfRatesWordFormatted(Context context, Event item){
        String aux;
        if (item.getNumberOfRates() == 1) {
            aux = context.getString(R.string.rate);
        }
        else {
            aux = context.getString(R.string.rates);
        }
        return item.getNumberOfRates() + " " + aux;
    }

    public static String getPointsWordFormatted(Context context, IPoints item) {
        String aux;
        if (item.getPoints() == 1) {
            aux = context.getString(R.string.point);
        }
        else {
            aux = context.getString(R.string.points);
        }
        return item.getPoints() + " " + aux;
    }

    public static String getBadgeStatusWordFormatted(Context context, Badge badge) {
        String aux = String.valueOf(badge.getCurrentValue() - badge.getTriggerValue());
        return aux + "/" + badge.getTriggerValue();
    }
}

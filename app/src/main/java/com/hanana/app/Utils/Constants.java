package com.hanana.app.Utils;

public class Constants {
    public static final int DISCOVER_ACTIVITY_NBR = 0;
    public static final int EVENTS_ACTIVITY_NBR = 1;
    public static final int MY_EVENTS_ACTIVITY_NBR = 2;
    public static final int RANKING_ACTIVITY_NBR = 3;
    public static final int SCAN_ACTIVITY_NBR = 4;

    public static final int PROFILE_ACTIVITY_NBR = 5;
    public static final int CHAT_LIST_ACTIVITY_NUMBER = 6;
    public static final int CREATE_EVENT_ACTIVITY_NBR = 7;

    public static final int SETTINGS_ACTIVITY_NBR = 8;

    public static final int FACEBOOK_CODE = 64206;
    public static final int TWITTER_CODE = 140;

    public static final int SUGGESTED = 0;
    public static final int POPULAR = 1;
    public static final int FRIENDS = 2;
    public static final int MAP = 3;
    public static final int PROFILE = 4;
    public static final int NEXT = 5;
    public static final int PAST = 6;
    public static final int IS_CHOOSING = 0;
    public static final int IS_MY_PROFILE = 1;
    public static final int IS_OTHERS_PROFILE = 2;
    public static final int FOLLOWERS = 0;
    public static final int FOLLOWINGS = 1;
    public static final int EVENTS = 2;
    public static final int GROUPS = 3;
    public static final String ID = "objectId";
    public static final String LIST_TYPE = "listType";
    public final static int SELECT_PHOTO = 1;
    public static final String USER = "user";
    public static final String GROUP = "group";
    public static final String EVENT = "event";
    public static final String FUTURE_EVENT = "future";
    public static final String PAST_EVENT = "past";


//    public static final String SERVER_URL = "http://192.168.1.133:4000";
    public static final String SERVER_URL = "http://10.4.41.170:4000";
    public static final String SERVER_REGISTER_TWITTER = "/api/register/twitter";
    public static final String SERVER_REGISTER_FACEBOOK = "/api/register/facebook";

    public static final String SERVER_CLOSE_ACCOUNT = "/api/users/closeAccount";
    public static final String SERVER_RANKING = "/api/ranking";
    public static final String SERVER_ME = "/api/users/me";
    public static final String SERVER_ME_BLOCKED = "/api/users/me/blocked";
    public static final String SERVER_UPLOAD_AVATAR = "/api/users/avatar";
    public static final String SERVER_UPDATE_BIO = "/api/users/setBio";
    public static final String SERVER_TOGGLE_PRIVACITY = "/api/users/togglePrivacity";
    public static final String SERVER_MY_INTERESTS = "/api/users/interests";
    public static final String SERVER_USER_INTERESTS = "/api/users/{id}/interests";
    public static final String SERVER_USERS_DISCOVER = "/api/users/list";
    public static final String SERVER_SINGLE_USER = "/api/users/{id}";
    public static final String SERVER_USER_FOLLOW = "/api/users/{id}/follow";
    public static final String SERVER_USER_FOLLOWERS = "/api/users/{id}/followers";
    public static final String SERVER_USER_BADGES = "/api/app/users/{id}/badges";
    public static final String SERVER_USER_FOLLOWING = "/api/users/{id}/following";
    public static final String SERVER_USER_BLOCK = "/api/users/{id}/block";
    public static final String SERVER_TOGGLE_NOTIFICATIONS = "/api/users/toggleNotifications";

    public static final String SERVER_GET_PENDING_REQUESTS = "/api/users/me/requests";
    public static final String SERVER_ACCEPT_REQUEST = "/api/users/me/requests/accept/{id}";
    public static final String SERVER_DECLINE_REQUEST = "/api/users/me/requests/decline/{id}";

    public static final String SERVER_GROUPS_DISCOVER = "/api/groups/discover";
    public static final String SERVER_SINGLE_GROUP = "/api/groups/{id}";
    public static final String SERVER_GROUP_FOLLOW = "/api/groups/{id}/follow";
    public static final String SERVER_GROUP_FOLLOWERS = "/api/groups/{id}/followers";
    public static final String SERVER_GROUP_SHARE = "/api/app/groups/{id}/share";

    public static final String SERVER_EVENTS_MAP = "/api/app/events/search/map";
    public static final String SERVER_EVENTS_SUGGESTED = "/api/app/events/search/suggested";
    public static final String SERVER_EVENTS_POPULAR = "/api/app/events/search/popular";
    public static final String SERVER_EVENTS_FRIENDS = "/api/app/events/search/friends";
    public static final String SERVER_USER_EVENTS = "/api/app/users/{id}/events/{type}";
    public static final String SERVER_EVENTS_CREATE = "/api/app/events";
    public static final String SERVER_EVENTS_PRIVATE_UPDATE_IMAGE = "/api/app/events/{id}/image";
    public static final String SERVER_EVENTS_ASSISTANTS = "/api/app/events/{id}/assistants";
    public static final String SERVER_EVENTS_DOWNVOTE = "/api/app/events/{eventId}/rate/{id}/downvote";
    public static final String SERVER_EVENTS_UPVOTE = "/api/app/events/{eventId}/rate/{id}/upvote";
    public static final String SERVER_EVENTS_RATES = "/api/app/events/{id}/rate/list";
    public static final String SERVER_EVENTS_SHARE = "/api/app/events/{id}/share";

    public static final String SERVER_SINGLE_EVENT = "/api/app/events/{id}";
    public static final String SERVER_RATE_EVENT = "/api/app/events/{id}/rate";
    public static final String SERVER_TOGGLE_ASSIST = "/api/app/events/{id}/assist";
    public static final String SERVER_EVENT_CONFIRM_QR = "/api/app/events/{id}/qr";

    public static final String SERVER_COMPLAINTS = "/api/complaints/{type}/{id}";

    public static final int EVENT_HOLDER_TYPE = 0;
    public static final int GROUP_HOLDER_TYPE = 1;
    public static final int USERS_HOLDER_TYPE = 2;
    public static final int FOLLOWS_HOLDER_TYPE = 3;
    public static final int REQUESTS_HOLDER_TYPE = 4;
    public static final int BLOCKED_HOLDER_TYPE = 5;
    public static final int BADGES_HOLDER_TYPE = 6;
    public static final int RANKING_HOLDER_TYPE = 7;
    public static final int RATE_HOLDER_TYPE = 8;
}

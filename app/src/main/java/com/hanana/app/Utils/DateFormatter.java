package com.hanana.app.Utils;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Helper class for handling a most common subset of ISO 8601 strings
 * (in the following format: "2008-03-01T13:00:00+01:00"). It supports
 * parsing the "Z" timezone, but many other less-used features are
 * missing.
 */
public final class DateFormatter {
    private static final String SERVER_ISOSTRING_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    private static final String HOUR_FORMAT = "HH:mm";
    private static final String DATE_FORMAT = "dd-MM-yyyy";

    private static final String PRIVATE_EVENT_DATETIME= "dd/MM/yy HH:mm";

    private static final String ISOSTRING_DATE = "yyyy-MM-dd'T'HH:mm:ss";

    public static String getDDMMYYYY(String isoDateString) throws ParseException {
        DateFormat f = new SimpleDateFormat(SERVER_ISOSTRING_FORMAT, Locale.getDefault());
        Date date = f.parse(isoDateString);
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
        return formatter.format(date);
    }

    public static String getHoursString(String isoDateString) throws ParseException {
        DateFormat f = new SimpleDateFormat(SERVER_ISOSTRING_FORMAT, Locale.getDefault());
        Date date = f.parse(isoDateString);
        SimpleDateFormat formatter = new SimpleDateFormat(HOUR_FORMAT, Locale.getDefault());
        return formatter.format(date);
    }

    public static String ISOStringFromDateAndTime(String date, String time) {
        String dateTime = date + " " +time;
        SimpleDateFormat inputDateFormat = new SimpleDateFormat(PRIVATE_EVENT_DATETIME, Locale.getDefault());
        SimpleDateFormat outputDateFormat = new SimpleDateFormat(ISOSTRING_DATE, Locale.getDefault());
        String formatted = null;
        try {
            Date parsedDate = inputDateFormat.parse(dateTime);
            formatted = outputDateFormat.format(parsedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formatted;
    }

    public static Boolean checkIfPast(String isoDateString) throws ParseException {
        DateFormat f = new SimpleDateFormat(SERVER_ISOSTRING_FORMAT, Locale.getDefault());
        Date date = f.parse(isoDateString);
        return date.before(new Date());
    }

    public static Boolean checkIfInProgress(String isoDateString) throws ParseException {
        DateFormat f = new SimpleDateFormat(SERVER_ISOSTRING_FORMAT, Locale.getDefault());
        Date date = f.parse(isoDateString);

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR_OF_DAY, 24);

        return cal.getTime().after(new Date());
    }
}
package com.hanana.app.Utils;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.sendbird.android.SendBird;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;

public class MyApplication extends Application {

    private static final String TWITTER_KEY = "ZfXflCxRNvQbP0d9bWwEk6fgL";
    private static final String TWITTER_SECRET = "NjZMGBlZ959CQmoE5Og30JQMLXrLNCGQucOQajk7TC2vCEKFRN";
    private static final String APP_ID = "0921F52B-28B6-4D34-89D4-29EDA4913F57";


    @Override
    public void onCreate() {
        super.onCreate();

        initSDKS();
        facebookApiKey();

    }

    protected void initSDKS() {
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        Realm.init(this);
        SendBird.init(APP_ID, this);
    }

    protected void facebookApiKey() {
        Log.v("APP","INIT APY KEY");
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.hanana.app",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}

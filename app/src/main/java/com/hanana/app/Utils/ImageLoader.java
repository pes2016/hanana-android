package com.hanana.app.Utils;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import static com.hanana.app.Utils.Constants.SERVER_URL;

public class ImageLoader {
    private final static String TAG = "ImageLoader";

    public static String URLFormatter(String path) {
        if (path.startsWith("http")) {
            return path.replace('\\','/');
        }
        if (path.startsWith("/")) {
            return SERVER_URL + path;
        }
        return (SERVER_URL + '/' + path).replace('\\','/');
    }

    public static void loadImage(Context context, String path, ImageView view) {
        Picasso.with(context).load(URLFormatter(path)).fit().centerCrop().into(view);
    }

    public static void loadImage(Context context, Uri uri, ImageView view) {
        Picasso.with(context).load(uri).fit().centerCrop().into(view);
    }

    public static void loadImageCircle(Context context, String path, ImageView view) {
        Picasso.with(context).load(URLFormatter(path)).fit().centerCrop().transform(new CircleTransform()).into(view);
    }

    public static void loadImageCircleNoFit(Context context, String path, ImageView view) {
        Picasso.with(context).load(URLFormatter(path)).transform(new CircleTransform()).into(view);
    }


    public static void loadImageCircle(Context context, Uri uri, ImageView view) {
        Picasso.with(context).load(uri).fit().centerCrop().transform(new CircleTransform()).into(view);
    }

    public static void loadImageCircleWidthHeight(Context context, String path, ImageView view, int width, int height) {
        Picasso.with(context).load(URLFormatter(path)).centerCrop().resize(width,height).transform(new CircleTransform()).into(view);
    }

    public static void loadSquareImageCenterCropWidthHeight(Context context, String path, ImageView view, int width, int height) {
        Picasso.with(context).load(URLFormatter(path)).resize(width,height).centerCrop().into(view);
    }
}

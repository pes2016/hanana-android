package com.hanana.app.Utils;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.hanana.app.DataModels.User;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;

public class ActiveUser {
    private final static String TAG = "ActiveUser";
    private static ActiveUser instance = null;
    private String _id, access_token, refresh_token, avatar, name;
    private boolean pushNotifications;

    public static ActiveUser getInstance() {
        if (instance == null) {
            instance = new ActiveUser();
        }
        return instance;
    }

    public void create(User user) {
        instance._id = user.get_Id();
        instance.access_token = user.getAccess_token();
        instance.avatar = user.getAvatar();
        instance.name = user.getName();
        instance.refresh_token = user.getRefresh_token();
        instance.pushNotifications = user.getPushNotifications();

        Log.d(TAG, "create: ActiveUser");

        FirebaseMessaging.getInstance().subscribeToTopic("notifications");

        SendBird.connect(instance._id, new SendBird.ConnectHandler() {
            @Override
            public void onConnected(com.sendbird.android.User user, SendBirdException e) {
                Log.d(TAG, "onConnected: OnConnected");
                if(e != null) {
                    Log.d(TAG, "onConnected: Error on connect " + e.getMessage());
                }
                Log.d(TAG, "onConnected: " + ImageLoader.URLFormatter(instance.avatar));
                SendBird.updateCurrentUserInfo(instance.name, ImageLoader.URLFormatter(instance.avatar), new SendBird.UserInfoUpdateHandler() {
                    @Override
                    public void onUpdated(SendBirdException e) {
                        if (e != null) {
                            // Error.
                            Log.d(TAG, "onUpdated: onUpdated error " + e.getMessage());
                            return;
                        }else{
                            Log.d(TAG, "onUpdated: Profile updated correctly");
                        }
                    }
                });

                if (FirebaseInstanceId.getInstance().getToken() == null) return;

                SendBird.registerPushTokenForCurrentUser(FirebaseInstanceId.getInstance().getToken(),
                        new SendBird.RegisterPushTokenWithStatusHandler() {
                            @Override
                            public void onRegistered(SendBird.PushTokenRegistrationStatus status, SendBirdException e) {
                                Log.d(TAG, "onRegistered: Firebase token registered");
                                if (e != null) {
                                    // Error.
                                    Log.d(TAG, "onRegistered: Error registering");
                                    return;
                                }
                            }
                        });
            }
        });
    }

    public String get_Id() {
        return instance._id;
    }

    public static void deleteSession() {
        instance = null;
    }

    public String getAccess_token() {
        return instance.access_token;
    }

    public void setAccess_token(String access_token) {
        instance.access_token = access_token;
    }

    public String getAvatar() {
        return instance.avatar;
    }

    public void setAvatar(String avatar) {
        instance.avatar = avatar;
    }

    public String getName() {
        return instance.name;
    }

    public String getRefresh_token() {
        return instance.refresh_token;
    }

    public Boolean getPushNotifications() {
        return instance.pushNotifications;
    }

    public void setPushNotifications(Boolean isEnabled) {
        instance.pushNotifications = isEnabled;
    }
}

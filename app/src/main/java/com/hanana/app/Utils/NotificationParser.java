package com.hanana.app.Utils;

import android.util.Log;

import com.google.android.gms.actions.NoteIntents;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.hanana.app.DataModels.NotificationDAO;

import java.util.Map;

/**
 * Created by marcos on 12/01/2017.
 */

public class NotificationParser {
    private static final String TAG = "NotificationDAO";

    public static NotificationDAO getPayload(RemoteMessage remoteMessage){

        NotificationDAO notification = new NotificationDAO();

        Map<String, String> keys = remoteMessage.getData();

        if(keys.get("sendbird") != null){
            JsonElement payload = new JsonParser().parse(remoteMessage.getData().get("sendbird"));
            String sender = payload.getAsJsonObject().get("sender").getAsJsonObject().get("name").getAsString();
            String message = payload.getAsJsonObject().get("message").getAsString();
            String channelURL = payload.getAsJsonObject().get("channel").getAsJsonObject().get("channel_url").getAsString();

            notification.setTitle(sender);
            notification.setMessage(message);
            notification.setChat(true);
            notification.putExtra("channel_url",channelURL);
        }else{
            String sender = keys.get("title");
            String message = keys.get("body");

            notification.setTitle(sender);
            notification.setMessage(message);
            notification.setChat(false);
        }

        return notification;

    }
}

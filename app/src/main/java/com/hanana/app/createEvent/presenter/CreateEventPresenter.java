package com.hanana.app.createEvent.presenter;

import com.hanana.app.DataModels.Event;
import com.hanana.app.DataModels.RatesList;
import com.hanana.app.createEvent.view.ICreateEventView;
import com.hanana.app.event.model.EventModel;
import com.hanana.app.event.model.IEventModel;

import java.io.File;

public class CreateEventPresenter implements ICreateEventPresenter{
    private ICreateEventView mView;
    private IEventModel mModel;


    public CreateEventPresenter(ICreateEventView view) {
        super();
        mView = view;
        mModel = new EventModel(this);
    }


    @Override
    public void getEvent(String eventId) {

    }

    @Override
    public void displayEvent(Event event) {

    }

    @Override
    public void toggleAssist(String eventId) {

    }

    @Override
    public void setAssistButtonPressed() {

    }

    @Override
    public void setAssistButtonUnpressed() {

    }


    @Override
    public void rateEvent(String eventId, int rated, String comment) {

    }

    @Override
    public void complainEvent(String eventId, String comment) {

    }

    @Override
    public void hasShared(String eventId) {

    }

    @Override
    public void createEvent(Event event, File file) {
        mModel.createEvent(event, file);
    }

    @Override
    public void onEventCreated(String id) {
        mView.onEventCreated(id);
    }
}

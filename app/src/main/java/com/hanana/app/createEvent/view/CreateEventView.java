package com.hanana.app.createEvent.view;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.hanana.app.Views.BaseActivity;
import com.hanana.app.DataModels.Event;
import com.hanana.app.R;
import com.hanana.app.Utils.DateFormatter;
import com.hanana.app.Utils.ImageLoader;
import com.hanana.app.createEvent.presenter.CreateEventPresenter;
import com.hanana.app.createEvent.presenter.ICreateEventPresenter;
import com.hanana.app.event.view.EventView;
import com.philliphsu.bottomsheetpickers.date.BottomSheetDatePickerDialog;
import com.philliphsu.bottomsheetpickers.date.DatePickerDialog;
import com.philliphsu.bottomsheetpickers.time.BottomSheetTimePickerDialog;
import com.philliphsu.bottomsheetpickers.time.numberpad.NumberPadTimePickerDialog;

import java.io.File;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static com.hanana.app.Utils.Constants.CREATE_EVENT_ACTIVITY_NBR;
import static com.hanana.app.Utils.Constants.ID;
import static com.hanana.app.Utils.Constants.SELECT_PHOTO;

public class CreateEventView extends BaseActivity implements ICreateEventView, DatePickerDialog.OnDateSetListener, BottomSheetTimePickerDialog.OnTimeSetListener {
    private static final String TAG = "CreateEventView";
    private ICreateEventPresenter mPresenter;
    private File imageFile;
    private ImageView mImageView;
    private TextView mDateText, mTimeText;
    private EditText mTitle, mDesc, mAddress;

    private Button mButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_create_event_view);
        super.setNavigationItemClicked(whoIAm());

        mPresenter = new CreateEventPresenter(this);

        mDateText = (TextView) findViewById(R.id.date_edit_text);
        mTimeText = (TextView) findViewById(R.id.time_edit_text);
        mImageView = (ImageView) findViewById(R.id.create_event_image);
        mButton = (Button) findViewById(R.id.button_create_event);

        mTitle = (EditText) findViewById(R.id.edit_text_title);
        mDesc = (EditText) findViewById(R.id.edit_text_desc);
        mAddress = (EditText) findViewById(R.id.edit_text_address);

        mDateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatePicker(v);
            }
        });
        mTimeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker(v);
            }
        });
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImage();
            }
        });
        mButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked lol");
                createEvent();
            }
        });

    }

    private void clearError(View v) {
        ((TextView) v).setError(null);
    }

    private void openTimePicker(View v) {
        NumberPadTimePickerDialog dialog = NumberPadTimePickerDialog.newInstance(
                CreateEventView.this);
        dialog.show(getSupportFragmentManager(), TAG);
        clearError(v);
    }

    private void openDatePicker(View v) {
        Calendar now = Calendar.getInstance();
        BottomSheetDatePickerDialog dialog = BottomSheetDatePickerDialog.newInstance(
                CreateEventView.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH));
        dialog.show(getSupportFragmentManager(), TAG);
        clearError(v);
    }

    private void createEvent() {
        if (validateForm()) {

            disableButton();

            String date = mDateText.getText().toString();
            String time = mTimeText.getText().toString();
            String formatted = DateFormatter.ISOStringFromDateAndTime(date, time);

            Event event = new Event();
            event.setTitle(mTitle.getText().toString());
            event.setDescription(mDesc.getText().toString());
            event.setAddress(mAddress.getText().toString());
            event.setDate(formatted);
            mPresenter.createEvent(event, imageFile);
        }
    }

    private void disableButton() {
        mButton.setEnabled(false);
        mButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_grey));
    }

    private boolean validateForm() {
        boolean valid = true;

        if (mTitle.getText().length() == 0) {
            mTitle.setError(getString(R.string.error_text_title));
            valid = false;
        }

        if (mDesc.getText().length() == 0) {
            mDesc.setError(getString(R.string.error_text_description));
            valid = false;
        }

        if (mAddress.getText().length() == 0) {
            mAddress.setError(getString(R.string.error_text_address));
            valid = false;
        }


        //DATES:

        String date = mDateText.getText().toString();
        String time = mTimeText.getText().toString();

        if (date.length() == 0) {
            mDateText.setError(getString(R.string.required_date));
            valid = false;
        }
        if (time.length() == 0) {
            mTimeText.setError(getString(R.string.required_time));
            valid = false;
        }

        return valid;
    }

    @Override
    protected int whoIAm() {
        return CREATE_EVENT_ACTIVITY_NBR;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SELECT_PHOTO:
                if (resultCode == RESULT_OK) {
                    final Uri imageUri = data.getData();
                    String realPath = getRealPathFromURI(imageUri);
                    imageFile = new File(realPath);
                    ImageLoader.loadImage(this, imageUri, mImageView);
                }
                break;
        }
    }


    private void pickImage() {
        askForPermission(Manifest.permission.READ_EXTERNAL_STORAGE, SELECT_PHOTO);
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
    }


    @Override
    public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
        Calendar cal = new GregorianCalendar();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, monthOfYear);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        mDateText.setText(DateFormat.getDateFormat(this).format(cal.getTime()));
    }

    @Override
    public void onTimeSet(ViewGroup viewGroup, int hourOfDay, int minute) {
        Calendar cal = new GregorianCalendar();
        cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
        cal.set(Calendar.MINUTE, minute);
        mTimeText.setText(DateFormat.getTimeFormat(this).format(cal.getTime()));
    }

    @Override
    public void onEventCreated(String eventId) {
        Intent intent = new Intent(getApplicationContext(), EventView.class);
        intent.putExtra(ID, eventId);
        startActivity(intent);
    }
}

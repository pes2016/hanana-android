package com.hanana.app.createEvent.view;

public interface ICreateEventView {
    void onEventCreated(String eventId);
}

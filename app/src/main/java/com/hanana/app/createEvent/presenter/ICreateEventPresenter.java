package com.hanana.app.createEvent.presenter;

import com.hanana.app.DataModels.Event;
import com.hanana.app.event.presenter.IEventPresenter;

import java.io.File;

public interface ICreateEventPresenter extends IEventPresenter{
    void createEvent(Event event, File file);
    void onEventCreated(String id);
}

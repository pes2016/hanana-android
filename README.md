#Hanana Android

##What is Hanana





#tags
None at this moment.



#Useful info
Hanana is developed using MVP pattern and the following libraries:
  -

##MVP
MVP stands for Model - View - Presenter, referring to a software pattern that aims to decouple the presentation layer from the logic.

##References:
- [Antonio Leiva. MVP - Android](http://antonioleiva.com/mvp-android/)
- [Fernando Cejas. Architecting Android the clean way](http://fernandocejas.com/2014/09/03/architecting-android-the-clean-way/)
- [Tinmegali.com. Model View Presenter Android Part 1](http://www.tinmegali.com/en/model-view-presenter-android-part-1/)
- [Tinmegali.com. Model View Presenter Android Part 2](http://www.tinmegali.com/en/model-view-presenter-android-part-2/)
- [Tinmegali.com. Model View Presenter Android Part 3](http://www.tinmegali.com/en/model-view-presenter-android-part-3/)


##Retrofit:
- [Retrofit examples](https://guides.codepath.com/android/Consuming-APIs-with-Retrofit)

##References:
- [Consuming APIs with Retrofit](https://github.com/codepath/android_guides/wiki/Consuming-APIs-with-Retrofit)